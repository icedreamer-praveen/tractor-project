    def remove_on_image_update(self):
        try:
            # is the object in the database yet?
            obj = AdminUser.objects.get(id=self.id)
        except AdminUser.DoesNotExist:
            # object is not in db, nothing to worry about
            return
        # is the save due to an update of the actual image file?
        if obj.image and self.image and obj.image != self.image:
            # delete the old image file from the storage in favor of the new file
            obj.image.delete()

    def delete(self, *args, **kwargs):
        # object is being removed from db, remove the file from storage first
        self.image.delete()
        return super(AdminUser, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        # object is possibly being updated, if so, clean up.
        group,created=Group.objects.get_or_create(name="Admin")
		self.user.groups.add(group)
        self.remove_on_image_update()
        return super(AdminUser, self).save(*args, **kwargs)