from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from dashboard.models import State, District, Municipality


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(AbstractUser):

    is_customer = models.BooleanField(default=False)
    is_dealer = models.BooleanField(default=False)
    email = models.EmailField(max_length=254, unique=True)

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse("User_detail", kwargs={"pk": self.pk})


class UserProfile(models.Model):

    address = models.CharField(_("Address"), max_length=50)
    contact = models.CharField(_("Phone Number"), max_length=50)
    user = models.OneToOneField(User, verbose_name=_(
        "User"), on_delete=models.CASCADE, null=True,)

    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")

    def __str__(self):
        return self.user.username

    def get_absolute_url(self):
        return reverse("Profile_detail", kwargs={"pk": self.pk})


class SellerDetail(models.Model):

    name = models.CharField(_("Name"), max_length=500)
    phone_no = models.CharField(_("Phone Number"), max_length=50)
    email = models.EmailField(null=True, blank=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    address = models.CharField(_("Full Address"), max_length=500)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    pincode = models.CharField(max_length=250, null=True, blank=True)
    municipality = models.ForeignKey(
        Municipality, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        verbose_name = _("SellerDetail")
        verbose_name_plural = _("SellerDetails")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("SellerDetail_detail", kwargs={"pk": self.pk})


class AdminUser(User):
    full_name = models.CharField(max_length=250)
    image = models.ImageField(upload_to='admin')
    address = models.CharField(max_length=250)
    contact = models.CharField(max_length=250)

    def __str__(self):
        return self.first_name
