from django.urls import path, include

from .views import *

urlpatterns = [
    path('login/', LoginView.as_view(), name="login"),
    path("logout", LogoutView.as_view(), name="logout"),
    path("signup/", SignUpView.as_view(), name="signup"),
    path('customer/forgot/password/', CustomerForgotPasswordView.as_view(),
         name='customer-forgot-password'),

    path('customer/profile/login/',
         CustomerLoginView.as_view(), name='customer-login'),

    path('customer/profile/logout/',
         CustomerLogoutView.as_view(), name='customer-logout'),

]
