from django import forms
from django.core.exceptions import ValidationError
from django.db.models import fields

from django.forms import widgets
from .models import *
from harvester.forms import FormControlMixin
from django.contrib.auth.forms import PasswordChangeForm


# class UserCreateForm(forms.ModelForm):
#     password1 = forms.CharField(required=True)
#     password2 = forms.CharField(required=True)

#     class Meta:
#         model = User
#         fields = ("username", "email", "password1", "password2")

#     def clean_password2(self):
#         password1 = self.cleaned_data.get("password1")
#         password2 = self.cleaned_data.get("password2")
#         if password1 and password2 and password1 != password2:
#             raise forms.ValidationError("password does not match")
#         return password2

#     def clean_email(self):
#         email = self.cleaned_data.get('email')
#         if User.objects.filter(email=email).exists():
#             raise forms.ValidationError("Email already exists")
#         return email


class SellerDetailForm(FormControlMixin, forms.ModelForm):

    class Meta:
        model = SellerDetail
        fields = "__all__"
        widgets = {
            'state': forms.Select(attrs={
                'id': 'state'
            }),
            'district': forms.Select(attrs={
                'id': 'district'
            })
        }


class AdminCreateForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = AdminUser
        fields = ['username', 'email', 'full_name',
                  'image', 'address', 'contact', 'groups']
        widgets = {
            'image': forms.ClearableFileInput(attrs={
                'onchange': 'preview()'
            })
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groups'].widget.attrs.update(
            {'class': 'form-control select2 feature-select'})

        def clean(self):
            cleaned_data = super().clean()
            username = self.cleaned_data['username']
            email = self.cleaned_data['email']
            mobile = self.cleaned_data['contact']
            if AdminUser.objects.filter(username=username).exists():
                raise ValidationError({
                    'username': 'this username is not available'
                })
            if AdminUser.objects.filter(email=email).exists():
                raise ValidationError({
                    'email': 'user with this email already exists'
                })
            if AdminUser.objects.filter(contact=mobile):
                raise ValidationError({
                    'contact': 'user with this mobile no. already exists'
                })
            if len(mobile) < 10:
                raise ValidationError({
                    'contact': 'Invalid mobile no.'
                })


class PasswordResetForm(forms.Form):

    email = forms.CharField(widget=forms.EmailInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter email address'
    }))

    def clean_email(self):
        e = self.cleaned_data.get('email')
        if User.objects.filter(email=e).exists():
            pass
        else:
            raise forms.ValidationError("User with this email doesn't exist")

        return e


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder':  'Old Password'}))

    new_password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder':  'Confirm Password'}))

    confirm_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control', 'placeholder':  'Confirm Password'}))

    def set_user(self, user):
        self.user = user

    def clean(self):
        old_password = self.cleaned_data.get('old_password')
        valpwd = self.cleaned_data.get('new_password1')
        confirm_password = self.cleaned_data.get('confirm_password')
        user = self.user

        if not user.check_password(old_password):
            raise ValidationError({
                'old_password': 'Incorrect Password'
            })

        if valpwd != confirm_password:
            raise forms.ValidationError({
                'new_password1': 'Password Not Matched'})

        return self.cleaned_data


class LoginForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())


class CustomerLoginForm(forms.Form):
    email = forms.EmailField(widget=forms.EmailInput(attrs={
        'class': 'form-control form-adjust__input',

    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control form-adjust__input'
    }))
