from frontend.forms import FormControlMixin
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib import auth
from django.contrib.auth.models import User, Group
from django.urls import reverse_lazy
from django.utils.crypto import get_random_string
from django.conf import settings as conf_settings
from django.core.mail import send_mail
from django.contrib import messages

from django.shortcuts import redirect
from django.views.generic.edit import CreateView, FormView

# from django.contrib.auth.models import User
from django.views.generic import TemplateView, View
from frontend.homemixins import FooterFormMixin
from django.contrib.auth.forms import UserCreationForm
from .forms import LoginForm, PasswordResetForm, CustomerLoginForm

from django.contrib.auth import get_user_model

User = get_user_model()

class UserRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.filter(name="Customer").exists():
            pass
        else:
            return redirect("customer-login")

        return super().dispatch(request, *args, **kwargs)


class LoginView(FormView):
    template_name = "users/login.html"
    form_class = LoginForm
    success_url = "/admin-dashboard/"

    def form_valid(self, form):
        email = form.cleaned_data["email"]
        pword = form.cleaned_data["password"]
        try:
            uname = User.objects.get(email=email).username
        except User.DoesNotExist:
            uname = None

        user = authenticate(username=uname, password=pword)
        if user is not None and user.groups.filter(name="Admin"):
            login(self.request, user)
        else:
            return render(self.request, self.template_name,
                          {'form': form,
                           'error': 'You have entered wrong email or password'})

        return super().form_valid(form)

    def get_success_url(self):
        user = self.request.user

        if user.groups.filter(name="Admin").exists():
            return "/admin-dashboard/"

        elif user.groups.filter(name="admin").exists():
            return "/admin-dashboard/"

        else:
            return "/login/"


class CustomerLoginView(FooterFormMixin, FormView):
    template_name = 'forms/login.html'
    form_class = CustomerLoginForm

    def form_valid(self, form):
        email = form.cleaned_data["email"]
        pword = form.cleaned_data["password"]
        try:
            uname = User.objects.get(email=email).username
        except User.DoesNotExist:
            uname = None

        user = authenticate(username=uname, password=pword)
        if user is not None and user.groups.filter(name="Customer").exists():
            messages.success(self.request, "Log In Success")
            login(self.request, user)
        else:
            return render(self.request, self.template_name,
                          {'form': form,
                           'error': 'You have entered wrong email or password'})

        return super().form_valid(form)

    def get_success_url(self):
        user = self.request.user
        if user.groups.filter(name="Customer").exists():
            return "/"
        elif user.groups.filter(name="customer").exists():
            return "/"
        else:
            return reverse_lazy('customer-login')


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        messages.success(request, 'You are logged out successfully.')
        return redirect('/login/')


class CustomerLogoutView(View):
    def get(self, request, *args, **kwargs):
        logout(request)
        messages.success(request, 'You are logged out successfully.')
        return redirect('/')


class SignUpView(FooterFormMixin, TemplateView):
    template_name = "forms/signup.html"

    def post(self, request, *args, **kwargs):
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['password2']
        # check if password match
        if password == password2:
            # check email
            if User.objects.filter(username=username):
                messages.error(request, 'Username is already taken.')
                return redirect('/')
            elif User.objects.filter(email=email):
                messages.error(request, 'Email is already used.')
                return redirect('/')
            else:
                user = User.objects.create_user(
                    username=username, password=password, email=email)
                group, created = Group.objects.get_or_create(name='Customer')
                user.groups.add(group)
                # login after register
                auth.login(request, user)
                messages.success(request, 'You are now logged in')
                return redirect('/')
        else:
            messages.error(request, 'Password do not match.')
            return redirect('/customer/profile/login/')


class CustomerForgotPasswordView(FormView):
    template_name = 'forms/reset-password.html'
    form_class = PasswordResetForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        email = form.cleaned_data['email']
        user = User.objects.filter(email=email).first()
        password = get_random_string(8)
        user.set_password(password)
        user.save(update_fields=['password'])

        text_content = f'''Your password has been changed.
        password: {password}'''
        send_mail(
            'Password Reset | Tractor Shop',
            text_content,
            conf_settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )
        messages.success(self.request, "Password reset code is sent")
        return super().form_valid(form)
