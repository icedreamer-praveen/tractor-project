from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
# from django.views.generic import TemplateView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('frontend.urls')),
    path('', include('inventory.urls')),
    path('', include('users.urls')),
    path('', include('dealer.urls')),
    path('', include('customer.urls')),
    path('', include('tool.urls')),
    path('', include('lubricant.urls')),
    path('', include('dashboard.urls')),
    path('', include('harvester.urls')),
    path('summernote/', include('django_summernote.urls')),
    # path('oauth/', include('social_django.urls', namespace='social')),
    # path('', TemplateView.as_view(template_name="signup.html")),
    # path('accounts/', include('allauth.urls')),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + \
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
