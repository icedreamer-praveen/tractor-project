from django.urls import path
from .views import *
from frontend import views

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('sell_tractor/', TractorSale.as_view(), name='sell_tractor'),
    # path('enquiry/', EnquiryView.as_view(), name='enquiry'),
    path('review/', ReviewView.as_view(), name='review'),
    path('all-reviews/<int:pk>/detail/', AllReviewsView.as_view(), name='all-review'),
    path('tractor_valuation/', TractorValuationView.as_view(),
         name='tractor_valuation'),
    path('videos/', views.video, name='video'),
    path('offers/', OfferView.as_view(), name='offer'),
    path('on_road_price/', OnRoadPriceView.as_view(), name='on_road_price'),
    path('send/newsletter/', NewsletterView.as_view(), name='send-newsletter'),

    # on road on_road_price
]
