from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.db.models.fields.related import ForeignKey
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from embed_video.fields import EmbedVideoField
from django.template.defaultfilters import slugify
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator
from inventory.models import Brand, Tractor
from dashboard.models import State, District
from inventory.models import Brand
from dashboard.models import State, District, Municipality


def current_year():
    return datetime.date.today().year


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


class VideoCategory(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)

    class Meta:
        verbose_name = _("VideoCategory")
        verbose_name_plural = _("VideoCategories")

    def __str__(self):
        return self.name


class Video(models.Model):
    category = models.ForeignKey(VideoCategory, on_delete=models.CASCADE)
    thumbnail = models.ImageField(upload_to="video")
    title = models.CharField(max_length=100)
    published_date = models.DateTimeField(
        auto_now_add=True, null=True, blank=True)
    video = EmbedVideoField()  # same like models.URLField()

    class Meta:
        verbose_name = _("Video")
        verbose_name_plural = _("Videos")

    def __str__(self):
        return self.title


class PostCategory(models.Model):

    name = models.CharField(_("Name"), max_length=500)

    class Meta:
        verbose_name = _("PostCategory")
        verbose_name_plural = _("PostCategorys")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("PostCategory_detail", kwargs={"pk": self.pk})


# model for post
class Post(models.Model):

    category = models.ForeignKey(PostCategory, verbose_name=_(
        "Categories"), on_delete=models.SET_NULL, null=True)
    title = models.CharField(_("Title"), max_length=500)
    text = models.TextField(_("Content"))
    image = models.ImageField(_("Image"), upload_to="blogs/",
                              height_field=None, width_field=None, max_length=None)
    date = models.DateTimeField(
        _("Added Date"), auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog-detail", kwargs={"pk": self.pk})

# model for news category


class NewsCategory(models.Model):
    name = models.CharField(_("Name"), max_length=200)
    slug = models.SlugField(_("Slug"), max_length=200)
    order_num = models.IntegerField(blank=True, null=True)
    # image = models.ImageField(_("Image"), upload_to="news_category/",height_field=None, width_field=None, max_length=None, null=True, blank=True)

    class Meta:
        verbose_name = _("NewsCategory")
        verbose_name_plural = _("NewsCategorys")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("NewsCategory_detail", kwargs={"pk": self.pk})

# # model for news


class News(models.Model):

    title = models.CharField(_("Title"), max_length=500)
    slug = models.SlugField(max_length=500, unique=True)
    text = models.TextField(_("News Content"))
    newscategory = models.ForeignKey(NewsCategory, verbose_name=_(
        "Categories"), on_delete=models.SET_NULL, null=True, blank=True)
    image = models.ImageField(_("News Image"), upload_to="news_images/",
                              height_field=None, width_field=None, max_length=None)
    date = models.DateTimeField(
        _("Added Date"), auto_now=False, auto_now_add=True)
    view_count = models.PositiveIntegerField(default=0)
    banner = models.BooleanField(default=True, null=True, blank=True)

    class Meta:
        verbose_name = _("News")
        verbose_name_plural = _("Newss")

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.url = slugify(self.title)
        super(News, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("News_detail", kwargs={"pk": self.pk})


class OnRoadPrice(models.Model):

    name = models.CharField(max_length=50)
    mobile = models.CharField(max_length=50)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    model = models.CharField(max_length=50)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    municipality = models.ForeignKey(Municipality, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('OnRoadPrice')
        verbose_name_plural = _("OnRoadPrices")

    def __str__(self):
        return self.model


class TractorValuation(models.Model):
    OWNER = (
        ('1st', '1st'),
        ('2nd', '2nd'),
        ('3rd', '3rd'),
        ('4th', '4th'),
        ('4th above', '4th above')
    )

    TYRE_CONDITION = (
        ('10%', '10%'),
        ('20%', '20%'),
        ('30%', '30%'),
        ('40%', '40%'),
        ('50%', '50%'),
        ('60%', '60%'),
        ('70%', '70%'),
        ('80%', '80%'),
        ('10%', '10%'),
        ('100%', '100%'),
    )

    name = models.CharField(max_length=50)
    mobile = models.CharField(max_length=30)
    model = models.CharField(max_length=30)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    owner = models.CharField(max_length=50, choices=OWNER)
    manufacture_year = models.PositiveIntegerField(default=current_year(), validators=[
        MinValueValidator(1984), max_value_current_year])
    tyre_condition = models.CharField(max_length=50, choices=TYRE_CONDITION)

    class Meta:
        verbose_name = ('TractorValuation')
        verbose_name_plural = ('TractorValuations')

    def __str__(self):
        return self.model


class Newsletter(models.Model):
    name = models.CharField(max_length=250)
    mobile = models.CharField(max_length=250)
    state = models.ForeignKey(State, on_delete=models.CASCADE)

    def __str__(self):
        return self.name




class Review(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    tractor = models.ForeignKey(Tractor, on_delete=models.CASCADE)
    rate = models.PositiveIntegerField(default=0)
    description = models.TextField()

    def __str__(self):
        return self.tractor.name