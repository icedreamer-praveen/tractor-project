from django.contrib import admin
from .models import Newsletter, Post, NewsCategory, News, PostCategory, OnRoadPrice, Review, TractorValuation
from django_summernote.admin import SummernoteModelAdmin

from embed_video.admin import AdminVideoMixin
from .models import Video


class MyModelAdmin(AdminVideoMixin, admin.ModelAdmin):
    pass


admin.site.register(Video, MyModelAdmin)

admin.site.register(PostCategory)
admin.site.register(OnRoadPrice)
admin.site.register(TractorValuation)
admin.site.register(Newsletter)
admin.site.register(Review)


class NewsCategoryAdmin(SummernoteModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ['name']}


admin.site.register(NewsCategory, NewsCategoryAdmin)


class NewsAdmin(SummernoteModelAdmin):
    list_display = ['title', 'newscategory']
    summernote_fields = ('text')
    prepopulated_fields = {'slug': ['title']}


admin.site.register(News, NewsAdmin)


class PostAdmin(SummernoteModelAdmin):
    list_display = ['title', 'category', 'date']
    summernote_fields = ('text')


admin.site.register(Post, PostAdmin)
