from django import forms
from django.core.exceptions import ValidationError
from django.db.models import fields
from django.forms import ModelForm
from .models import *
from frontend.models import *
from django_summernote.widgets import SummernoteWidget


class FormControlMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({

                'class': 'form-control tractor-input'

            })


class NewsCatForm(forms.ModelForm):
    class Meta:
        model = NewsCategory
        fields = ['name', 'slug', 'order_num']
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control tractor-input',
                'placeholder': 'Enter Category name'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control tractor-input',
                'placeholder': 'Enter slug'
            }),
            'order_num': forms.TextInput(attrs={
                'class': 'form-control tractor-input',
                'placeholder': 'Enter Order number'
            }),
            # 'image': forms.FileInput(attrs={
            #     'class': 'form-control tractor-input',
            #     "id" :      "image",
            # }),
        }


class NewsForm(forms.ModelForm):
    class Meta:
        model = News
        fields = ['title', 'slug', 'text', 'image', 'newscategory', 'banner']
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control', 'placeholder': 'Title'
            }),
            'slug': forms.TextInput(attrs={
                'class': 'form-control', 'placeholder': 'slug'
            }),
            'text': forms.Textarea(),
            'category': forms.TextInput(attrs={
                'class': 'form-control',
                'id': 'id_news_text'

            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control'
            }),
            'newscategory': forms.Select(attrs={
                'class': 'form-control'
            }),
            'banner': forms.CheckboxInput(),
            'text': SummernoteWidget(),

        }


class OnRoadPriceForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = OnRoadPrice
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['municipality'].widget.attrs.update({
            'placeholder' : 'municipality.'
            })
        self.fields['mobile'].widget.attrs.update({
            'placeholder': 'Moble No.'
        })
        self.fields['model'].widget.attrs.update({
            'placeholder': 'Model No.'
        })
        self.fields['name'].widget.attrs.update({
            'placeholder': 'Your Name'
        })
        self.fields['district'].widget.attrs.update({
            'placeholder': 'District'
        })

    def clean(self):
        cleaned_data = super().clean()
        mobile = self.cleaned_data['mobile']
        if '-' in mobile:
            if '-' in mobile[2]:
                y = mobile.split('-')
                if len(y[0]) != 2 or len(y[1]) != 8:
                    raise ValidationError({
                        'mobile': 'Invalid phone number'
                    })
            else:
                raise ValidationError({
                    'mobile': 'invalid phone number'
                })
        else:
            pass
        if len(mobile) < 8 or len(mobile) > 14:
            raise ValidationError({
                'mobile': 'Invalid Mobile Number'
            })


class TractorValuationForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = TractorValuation
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['owner'].choices = self.fields['owner'].choices[1:]
        self.fields['tyre_condition'].choices = self.fields['tyre_condition'].choices[1:]
        self.fields['model'].widget.attrs.update({
            'placeholder': 'enter model'
        })
        self.fields['manufacture_year'].widget.attrs.update({
            'placeholder': 'manufacture year'
        })
        self.fields['name'].widget.attrs.update({
            'placeholder': 'Your Name'
        })
        self.fields['mobile'].widget.attrs.update({
            'placeholder': 'Mobile No.'
        })

    def clean(self):
        cleaned_data = super().clean()
        mobile = self.cleaned_data['mobile']
        if '-' in mobile:
            if '-' in mobile[2]:
                y = mobile.split('-')
                if len(y[0]) != 2 or len(y[1]) != 8:
                    raise ValidationError({
                        'mobile': 'Invalid phone number'
                    })
            else:
                raise ValidationError({
                    'mobile': 'invalid phone number'
                })
        else:
            pass
        if len(mobile) < 8 or len(mobile) > 14:
            raise ValidationError({
                'mobile': 'Invalid Mobile Number'
            })


class NewsletterForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Newsletter
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Name'
            }),
            'mobile': forms.TextInput(attrs={
                'placeholder': 'Mobile No.'
            }),
        }
class VideoCategoryForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = VideoCategory
        fields = '__all__'

class VideoForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Video
        fields = ["category","thumbnail","title","video"]



# class ReviewForm(FormControlMixin, forms.ModelForm):
#     class Meta:
#         model = Review
#         fields = '__all__'
#         widgets = {
#             'brand' : forms.Select(attrs={
#                 'class' : 'form-control'
#             }),
#             'model' : forms.Select(attrs={
#                 'class' : 'form-control'
#             }),
#             'rate' : forms.TextInput(attrs={
#                 'class' : 'form-control',
#                 'id' : 'sumstar'
#             }),
#             'description' : forms.Textarea(attrs={
#                 'class' : 'form-control'
#             })
#         }