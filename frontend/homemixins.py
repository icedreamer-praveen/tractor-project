from frontend.forms import NewsletterForm
from inventory.models import Tractor, OldTractor, Implement


class FooterFormMixin():
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['n_form'] = NewsletterForm
        context['search_tractors'] = Tractor.objects.all().order_by(
            '-id')
        context['search_old_tractors'] = OldTractor.objects.all().order_by(
            '-view_count')
        context['search_implements'] = Implement.objects.all().order_by('-id')
        return context
        