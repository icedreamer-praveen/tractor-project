from django.shortcuts import render
from django.views.generic import RedirectView, TemplateView, CreateView, View
from django.views.generic.list import ListView
from django.views.generic.base import ContextMixin
from inventory.models import *
from inventory.forms import OldTractorForm, InventoryImageForm
from users.forms import SellerDetailForm
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, request
from django.contrib import messages
from frontend.models import *
from users.models import *
from django.utils.timezone import datetime  # important if using timezones
from django.urls import reverse_lazy
from users.views import UserRequiredMixin


from .forms import *

from .homemixins import FooterFormMixin


# Create your views here.
class IndexView(FooterFormMixin, TemplateView):
    template_name = 'frontend/index.html'

    def get_context_data(self, ** kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context["new_tractors"] = Tractor.objects.order_by('-id')
        context["popular_tractors"] = Tractor.objects.filter(is_popular=True)
        context["upcoming_tractors"] = Tractor.objects.filter(is_upcoming=True)
        context["old_tractors"] = OldTractor.objects.order_by('-id')
        context['old_tractor_brands'] = OldTractorBrand.objects.all()
        context['new_tractor_brands'] = Brand.objects.all()
        context['below_three_tractors'] = Tractor.objects.filter(
            price__lt=300000)
        context['between_three_and_five_tractors'] = Tractor.objects.filter(
            price__range=(300000, 500000))
        context['between_five_and_seven_tractors'] = Tractor.objects.filter(
            price__range=(500000, 700000))
        context['between_seven_and_ten_tractors'] = Tractor.objects.filter(
            price__range=(700000, 1000000))
        context['above_ten_tractors'] = Tractor.objects.filter(
            price__gt=1000000)
        context['brands'] = Brand.objects.all()
        context["premium_tractors"] = OldTractor.objects.filter(
            is_premium=True)
        context["old_popular_tractors"] = OldTractor.objects.filter(
            is_popular=True)
        context["tractor_videos"] = Video.objects.filter(
            category__slug="tractor-video")
        context["ag_videos"] = Video.objects.filter(
            category__slug="agriculture-videos")
        context["slider"] = Slider.objects.all()
        context["popular_implements"] = Implement.objects.all().order_by(
            '-view_count')
        context["tractor_news"] = News.objects.filter(
            newscategory__slug__icontains="tractor-news").order_by('-id')
        context["ag_news"] = News.objects.filter(
            newscategory__slug__icontains="agriculture-news").order_by('-id')

        return context


class TractorSale(FooterFormMixin, TemplateView):
    template_name = "frontend/sell_tractor.html"
    # model = OldTractor

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brandList'] = OldTractorBrand.objects.all()
        context['form'] = SellerDetailForm
        return context

    def post(self, request, *args, **kwargs):
        # personal info
        name = request.POST.get('name')
        mobile = request.POST.get('phone_no')
        s_id = request.POST.get('state')
        state = State.objects.filter(id=s_id).first()
        d_id = request.POST.get('district')
        district = District.objects.filter(id=d_id).first()
        email = request.POST.get('email')
        address = request.POST.get('address')

        # tractor info
        b_id = request.POST.get('brand')
        brand = OldTractorBrand.objects.get(pk=b_id)
        model = request.POST.get('model')
        p_year = request.POST.get('purchase_year')
        p_month = request.POST.get('purchase_month')
        hours = request.POST.get('hours')
        hp = request.POST.get('hp')

        reg = request.POST.get('registration')
        rc = request.POST.get('rc_available')
        engine = request.POST.get('engine_condition')
        tyre = request.POST.get('tyre_condition')
        finance_noc = request.POST.get('financial_noc')
        rot = request.POST.get('ROT_no')
        about = request.POST.get('about')
        overview = request.POST.get('overview')
        price = request.POST.get('price')

        # image
        l_image = request.FILES.get('left_image')
        r_image = request.FILES.get('right_image')
        f_image = request.FILES.get('front_image')
        b_image = request.FILES.get('back_image')
        t_image = request.FILES.get('tyre_image')
        m_image = request.FILES.get('meter_image')

        image = Image(left_image=l_image, right_image=r_image,
                      front_image=f_image, back_image=b_image,
                      meter_image=m_image, tyre_image=t_image)
        image.save()
        seller = SellerDetail(name=name, email=email, phone_no=mobile,
                              address=address, state=state, district=district)

        seller.save()
        tractor = OldTractor(seller=seller, name=model, brand=brand, purchase_year=p_year,
                             purchase_month=p_month, hours=hours, hp=hp, RC=rc,
                             reg_no=reg, engine_condition=engine, price=price,
                             ROT_no=rot, tyre_condition=tyre, overview=overview,
                             financial_noc=finance_noc, about=about, image=image,
                             is_posted=True)
        tractor.save()
        if int(price) > 2000000:
            tractor.is_premium = True
        else:
            tractor.is_premium = False
        tractor.save()
        messages.success(request, "Sell request has been sent.")

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# class EnquiryView(TemplateView):
#         template_name = "frontend/enquiry.html"

class ReviewView(UserRequiredMixin, FooterFormMixin, TemplateView):
    template_name = "frontend/review.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)

        if self.request.GET.get('brand') and self.request.GET.get('model') and self.request.GET.get('image'):
            context['brands'] = self.request.GET.get('brand')
            context['models'] = self.request.GET.get('model')
            context['image'] = self.request.GET.get('image')
            context['tractor_id'] = self.request.GET.get('tri')
            context['status'] = 1
        else:
            context ['brands'] = Brand.objects.all().order_by('-id')
            context ['tractors'] = Tractor.objects.all().order_by('-id')
            context['status'] = ""
        return context
    
    def post(self, request, *args, **kwargs):
        loggedin_user = self.request.user
        if  request.POST.get("tractor_id"):
            rv = Review(user=loggedin_user, tractor_id=request.POST.get("tractor_id"), rate =request.POST.get("star"), description=request.POST.get("review"))
            rv.save()
        else:
            brand_id = request.POST.get("brand1")
            model=request.POST.get("model1")
            tractor = Tractor.objects.filter(brand__id=brand_id, model=model).first()
            if tractor:
                rv = Review(user=loggedin_user, tractor=tractor, rate =request.POST.get("star"), description=request.POST.get("review"))
                rv.save()



        

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def form_valid(self, form):
       
        

        return super().form_valid(form)


class AllReviewsView(TemplateView):
    template_name = 'frontend/all-review.html'
   
    def get_context_data(self, ** kwargs):
        context = super(AllReviewsView, self).get_context_data(**kwargs)
        context['tractor'] = Tractor.objects.get(id=self.kwargs.get('pk'))
        context['reviews'] = Review.objects.filter(tractor_id = self.kwargs.get('pk')).order_by('-id')
        popular_tractors = Tractor.objects.order_by('-id').filter(is_popular=True)
        context['review'] = Review.objects.filter(tractor_id=self.kwargs.get('pk')).first()
        context["popular_tractors"] = popular_tractors

        return context


class TractorValuationView(FooterFormMixin, CreateView):
    template_name = "frontend/tractor_valuation.html"
    form_class = TractorValuationForm
    success_url = reverse_lazy('tractor_valuation')

    def form_valid(self, form):
        if form.is_valid():
            messages.success(self.request, "successfully submitted")
        else:
            messages.error(self.request, "Something went wrong")

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['valuations'] = OldTractorBrand.objects.filter()

        return context


def video(request):
    obj = Video.objects.all()
    return render(request, 'frontend/video.html', {'obj': obj})


class OfferView(TemplateView):
    template_name = "frontend/offer.html"


class OnRoadPriceView(FooterFormMixin, CreateView):
    template_name = "frontend/on_road_price.html"
    form_class = OnRoadPriceForm
    success_url = reverse_lazy('on_road_price')

    def form_valid(self, form):
        if form.is_valid():
            messages.success(self.request, "successfully submitted")
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brands'] = Brand.objects.all()

        return context


class NewsletterView(View):
    def post(self, request, *args, **kwargs):
        name = self.request.POST.get('name')
        mobile = self.request.POST.get('mobile')
        state_id = self.request.POST.get('state')
        state = State.objects.filter(id=state_id).first()

        if Newsletter.objects.filter(mobile=mobile).exists():
            messages.warning(request, "Wow you have already subscribed")
        else:
            obj = Newsletter.objects.create(
                name=name, mobile=mobile, state=state)
            messages.success(request, f"Thank you for subscription {name}")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class TractorSearchView(View):
    pass
