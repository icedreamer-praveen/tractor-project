from django.http import request
from django.shortcuts import render

from django.views.generic import RedirectView, TemplateView, ListView, DetailView
from inventory.models import Category, LubricantDetail, LubricantBrand, LubricantType

from frontend.homemixins import FooterFormMixin


class LubricantView(FooterFormMixin, TemplateView):
    template_name = 'lubricant/lubricant.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_lub'] = LubricantDetail.objects.count()
        context['lub_category'] = LubricantType.objects.all()
        context['lub_brands'] = LubricantBrand.objects.all()
        context["out_of_stock_lub"] = LubricantDetail.objects.filter()
        lubricants = LubricantDetail.objects.all().order_by('-id')

        filter_detail = {}

        # Price filter
        prices = self.request.GET.get('prices')
        if prices:
            x = prices.split("-")
            lubricants = lubricants.filter(
                price__range=(x[0]+'00000', x[1]+'00000'))
            filter_detail['price'] = "{}-{}".format(x[0], x[1])

        # HPS filter
        category = self.request.GET.getlist('category')
        if category:
            lubricants = lubricants.filter(
                lubricantdetail_type_id__in=category)
            filter_detail['category'] = lubricants

        # Filter with brand
        if 'brand' in self.request.GET:
            brand = self.request.GET.getlist('brand')
            lubricants = lubricants.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["object_list"] = lubricants
        context["filters"] = filter_detail
        return context

        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                price = self.request.GET.get('prices')
                price = price.split('-')
                queryset = queryset.filter(price__range=(price[0], price[-1]))
        if 'category' in self.request.GET:
            if self.request.GET.get('category') != '':
                category = self.request.GET.get('category')
                queryset = queryset.filter(
                    lubricantdetail_type__lubricant_type=category)
        if 'brand' in self.request.GET:
            if self.request.GET.get('brand') != '':
                brand = self.request.GET.get('brand')
                queryset = queryset.filter(brand__name=brand)
        return queryset


class LubricantDetailView(FooterFormMixin, DetailView):
    template_name = 'lubricant/lubricant_detail.html'
    model = LubricantDetail
    context_object_name = 'lubricant_detail'
