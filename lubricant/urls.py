from django.urls import path
from .views import *

urlpatterns = [
    path('lubricant/', LubricantView.as_view(), name='lubricant'),
    path('lubricant/<int:pk>/detail/',
         LubricantDetailView.as_view(), name='lubricant-detail'),
]
