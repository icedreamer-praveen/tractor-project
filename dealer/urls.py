from django.urls import path
from .views import DealearshipView, FindDealerView


urlpatterns = [
    # path('certified-dealers/', CertifiedDealerView.as_view(),
    #      name='certified-dealer'),
    path('dealership/', DealearshipView.as_view(), name='dealership'),
    path('find-dealer/', FindDealerView.as_view(), name='find-dealer'),
]
