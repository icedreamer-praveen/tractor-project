from django.contrib import admin
from dealer.models import *
# Register your models here.
admin.site.register([DealershipEnquiry, CertifiedDealer, BrokerDealer])
