from dealer.models import CertifiedDealer
from django.shortcuts import render
from django.contrib import messages
from django.shortcuts import redirect

from django.views.generic import RedirectView, TemplateView, CreateView, ListView
from inventory.models import Brand
from .forms import CertifiedDealerForm, DealershipEnquiryForm, FindDealerForm
from django.urls import reverse_lazy
from frontend.homemixins import FooterFormMixin

from dashboard.models import State, District


# class CertifiedDealerView(FooterFormMixin, TemplateView):
#     template_name = 'dealer/certified_dealer.html'


class DealearshipView(FooterFormMixin, CreateView):
    template_name = 'dealer/dealership.html'
    form_class = DealershipEnquiryForm
    success_url = reverse_lazy('dealership')

    def form_valid(self, form):
        if form.is_valid():
            messages.success(self.request, 'Form Submitted')
        return super().form_valid(form)

    # def get_context_data(self, *args, **kwargs):
    #     context = super(DealearshipView, self).get_context_data(
    #         *args, **kwargs)
    #     context['brands'] = Brand.objects.all()
    #     context['form'] = self.form_class(self.request.POST or None)
    #     return context

    # def post(self, request, *args, **kwargs):
    #     form = self.form_class(request.POST)
    #     context = self.get_context_data()
    #     if form.is_valid():
    #         form.save()
    #         form = self.form_class()
    #         messages.success(request, 'Form Submitted')
    #         return redirect('/dealership')
    #     return render(request, self.template_name, context)


class FindDealerView(FooterFormMixin, ListView):
    template_name = 'dealer/findDealer.html'
    model = CertifiedDealer
    paginate_by = 12

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['total_dealer'] = CertifiedDealer.objects.count()
        context['form'] = FindDealerForm
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        if 'authorization' and 'state' and 'district' in self.request.GET:
            br = self.request.GET.get('authorization')
            st = self.request.GET.get('state')
            ds = self.request.GET.get('district')
            brand = Brand.objects.filter(id=br).first()
            state = State.objects.filter(id=st).first()
            district = District.objects.filter(id=ds).first()
            queryset = queryset.filter(
                authorization=brand, state=state, district=district)
        return queryset
