from frontend.homemixins import FooterFormMixin
from django import forms
from django.core.exceptions import ValidationError
from django.db.models import fields
from .models import BrokerDealer, CertifiedDealer, DealershipEnquiry

from harvester.forms import FormControlMixin


class DealershipEnquiryForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = DealershipEnquiry
        fields = "__all__"

    def clean_phone_no(self):
        phone_no = self.cleaned_data['phone_no']
        try:
            int(phone_no)
        except ValueError:
            raise forms.ValidationError("Invalid Contact Number")
        return phone_no

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({
            'placeholder': 'Your Name'
        })
        self.fields['phone_no'].widget.attrs.update({
            'placeholder': 'Phone Number'
        })
        self.fields['district'].widget.attrs.update({
            'placeholder': 'District Here'
        })

    def clean(self):
        cleaned_data = super().clean()
        mobile = self.cleaned_data['phone_no']
        if '-' in mobile:
            if '-' in mobile[2]:
                y = mobile.split('-')
                if len(y[0]) != 2 or len(y[1]) != 8:
                    raise ValidationError({
                        'phone_no': 'Invalid phone number'
                    })
            else:
                raise ValidationError({
                    'phone_no': 'invalid phone number'
                })
        else:
            pass
        if len(mobile) < 8 or len(mobile) > 14:
            raise ValidationError({
                'phone_no': 'Invalid Mobile Number'
            })


class CertifiedDealerForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = CertifiedDealer
        fields = '__all__'


class FindDealerForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = CertifiedDealer
        fields = ['authorization', 'state', 'district']


class BrokerDealerForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = BrokerDealer
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({
            'placeholder': 'Enter your name'
        })
        self.fields['contact'].widget.attrs.update({
            'placeholder': 'Enter your contact'
        }),
        self.fields['brand'].widget.attrs.update({
            'class': 'form-select js-example-basic-multiple-limit',
            'data-placeholder': 'select brand'
        }),
        self.fields['state'].widget.attrs.update({
            'data-placeholder': 'select state',
            'class': 'form-select'
        })
