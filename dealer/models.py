from django.db import models
from django.db.models.fields.related import OneToOneField
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from inventory.models import Brand
from dashboard.models import State, District
# iterable
# STATES = (
#     ("Pradesh One", "Pradesh One"),
#     ("Pradesh Two", "Pradesh Two"),
#     ("Bagmati Pradesh", "Bagmati Pradesh"),
#     ("Gandaki Pradesh", "Gandaki Pradesh"),
#     ("Pradesh Five", "Pradesh Five"),
#     ("Karnali Pradesh", "Karnali Pradesh"),
#     ("Sudurpaschim Pradesh", "Sudurpaschim Pradesh"),
# )


class DealershipEnquiry(models.Model):

    name = models.CharField(_("Full Name"), max_length=500)
    phone_no = models.CharField(_("Phone Number"), max_length=50)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    date = models.DateField(_("Date"), auto_now_add=True, null=True)
    brand = models.ForeignKey("inventory.Brand", verbose_name=_(
        "Brand"), on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = _("DealershipEnquiry")
        verbose_name_plural = _("DealershipEnquirys")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("DealershipEnquiry_detail", kwargs={"pk": self.pk})


class CertifiedDealer(models.Model):
    name = models.CharField(max_length=250)
    authorization = models.ForeignKey(Brand, on_delete=models.CASCADE)
    contact = models.CharField(max_length=250)
    pin_no = models.PositiveIntegerField(unique=True, default=0)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    address = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class BrokerDealer(models.Model):
    PER_MONTH = (
        ('0-5', '0-5'),
        ('5-10', '5-10'),
        ('10-15', '10-15'),
        ('15-20', '15-20'),
        ('20-30', '20-30'),
        ('30 & Above', '30 & Above')
    )
    name = models.CharField(max_length=250)
    contact = models.CharField(max_length=250)
    brand = models.ManyToManyField(Brand)
    tractor_per_month = models.CharField(max_length=250, choices=PER_MONTH)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
