from django.urls import path
from .views import *


urlpatterns = [
    path('all-tractor/', TractorListView.as_view(), name='all-tractor'),
    path('tractor/<int:pk>/', TractorDetailView.as_view(), name='tractor-detail'),
    path('popular-tractors/', PopularTractorListView.as_view(),
         name='popular-tractors'),
    path('latest-tractors/', LatestTractorListView.as_view(), name='latest-tractors'),
    path('upcoming-tractors/', UpcomingTractorListView.as_view(),
         name='upcoming-tractors'),
    path('new-tractors/', NewTractorListView.as_view(), name='new-tractors'),
    path('mini-tractors/', MiniTractorListView.as_view(), name='mini-tractors'),

    path('full-tractor-specification/<int:pk>/',
         TractorFullSpecificationView.as_view(), name='full-tractor-specification'),

    path('old-tractor-detail/<int:pk>/',
         OldTractorDetailView.as_view(), name='old-tractor-detail'),

    path('search-tractor/', SearchTractorView.as_view(), name='search-tractor'),
    path('all-brands/', BrandView.as_view(), name='all-brands'),
    path('compare/', CompareView.as_view(), name='compare'),
    path('compare-result/', CompareResultView.as_view(), name='compare-result'),
    path('used-tractor/', UsedTractorView.as_view(), name='used-tractor'),
    # url for implements
    path('implement/', ImplementView.as_view(), name='implement'),
    path('implement-detail-<slug:slug>/', ImplementDetailView.as_view(),
         name='implement-detail'),
    path('rotary_tiller/', ImplementRotaryTillerView.as_view(), name='rotary_tiller'),
    path('cultivator/', CultivatorView.as_view(), name='cultivator'),
    path('plough/', PloughView.as_view(), name='plough'),
    path('harrow/', HarrowView.as_view(), name='harrow'),
    path('trailer/', TrailerView.as_view(), name='trailer'),


    # url for tyre
    path('tyre/', TyreView.as_view(), name='tyre'),
    path('tyre-detail/<int:pk>/', TyreDetailView.as_view(), name='tyre-detail'),
    path('tyre-compare/', TyreCompareView.as_view(), name='tyre-compare'),
    path('tyre-compare-result/', TyreCompareResultView.as_view(),
         name='tyre-compare-result'),
    path('tyre/search/result/', TyreSearchedResult.as_view(), name='tyre-search'),

    path('more/', MoreView.as_view(), name='more'),

    # url for menu buy
    path('farm_implement/', FarmImplementView.as_view(), name='farm_implement'),
    path('farm_implement_detail/<int:pk>/detail/',
         FarmImplemenDetailtView.as_view(), name="farm_implement_detail"),
    path('land_properties/', LandPropertiesView.as_view(), name='land_properties'),
    path('animal_livestock/', AnimalLivestockView.as_view(),
         name='animal_livestock'),
    path('land-property-detail/', LandPropertyDetailView.as_view(),
         name='land-property-detail'),
    path('animal-livestock-detail/', AnimalLivestockDetailView.as_view(),
         name='animal-livestock-detail'),

    # url for news
    path('news/', NewsView.as_view(), name='news'),
    path('news-detail/<slug:slug>/', NewsDetailView.as_view(), name='newsdetail'),
    path('categorized-news/<slug:slug>/',
         CategorizedNewsView.as_view(), name='categorized-news'),
    path('news-search/', NewsSearchView.as_view(), name='news-search'),
    path('tractor-news/', TractorNewsView.as_view(), name='tractor-news'),
    path('agriculture-news/', AgricultureNewsView.as_view(),
         name='agriculture-news'),
    path('weather-news/', WeatherNewsView.as_view(), name='weather-news'),
    path('argi-business-news/', ArgiBusinessNewsView.as_view(),
         name='argi-business-news'),
    path('sarkari-yojana-news/', SarkariYojanaNewsView.as_view(),
         name='sarkari-yojana-news'),


]
