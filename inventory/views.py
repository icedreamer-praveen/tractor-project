from tool.models import ToolBrand, Images
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from .models import *
from .models import SIZE
from harvester.models import HarvesterBrand
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count
from django.views.generic import *
from frontend.models import News, NewsCategory, Review, Video
import datetime
from django.db.models import Q
from django.contrib import messages

from dashboard.forms import BuyerInqueryForm, BuyerOldImplementForm

from frontend.homemixins import FooterFormMixin


class TractorListView(FooterFormMixin, TemplateView):
    template_name = 'inventory/tractors.html'

    def get_context_data(self, ** kwargs):
        context = super(TractorListView, self).get_context_data(**kwargs)
        context["tractors"] = Tractor.objects.all().order_by('-id')[:7]
        return context


class TractorDetailView(FooterFormMixin, TemplateView):
    template_name = 'inventory/tractor_detail.html'

    def get_context_data(self, ** kwargs):
        context = super(TractorDetailView, self).get_context_data(**kwargs)
        context["tractor"] = Tractor.objects.get(id=self.kwargs.get('pk'))
        context['review'] = Review.objects.filter(tractor_id=self.kwargs.get('pk')).first()

        context["similar_tractors"] = Tractor.objects.exclude(id=self.kwargs.get('pk')).order_by('-id')
        context["oldtractors"] = OldTractor.objects.all().order_by('-id')
        return context


class TractorFullSpecificationView(FooterFormMixin, TemplateView):
    template_name = 'inventory/tractor-full-specification.html'

    def get_context_data(self, ** kwargs):
        context = super(TractorFullSpecificationView,
                        self).get_context_data(**kwargs)
        context["tractor"] = Tractor.objects.get(id=self.kwargs.get('pk'))
        return context


@method_decorator(csrf_exempt, name="dispatch")
class PopularTractorListView(FooterFormMixin, TemplateView):
    template_name = 'inventory/popular_tractors.html'

    def get_context_data(self, ** kwargs):
        context = super(PopularTractorListView,
                        self).get_context_data(**kwargs)
        popular_tractors = Tractor.objects.order_by(
            '-id').filter(is_popular=True)
        # context["brands"] = Brand.objects.all()
        context["brands"] = Tractor.objects.filter(is_popular=True).values(
            'brand_id', 'brand__name').annotate(dcount=Count('brand'))
        filter_detail = {}

        # Price filter
        prices = self.request.GET.get('prices')
        if prices:
            x = prices.split("-")
            popular_tractors = popular_tractors.filter(
                price__range=(x[0]+'00000', x[1]+'00000'))
            filter_detail['price'] = "{}-{}".format(x[0], x[1])

        # HPS filter
        hps = self.request.GET.get('hps')
        if hps:
            y = hps.split("-")
            popular_tractors = popular_tractors.filter(
                engine__hp__range=(y[0], y[1]))

            filter_detail['hps'] = "{}-{}".format(y[0], y[1])

        # Filter with brand
        if 'brand' in self.request.GET:
            brand = self.request.GET.getlist('brand')
            popular_tractors = popular_tractors.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["popular_tractors"] = popular_tractors
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }
        if request.POST['sort_by'] == '1':
            qs = Tractor.objects.filter(is_popular=True).order_by('price')
        else:
            qs = Tractor.objects.filter(is_popular=True).order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'hp': q.engine.hp if q.engine else " ",
                'name': q.name,
                'model': q.model,
                # 'image': q.image.url,
                # parent.secondary_parent_relation.name if parent.secondary_parent_relation else '--',
                'wd': q.wheel_tyre.wheel_drive if q.wheel_tyre else " ",
                'price': q.price,
            })
        return JsonResponse(context)


@method_decorator(csrf_exempt, name="dispatch")
class LatestTractorListView(FooterFormMixin, TemplateView):
    template_name = 'inventory/latest_tractors.html'

    def get_context_data(self, ** kwargs):
        context = super(LatestTractorListView, self).get_context_data(**kwargs)
        latest_tractors = Tractor.objects.order_by('-id')
        # context["brands"] = Brand.objects.all()
        context["brands"] = Tractor.objects.order_by(
            '-id').values('brand_id', 'brand__name').annotate(dcount=Count('brand'))
        filter_detail = {}

        # Price filter
        prices = self.request.GET.get('prices')
        if prices:
            x = prices.split("-")
            latest_tractors = latest_tractors.filter(
                price__range=(x[0]+'00000', x[1]+'00000'))
            filter_detail['price'] = "{}-{}".format(x[0], x[1])

        # HPS filter
        hps = self.request.GET.get('hps')
        if hps:
            y = hps.split("-")
            latest_tractors = latest_tractors.filter(
                engine__hp__range=(y[0], y[1]))

            filter_detail['hps'] = "{}-{}".format(y[0], y[1])

        # Filter with brand
        if 'brand' in self.request.GET:
            brand = self.request.GET.getlist('brand')
            latest_tractors = latest_tractors.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["latest_tractors"] = latest_tractors
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }
        if request.POST['sort_by'] == '1':
            qs = Tractor.objects.order_by('price', 'id')
        else:
            qs = Tractor.objects.order_by('-price', '-id')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'hp': q.engine.hp if q.engine else " ",
                'name': q.name,
                'model': q.model,
                # 'image': q.image.url,
                # parent.secondary_parent_relation.name if parent.secondary_parent_relation else '--',
                'wd': q.wheel_tyre.wheel_drive if q.wheel_tyre else " ",
                'price': q.price,
            })
        return JsonResponse(context)


@method_decorator(csrf_exempt, name="dispatch")
class UpcomingTractorListView(FooterFormMixin, TemplateView):
    template_name = 'inventory/upcoming_tractors.html'

    def get_context_data(self, ** kwargs):
        context = super(UpcomingTractorListView,
                        self).get_context_data(**kwargs)
        upcoming_tractors = Tractor.objects.order_by(
            '-id').filter(is_upcoming=True)
        # context["brands"] = Brand.objects.all()
        context["brands"] = Tractor.objects.filter(is_upcoming=True).values(
            'brand_id', 'brand__name').annotate(dcount=Count('brand'))
        filter_detail = {}

        # Price filter
        prices = self.request.GET.get('prices')
        if prices:
            x = prices.split("-")
            upcoming_tractors = upcoming_tractors.filter(
                price__range=(x[0]+'00000', x[1]+'00000'))
            filter_detail['price'] = "{}-{}".format(x[0], x[1])

        # HPS filter
        hps = self.request.GET.get('hps')
        if hps:
            y = hps.split("-")
            upcoming_tractors = upcoming_tractors.filter(
                engine__hp__range=(y[0], y[1]))

            filter_detail['hps'] = "{}-{}".format(y[0], y[1])

        # Filter with brand
        if 'brand' in self.request.GET:
            brand = self.request.GET.getlist('brand')
            upcoming_tractors = upcoming_tractors.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["upcoming_tractors"] = upcoming_tractors
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }
        if request.POST['sort_by'] == '1':
            qs = Tractor.objects.filter(is_upcoming=True).order_by('price')
        else:
            qs = Tractor.objects.filter(is_upcoming=True).order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'hp': q.engine.hp if q.engine else " ",
                'name': q.name,
                'model': q.model,
                # 'image': q.image.url,
                # parent.secondary_parent_relation.name if parent.secondary_parent_relation else '--',
                'wd': q.wheel_tyre.wheel_drive if q.wheel_tyre else " ",
                'price': q.price,
            })
        return JsonResponse(context)


class UsedTractorView(FooterFormMixin, ListView):
    template_name = 'inventory/used_tractors.html'
    model = OldTractor
    context_object_name = "tractors"

    def get_context_data(self, ** kwargs):
        context = super(UsedTractorView, self).get_context_data(**kwargs)
        context["old_brand"] = OldTractorBrand.objects.all()
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        if "brand" in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'brand':
                    queryset = queryset.filter(
                        brand__name__in=value)
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                price = self.request.GET.get('prices')
                price = price.split('-')
                amt_from = price[0]
                amt_to = price[1]
                queryset = queryset.filter(
                    price__range=(amt_from, amt_to)
                )
        if 'hps' in self.request.GET:
            if self.request.GET.get('hps') != '':
                hp = self.request.GET.get('hps')
                hp = hp.split('-')
                hp_from = hp[0]
                hp_to = hp[1]
                queryset = queryset.filter(hp__range=(hp_from, hp_to))
        if 'state' in self.request.GET:
            if self.request.GET.get('state') != '':
                state_id = self.request.GET.get('state')
                state = State.objects.filter(id=state_id).first()
                queryset = queryset.filter(seller__state=state)
        return queryset


class SearchTractorView(FooterFormMixin, TemplateView):
    template_name = 'inventory/search_tractors.html'

    def get_context_data(self, ** kwargs):
        context = super(SearchTractorView, self).get_context_data(**kwargs)
        context["tractors"] = Tractor.objects.all().order_by('-id')
        return context


class BrandView(FooterFormMixin, TemplateView):
    template_name = 'inventory/brand.html'

    def get_context_data(self, ** kwargs):
        context = super(BrandView, self).get_context_data(**kwargs)
        context['new_tractor'] = Brand.objects.all()
        context["old_tractor"] = OldTractorBrand.objects.all()
        context['implement_brand'] = ImplementBrand.objects.all()
        context['tool_brand'] = ToolBrand.objects.all()
        context['harvester_brand'] = HarvesterBrand.objects.all()
        context['lubricant_brand'] = LubricantBrand.objects.all()
        return context

# class UsedTractorView(TemplateView):
#     template_name = 'inventory/used_tractors.html'


@ method_decorator(csrf_exempt, name="dispatch")
class OldTractorDetailView(FooterFormMixin, DetailView):
    template_name = 'inventory/old_tractor_detail.html'
    model = OldTractor
    context_object_name = 'tractor'

    def get_context_data(self, ** kwargs):
        context = super(OldTractorDetailView, self).get_context_data(**kwargs)
        context["similar_tractors"] = OldTractor.objects.all()
        context["form"] = BuyerInqueryForm
        return context

    def get_object(self):
        obj = super().get_object()
        obj.view_count += 1
        if obj.view_count > 50:
            obj.is_popular = True
        obj.save()
        return obj

    def post(self, request, *args, **kwargs):
        tractor_id = self.kwargs.get('pk')
        tractor = OldTractor.objects.get(pk=tractor_id)
        name = request.POST.get('name')
        contact = request.POST.get('contact')
        s_id = request.POST.get('state')
        state = State.objects.filter(id=s_id).first()
        d_id = request.POST.get('district')
        district = District.objects.filter(id=d_id).first()
        bid_price = request.POST.get('bid_price')
        obj = BuyOldTractor.objects.create(name=name, contact=contact, state=state, district=district,
                                           bid_price=bid_price, tractor=tractor)
        messages.success(request, "Your inquery has been sent.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@ method_decorator(csrf_exempt, name="dispatch")
class NewTractorListView(FooterFormMixin, TemplateView):
    template_name = 'inventory/new_tractors.html'

    def get_context_data(self, ** kwargs):
        context = super(NewTractorListView, self).get_context_data(**kwargs)
        context['reviews'] = Review.objects.all().order_by('-id')
        # context["brands"] = Brand.objects.all()
        context["brands"] = Tractor.objects.all().values(
            'brand_id', 'brand__name').annotate(dcount=Count('brand'))
        tractors = Tractor.objects.all().order_by('-id')

        filter_detail = {}
        prices = self.request.GET.get('prices')

        # Price filter
        if prices:
            x = prices.split("-")
            tractors = tractors.filter(
                price__range=(x[0]+'00000', x[1]+'00000'))
            filter_detail['price'] = "{}-{}".format(x[0], x[1])

        # HPS filter
        hps = self.request.GET.get('hps')
        if hps:
            y = hps.split("-")
            tractors = tractors.filter(
                engine__hp__range=(y[0], y[1]))

            filter_detail['hps'] = "{}-{}".format(y[0], y[1])

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            tractors = tractors.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["tractors"] = tractors
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }

        if request.POST['sort_by'] == '1':
            qs = Tractor.objects.all().order_by('price')
        else:
            qs = Tractor.objects.all().order_by('-price')

        for q in qs:
            image = Image.objects.all().filter(id=q.image_id)
            context['data'].append({
                'id': q.id,
                'hp': q.engine.hp if q.engine else " ",
                'name': q.name,
                'model': q.model,
                'image': str(image[0]),
                # parent.secondary_parent_relation.name if parent.secondary_parent_relation else '--',
                'wd': q.wheel_tyre.wheel_drive if q.wheel_tyre else " ",
                'price': q.price,
            })
        return JsonResponse(context)


@ method_decorator(csrf_exempt, name="dispatch")
class MiniTractorListView(FooterFormMixin, TemplateView):
    template_name = 'inventory/mini_tractors.html'

    def get_context_data(self, ** kwargs):
        context = super(MiniTractorListView, self).get_context_data(**kwargs)
        # context["brands"] = Brand.objects.all()
        context["brands"] = Tractor.objects.filter(is_mini_tractor=True).values(
            'brand_id', 'brand__name').annotate(dcount=Count('brand'))
        tractors = Tractor.objects.filter(is_mini_tractor=True)

        filter_detail = {}
        prices = self.request.GET.get('prices')

        # Price filter
        if prices:
            x = prices.split("-")
            tractors = tractors.filter(
                price__range=(x[0]+'00000', x[1]+'00000'))
            filter_detail['price'] = "{}-{}".format(x[0], x[1])

        # HPS filter
        hps = self.request.GET.get('hps')
        if hps:
            y = hps.split("-")
            tractors = tractors.filter(
                engine__hp__range=(y[0], y[1]))

            filter_detail['hps'] = "{}-{}".format(y[0], y[1])

        # Filter with brand
        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            tractors = tractors.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["tractors"] = tractors
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }
        if request.POST['sort_by'] == '1':
            qs = Tractor.objects.filter(is_mini_tractor=True).order_by('price')
        else:
            qs = Tractor.objects.filter(
                is_mini_tractor=True).order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'hp': q.engine.hp if q.engine else " ",
                'name': q.name,
                'model': q.model,
                # 'image': q.image.url,
                # parent.secondary_parent_relation.name if parent.secondary_parent_relation else '--',
                'wd': q.wheel_tyre.wheel_drive if q.wheel_tyre else " ",
                'price': q.price,
            })
        return JsonResponse(context)


class CompareView(FooterFormMixin, TemplateView):
    template_name = 'inventory/compare.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brands'] = Brand.objects.all()
        context['tractors'] = Tractor.objects.all().order_by('-id')

        return context


class CompareResultView(FooterFormMixin, TemplateView):
    template_name = 'inventory/compare_result.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'brand1' and 'model1' in self.request.GET:
            if 'brand1' != '' and 'model1' != '':
                brand1 = self.request.GET.get('brand1')
                model1 = self.request.GET.get('model1')
                context['tractor1'] = Tractor.objects.filter(
                    brand__name=brand1, model=model1).first()
        if 'brand2' and 'model2' in self.request.GET:
            if 'brand2' != '' and 'model2' != '':
                brand2 = self.request.GET.get('brand2')
                model2 = self.request.GET.get('model2')
                context['tractor2'] = Tractor.objects.filter(
                    brand__name=brand2, model=model2).first()
        if 'brand3' and 'model3' in self.request.GET:
            if 'brand3' != '' and 'model3' != '':
                brand3 = self.request.GET.get('brand3')
                model3 = self.request.GET.get('model3')
                context['tractor3'] = Tractor.objects.filter(
                    brand__name=brand3, model=model3).first()
        return context


@ method_decorator(csrf_exempt, name="dispatch")
class ImplementView(FooterFormMixin, TemplateView):
    template_name = 'inventory/implement.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ImplementView, self).get_context_data(**kwargs)
        implements = Implement.objects.all().order_by('-id')

        context["brands"] = Implement.objects.all().values(
            'brand_id', 'brand__name').annotate(justcount=Count('brand'))
        context["implementcategory"] = Implement.objects.all().values(
            'implementcategory_id', 'implementcategory__name').annotate(justcount=Count('implementcategory'))
        context["implementtype"] = Implement.objects.all().values(
            'implementtype_id', 'implementtype__name').annotate(justcount=Count('implementtype'))

        filter_detail = {}

        type = self.request.GET.get('type')
        if type:
            type = self.request.GET.getlist('type')
            implements = implements.filter(implementtype__id__in=type)
            filter_detail["types"] = type

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            implements = implements.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            implements = implements.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["implements"] = implements
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }

        if request.POST['sort_by'] == '1':
            qs = Implement.objects.all().order_by('price')
        else:
            qs = Implement.objects.all().order_by('-price')

        if request.POST['current_url']:
            brand_id = []
            cat_id = []
            type_id = []

            current_url = request.POST['current_url']
            url_parts = current_url.split('&')
            for url_part in url_parts:
                key_url = url_part.split('=')
                if 'brand' in key_url:
                    brand_id += key_url[1]
                if 'type' in key_url:
                    type_id += key_url[1]
                if 'category' in key_url:
                    cat_id += key_url[1]

            if type_id:
                implements = qs.filter(implementtype__id__in=type_id)

            if cat_id:
                implements = qs.filter(implementcategory__id__in=cat_id)

            if brand_id:
                # qs = Implement.objects.filter(brand__id__in=brand_id)
                implements = qs.filter(brand__id__in=brand_id)

        else:
            implements = qs

        for q in implements:
            context['data'].append({
                'id': q.id,
                'slug': q.slug,
                'name': q.name,
                'model': q.modelname,
                'image': q.thumbnail.url,
                'price': q.price,
                'power': q.implementpower,
                'category': q.implementcategory.name,
                'brand': q.brand.name,
            })
        return JsonResponse(context)


class ImplementDetailView(FooterFormMixin, DetailView):
    template_name = 'inventory/implement_detail.html'
    model = Implement
    context_object_name = "implement"

    def get_object(self):
        obj = super().get_object()
        obj.view_count += 1
        if obj.view_count > 50:
            obj.is_popular = True
        obj.save()
        return obj

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        implement = Implement.objects.get(slug=self.kwargs["slug"])
        context['cats'] = ImplementCategory.objects.get(id=implement.implementcategory.id)

        return context


class ImplementTypeWiseView(FooterFormMixin, TemplateView):
    template_name = 'inventory/implement_with_type.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context["type"] = ImplementType.objects.get(slug=self.kwargs["slug"])

        return context


@ method_decorator(csrf_exempt, name="dispatch")
class ImplementRotaryTillerView(FooterFormMixin, TemplateView):
    template_name = 'inventory/implementrotarytiller.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ImplementRotaryTillerView,
                        self).get_context_data(**kwargs)
        implements = Implement.objects.filter(Q(implementtype__name__icontains="Tiller") | Q(
            implementtype__name__icontains="Rotary") | Q(implementtype__name__icontains="Rotavator"))
        # implements = Implement.objects.all().order_by('-id')

        context["brands"] = Implement.objects.filter(Q(implementtype__name__icontains="Tiller") | Q(implementtype__name__icontains="Rotary") | Q(
            implementtype__name__icontains="Rotavator")).values('brand_id', 'brand__name').annotate(justcount=Count('brand'))
        context["implementcategory"] = Implement.objects.filter(Q(implementtype__name__icontains="Tiller") | Q(implementtype__name__icontains="Rotary") | Q(
            implementtype__name__icontains="Rotavator")).values('implementcategory_id', 'implementcategory__name').annotate(justcount=Count('implementcategory'))
        context["implementtype"] = Implement.objects.filter(Q(implementtype__name__icontains="Tiller") | Q(implementtype__name__icontains="Rotary") | Q(
            implementtype__name__icontains="Rotavator")).values('implementtype_id', 'implementtype__name').annotate(justcount=Count('implementtype'))

        filter_detail = {}

        type = self.request.GET.get('type')
        if type:
            type = self.request.GET.getlist('type')
            implements = implements.filter(implementtype__id__in=type)
            filter_detail["types"] = type

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            implements = implements.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            implements = implements.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["implements"] = implements
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }

        if request.POST['sort_by'] == '1':
            qs = Implement.objects.all().order_by('price')
        else:
            qs = Implement.objects.all().order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'slug': q.slug,
                'name': q.name,
                'model': q.modelname,
                'image': q.thumbnail.url,
                'price': q.price,
                'power': q.implementpower,
                'category': q.implementcategory.name,
                'brand': q.brand.name,
            })
        return JsonResponse(context)


@ method_decorator(csrf_exempt, name="dispatch")
class CultivatorView(FooterFormMixin, TemplateView):
    template_name = 'inventory/cultivator.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CultivatorView, self).get_context_data(**kwargs)
        implements = Implement.objects.filter(
            implementtype__name__icontains="Cultivator")

        context["brands"] = Implement.objects.filter(implementtype__name__icontains="Cultivator").values(
            'brand_id', 'brand__name').annotate(justcount=Count('brand'))
        context["implementcategory"] = Implement.objects.filter(implementtype__name__icontains="Cultivator").values(
            'implementcategory_id', 'implementcategory__name').annotate(justcount=Count('implementcategory'))

        filter_detail = {}

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            implements = implements.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            implements = implements.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["implements"] = implements
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }

        if request.POST['sort_by'] == '1':
            qs = Implement.objects.all().order_by('price')
        else:
            qs = Implement.objects.all().order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'slug': q.slug,
                'name': q.name,
                'model': q.modelname,
                'image': q.thumbnail.url,
                'price': q.price,
                'power': q.implementpower,
                'category': q.implementcategory.name,
                'brand': q.brand.name,
            })
        return JsonResponse(context)


@ method_decorator(csrf_exempt, name="dispatch")
class PloughView(FooterFormMixin, TemplateView):
    template_name = 'inventory/plough.html'

    def get_context_data(self, *args, **kwargs):
        context = super(PloughView, self).get_context_data(**kwargs)
        implements = Implement.objects.filter(
            implementtype__name__icontains="plough")

        context["brands"] = Implement.objects.filter(implementtype__name__icontains="plough").values(
            'brand_id', 'brand__name').annotate(justcount=Count('brand'))
        context["implementcategory"] = Implement.objects.filter(implementtype__name__icontains="plough").values(
            'implementcategory_id', 'implementcategory__name').annotate(justcount=Count('implementcategory'))
        context["implementtype"] = Implement.objects.filter(implementtype__name__icontains="plough").values(
            'implementtype_id', 'implementtype__name').annotate(justcount=Count('implementtype'))

        filter_detail = {}

        type = self.request.GET.get('type')
        if type:
            type = self.request.GET.getlist('type')
            implements = implements.filter(implementtype__id__in=type)
            filter_detail["types"] = type

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            implements = implements.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            implements = implements.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["implements"] = implements
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }

        if request.POST['sort_by'] == '1':
            qs = Implement.objects.all().order_by('price')
        else:
            qs = Implement.objects.all().order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'slug': q.slug,
                'name': q.name,
                'model': q.modelname,
                'image': q.thumbnail.url,
                'price': q.price,
                'power': q.implementpower,
                'category': q.implementcategory.name,
                'brand': q.brand.name,
            })
        return JsonResponse(context)


@ method_decorator(csrf_exempt, name="dispatch")
class HarrowView(FooterFormMixin, TemplateView):
    template_name = 'inventory/harrow.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HarrowView, self).get_context_data(**kwargs)
        implements = Implement.objects.filter(
            implementtype__name__icontains="harrow")

        context["brands"] = Implement.objects.filter(implementtype__name__icontains="harrow").values(
            'brand_id', 'brand__name').annotate(justcount=Count('brand'))
        context["implementcategory"] = Implement.objects.filter(implementtype__name__icontains="harrow").values(
            'implementcategory_id', 'implementcategory__name').annotate(justcount=Count('implementcategory'))
        context["implementtype"] = Implement.objects.filter(implementtype__name__icontains="harrow").values(
            'implementtype_id', 'implementtype__name').annotate(justcount=Count('implementtype'))

        filter_detail = {}

        type = self.request.GET.get('type')
        if type:
            type = self.request.GET.getlist('type')
            implements = implements.filter(implementtype__id__in=type)
            filter_detail["types"] = type

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            implements = implements.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            implements = implements.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["implements"] = implements
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }

        if request.POST['sort_by'] == '1':
            qs = Implement.objects.all().order_by('price')
        else:
            qs = Implement.objects.all().order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'slug': q.slug,
                'name': q.name,
                'model': q.modelname,
                'image': q.thumbnail.url,
                'price': q.price,
                'power': q.implementpower,
                'category': q.implementcategory.name,
                'brand': q.brand.name,
            })
        return JsonResponse(context)


@ method_decorator(csrf_exempt, name="dispatch")
class TrailerView(FooterFormMixin, TemplateView):
    template_name = 'inventory/trailer.html'

    def get_context_data(self, *args, **kwargs):
        context = super(TrailerView, self).get_context_data(**kwargs)
        implements = Implement.objects.filter(
            implementtype__name__icontains="trailer")

        context["brands"] = Implement.objects.filter(implementtype__name__icontains="trailer").values(
            'brand_id', 'brand__name').annotate(justcount=Count('brand'))
        context["implementcategory"] = Implement.objects.filter(implementtype__name__icontains="trailer").values(
            'implementcategory_id', 'implementcategory__name').annotate(justcount=Count('implementcategory'))
        context["implementtype"] = Implement.objects.filter(implementtype__name__icontains="trailer").values(
            'implementtype_id', 'implementtype__name').annotate(justcount=Count('implementtype'))

        filter_detail = {}

        type = self.request.GET.get('type')
        if type:
            type = self.request.GET.getlist('type')
            implements = implements.filter(implementtype__id__in=type)
            filter_detail["types"] = type

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            implements = implements.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            implements = implements.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["implements"] = implements
        context["filters"] = filter_detail
        return context

    def post(self, request, *args, **kwargs):
        context = {
            "data": []
        }

        if request.POST['sort_by'] == '1':
            qs = Implement.objects.all().order_by('price')
        else:
            qs = Implement.objects.all().order_by('-price')
        for q in qs:
            context['data'].append({
                'id': q.id,
                'slug': q.slug,
                'name': q.name,
                'model': q.modelname,
                'image': q.thumbnail.url,
                'price': q.price,
                'power': q.implementpower,
                'category': q.implementcategory.name,
                'brand': q.brand.name,
            })
        return JsonResponse(context)


class ToolView(FooterFormMixin, TemplateView):
    template_name = 'inventory/tool.html'


class TyreView(FooterFormMixin, ListView):
    template_name = 'tyre/tyre.html'
    context_object_name = 'tyres'
    model = TractorTyre

    def get_queryset(self):
        queryset = super().get_queryset().order_by('-view_count')
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['f_tyres'] = TractorTyre.objects.filter(type='Front Tyre')
        context['b_tyres'] = TractorTyre.objects.filter(type='Rear Tyre')
        context['t_brands'] = TyreBrand.objects.all()
        context["tyre_videos"] = Video.objects.filter(
            category__slug="tyre-vidoes")

        sizes = set()
        for size in SIZE:
            for s in size:
                sizes.add(s)
        context['sizes'] = sizes

        return context


class TyreSearchedResult(FooterFormMixin, TemplateView):
    template_name = 'tyre/searched-tyre.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.GET != '':
            b_id = self.request.GET.get('brand')
            brand = TyreBrand.objects.filter(id=b_id).first()
            size = self.request.GET.get('size')

        search_result = TractorTyre.objects.filter(
            brand__brand=brand, size=size)
        context['tyres'] = search_result
        return context


class TyreDetailView(FooterFormMixin, DetailView):
    template_name = 'tyre/tyre_detail.html'
    model = TractorTyre
    context_object_name = 'tyre'

    def get_context_data(self, ** kwargs):
        context = super(TyreDetailView, self).get_context_data(**kwargs)
        # context["tyre"] = Tyre.objects.get(id=self.kwargs.get('pk'))
        t_id = self.kwargs.get('pk')
        obj = TractorTyre.objects.get(id=t_id)
        t_type = obj.type
        context['similar_tyre'] = TractorTyre.objects.filter(
            type=t_type).exclude(id=t_id)
        return context

    def get_object(self):
        obj = super().get_object()
        obj.view_count += 1
        obj.save()
        return obj


class TyreCompareView(FooterFormMixin, TemplateView):
    template_name = 'tyre/tyre_compare.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brands'] = TyreBrand.objects.all()
        context['models'] = TractorTyre.objects.all()
        return context


class TyreCompareResultView(FooterFormMixin, TemplateView):
    template_name = 'tyre/tyre_compare_result.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'first_brand' and 'first_model' in self.request.GET:
            brand1 = self.request.GET.get('first_brand')
            model1 = self.request.GET.get('first_model')
            context['tyre1'] = TractorTyre.objects.filter(
                brand__brand=brand1, model_name=model1).first()
        if 'second_brand' and 'second_model' in self.request.GET:
            brand2 = self.request.GET.get('second_brand')
            model2 = self.request.GET.get('second_model')
            context['tyre2'] = TractorTyre.objects.filter(
                brand__brand=brand2, model_name=model2).first()

        if 'third_brand' and 'third_model' in self.request.GET:
            brand3 = self.request.GET.get('third_brand')
            model3 = self.request.GET.get('third_model')
            context['tyre3'] = TractorTyre.objects.filter(
                brand__brand=brand3, model_name=model3).first()
        return context


class MoreView(FooterFormMixin, TemplateView):
    template_name = 'inventory/more.html'

# buy menu template view


class FarmImplementView(FooterFormMixin, TemplateView):
    template_name = 'buy/farm_implement.html'

    def get_context_data(self, *args, **kwargs):
        context = super(FarmImplementView, self).get_context_data(**kwargs)
        implements = OldImplement.objects.all().order_by('-id')

        context["brands"] = OldImplement.objects.all().values(
            'brand_id', 'brand__name').annotate(justcount=Count('brand'))
        context["implementcategory"] = OldImplement.objects.all().values(
            'category_id', 'category__name').annotate(justcount=Count('category'))
        context["implementtype"] = Implement.objects.all().values(
            'implementtype_id', 'implementtype__name').annotate(justcount=Count('implementtype'))
        
        filter_detail = {}
        prices = self.request.GET.get('prices')
        if prices:
            x = prices.split("-")
            tractors = implements.filter(price__range=(x[0]+'00000', x[1]+'00000'))
            filter_detail['price'] = "{}-{}".format(x[0], x[1])


        type = self.request.GET.get('type')
        if type:
            type = self.request.GET.getlist('type')
            implements = implements.filter(implementtype__id__in=type)
            filter_detail["types"] = type

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            implements = implements.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            implements = implements.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["implements"] = implements
        context["filters"] = filter_detail
        return context


class FarmImplemenDetailtView(FooterFormMixin, DetailView):
    template_name = 'buy/farm_implement_detail.html'
    model = OldImplement
    context_object_name = 'old_implement'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = BuyerOldImplementForm

        return context

    def post(self, request, *args, **kwargs):
        implement_id = self.kwargs.get('pk')
        implement = OldImplement.objects.get(pk=implement_id)
        name = request.POST.get('name')
        contact = request.POST.get('contact')
        s_id = request.POST.get('state')
        state = State.objects.filter(id=s_id).first()
        d_id = request.POST.get('district')
        district = District.objects.filter(id=d_id).first()
        bid_price = request.POST.get('bid_price')
        obj = BuyOldImplement.objects.create(name=name, contact=contact, state=state, district=district,
                                             bid_price=bid_price, implement=implement)
        messages.success(request, "Your inquery has been sent.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class LandPropertiesView(FooterFormMixin, TemplateView):
    template_name = 'buy/land_properties.html'


class AnimalLivestockView(FooterFormMixin, TemplateView):
    template_name = 'buy/animal_livestock.html'


class AnimalLivestockDetailView(FooterFormMixin, TemplateView):
    template_name = 'buy/animal_livestock_detail.html'


class LandPropertyDetailView(FooterFormMixin, TemplateView):
    template_name = 'buy/land_property_detail.html'

# News


class NewsView(FooterFormMixin, TemplateView):
    template_name = 'inventory/news.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        firstnews = News.objects.filter(banner=True).order_by('-id')[0]
        recentnews = News.objects.last()
        news2 = News.objects.all().order_by('-id')[1]
        news3 = News.objects.all().order_by('-id')[2]
        if firstnews.id == recentnews.id:
            context['news2'] = news2
            context['news3'] = news3

        elif firstnews.id == news2.id:
            context['news2'] = recentnews
            context['news3'] = news3
        else:
            context['news2'] = recentnews
            context['news3'] = news2

        context['firstnews'] = firstnews
        week_ago = datetime.date.today()-datetime.timedelta(days=15)
        trendsnews = News.objects.filter(
            date__gte=week_ago).order_by('-view_count')[0:4]
        if not trendsnews:
            week_ago = datetime.date.today()-datetime.timedelta(days=30)
            trendsnews = News.objects.filter(
                date__gte=week_ago).order_by('-view_count')[0:4]
        context['trendingnews'] = trendsnews
        context['tractornews'] = News.objects.filter(
            newscategory__order_num=1).order_by('-id')
        context['agrinews'] = News.objects.filter(
            newscategory__order_num=2).order_by('-id')
        context['yojananews'] = News.objects.filter(
            newscategory__order_num=3).order_by('-id')
        context['weathernews'] = News.objects.filter(
            newscategory__order_num=4).order_by('-id')
        context['agribiznews'] = News.objects.filter(
            newscategory__order_num=5).order_by('-id')
        month_ago = datetime.date.today()-datetime.timedelta(days=30)
        context['popularposts'] = News.objects.filter(
            date__gte=month_ago).order_by('-view_count')[0:5]
        context['newscats'] = NewsCategory.objects.all().order_by('order_num')

        return context


class NewsDetailView(FooterFormMixin, DetailView):
    template_name = 'inventory/news_detail.html'
    model = News
    context_object_name = "newsdetail"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        my_slug = self.kwargs["slug"]
        news_obj = News.objects.get(slug=my_slug)
        news_obj.view_count += 1
        news_obj.save()

        week_ago = datetime.date.today()-datetime.timedelta(days=7)
        context['popularnews'] = News.objects.filter(
            date__gte=week_ago).order_by('-view_count')[1:6]

        return context


class CategorizedNewsView(FooterFormMixin, DetailView):
    template_name = 'news/categorizednews.html'
    model = NewsCategory
    context_object_name = "newscategory"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['newscats'] = NewsCategory.objects.exclude(
            slug=self.kwargs['slug']).order_by('order_num')
        week_ago = datetime.date.today()-datetime.timedelta(days=7)
        context['popularposts'] = News.objects.filter(
            date__gte=week_ago).order_by('-view_count')[0:5]

        return context


class NewsSearchView(FooterFormMixin, TemplateView):
    template_name = 'news/searchnews.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        key = self.request.GET['key']
        context['newscats'] = NewsCategory.objects.all().order_by('order_num')
        context['newss'] = News.objects.filter(
            title__icontains=key).order_by('-id')
        week_ago = datetime.date.today()-datetime.timedelta(days=15)
        context['popularposts'] = News.objects.filter(
            date__gte=week_ago).order_by('-view_count')[0:5]

        return context


class TractorNewsView(FooterFormMixin, TemplateView):
    template_name = 'news/tractorNews.html'


class AgricultureNewsView(FooterFormMixin, TemplateView):
    template_name = 'news/agricultureNews.html'


class WeatherNewsView(FooterFormMixin, TemplateView):
    template_name = 'news/weatherNews.html'


class ArgiBusinessNewsView(FooterFormMixin, TemplateView):
    template_name = 'news/agriBusinessNews.html'


class SarkariYojanaNewsView(FooterFormMixin, TemplateView):
    template_name = 'news/sarkariYojanaNews.html'
