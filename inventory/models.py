from django.db.models.fields import BLANK_CHOICE_DASH
from dashboard.models import State
from django.db import models
from django.urls.base import reverse_lazy
from django.utils import tree
from django.utils.translation import gettext_lazy as _
from django.urls import reverse

from tool.models import Images
from dashboard.models import State, District

from users.models import SellerDetail


class Slider(models.Model):
    image1 = models.ImageField(upload_to="image1")
    image2 = models.ImageField(upload_to='image2')
    image3 = models.ImageField(upload_to='image3')

    class Meta:
        verbose_name = _('Slider')
        verbose_name_plural = _('Sliders')

    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Slider_detail", kwargs={"pk": self.pk})


class Engine(models.Model):

    no_cylinder = models.IntegerField(_("Number of Cylinder"))
    hp = models.DecimalField(_("Horse Power"), max_digits=10, decimal_places=2)
    capacity_cc = models.DecimalField(
        _("Capacity in CC"), max_digits=15, decimal_places=2)
    cooling = models.CharField(
        _("Engine Cooling"), max_length=500, null=True, blank=True)
    air_filter = models.CharField(
        _("Air Filter"), max_length=500, null=True, blank=True)
    engine_rated_rpm = models.CharField(
        _("Engine Rated RPM"), max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = _("Engine")
        verbose_name_plural = _("Engines")

    def __str__(self):
        return str(self.no_cylinder)+" clinder-"+str(self.hp)+"hp -"+str(self.capacity_cc)+" cc-"+str(self.engine_rated_rpm)+" rpm"

    def get_absolute_url(self):
        return reverse("Engine_detail", kwargs={"pk": self.pk})


class Transmission(models.Model):

    clutch = models.CharField(_("Clutch"), max_length=500)
    gear_box = models.CharField(_("Gear Box"), max_length=500)
    battery = models.CharField(_("Battery"), max_length=500)
    alternator = models.CharField(_("Alternator"), max_length=500)
    forward_speed = models.CharField(_("Forward Speed"), max_length=500)
    reverse_speed = models.CharField(_("Reverse Speed"), max_length=500)
    transmission_type = models.CharField(
        _("Transmission Type"), max_length=500)

    class Meta:
        verbose_name = _("Transmission")
        verbose_name_plural = _("Transmissions")

    def __str__(self):
        return "clutch: "+str(self.clutch)+" -Gear: "+str(self.gear_box)+" -Battery: "+str(self.battery)+" -Alt: "+str(self.alternator)

    def get_absolute_url(self):
        return reverse("Transmission_detail", kwargs={"pk": self.pk})


class Steering(models.Model):

    steering_type = models.CharField(_("Steering type"), max_length=50)

    class Meta:
        verbose_name = _("Steering")
        verbose_name_plural = _("Steerings")

    def __str__(self):
        return self.steering_type

    def get_absolute_url(self):
        return reverse("Steering_detail", kwargs={"pk": self.pk})


class Hydraulic(models.Model):

    lifting_capacity = models.CharField(_("Lifting Capacity"), max_length=500)
    three_point_linkage = models.CharField(
        _("Three Point Linkage"), max_length=500)

    class Meta:
        verbose_name = _("Hydraulic")
        verbose_name_plural = _("Hydraulices")

    def __str__(self):
        return self.lifting_capacity

    def get_absolute_url(self):
        return reverse("Hydraulic_detail", kwargs={"pk": self.pk})


class WheelAndTyre(models.Model):

    wheel_drive = models.CharField(_("Wheel Drive"), max_length=500)
    front = models.CharField(_("Front"), max_length=500)
    rear = models.CharField(_("Rear"), max_length=500)

    class Meta:
        verbose_name = _("WheelAndTyre")
        verbose_name_plural = _("WheelAndTyres")

    def __str__(self):
        return f'wheel drive: {self.wheel_drive}, front: {self.front}, rear: {self.rear}'

    def get_absolute_url(self):
        return reverse("WheelAndTyre_detail", kwargs={"pk": self.pk})


class Category(models.Model):
    CATEGORY = (
        ('Old Tractor', 'Old Tractor'),
        ('New Tractor', 'New Tractor'),
    )

    name = models.CharField(
        _("Category Name"), max_length=500, choices=CATEGORY)

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categorys")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Category_detail", kwargs={"pk": self.pk})


class Brand(models.Model):
    name = models.CharField(_("Brand Name"), max_length=500)
    image = models.ImageField(upload_to="image")

    class Meta:
        verbose_name = _("Brand")
        verbose_name_plural = _("Brands")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Brand_detail", kwargs={"pk": self.pk})


class OtherInformation(models.Model):

    accessories = models.CharField(_("Accessories"), max_length=500)
    additional_feature = models.TextField(_("Additional Feature"))
    warranty = models.CharField(_("Warranty"), max_length=500)
    status = models.CharField(
        _("Status"), max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = _("OtherInformation")
        verbose_name_plural = _("OtherInformations")

    def __str__(self):
        return "acc: "+str(self.accessories)+"  warranty: "+str(self.warranty)+"  stat: "+str(self.status)

    def get_absolute_url(self):
        return reverse("OtherInformation_detail", kwargs={"pk": self.pk})


class Power(models.Model):
    power_type = models.CharField(
        _("Type"), max_length=500, null=True, blank=True)
    rpm = models.CharField(_("RPM"), max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = _("Power")
        verbose_name_plural = _("Powers")

    def __str__(self):
        return self.power_type

    def get_absolute_url(self):
        return reverse("Power_detail", kwargs={"pk": self.pk})


class FuelTank(models.Model):
    capacity = models.CharField(_("Capacity"), max_length=500)

    class Meta:
        verbose_name = _("Fuel Tank")
        verbose_name_plural = _("Fuel Tanks")

    def __str__(self):
        return self.capacity

    def get_absolute_url(self):
        return reverse("FuelTank_detail", kwargs={"pk": self.pk})


class DimensionAndWeight(models.Model):
    total_weight = models.CharField(_("Total Weight"), max_length=500)
    wheel_base = models.CharField(
        _("Wheel Base"), max_length=500, null=True, blank=True)
    overall_length = models.CharField(_("Overall Length"), max_length=500)
    overall_weight = models.CharField(_("Overall Weight"), max_length=500)
    ground_clearance = models.CharField(
        _("Ground Clearance"), max_length=500, null=True, blank=True)
    turning_radius_brakes = models.CharField(
        _("Turning Radius Brakes"), max_length=500, null=True, blank=True)

    class Meta:
        verbose_name = _("DimensionAndWeight")
        verbose_name_plural = _("DimensionAndWeights")

    def __str__(self):
        return f'id: {self.id}Weight:{self.total_weight}, length:{self.overall_length}, brakes: {self.turning_radius_brakes}'

    def get_absolute_url(self):
        return reverse("DimensionAndWeight_detail", kwargs={"pk": self.pk})


class Image(models.Model):
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, null=True, blank=True)
    left_image = models.ImageField(
        upload_to="tractors/", height_field=None, width_field=None, max_length=None)
    right_image = models.ImageField(
        upload_to="tractors/", height_field=None, width_field=None, max_length=None)
    front_image = models.ImageField(
        upload_to="tractors/", height_field=None, width_field=None, max_length=None, null=True, blank=True)
    back_image = models.ImageField(
        upload_to="tractors/", height_field=None, width_field=None, max_length=None, null=True, blank=True)
    meter_image = models.ImageField(
        upload_to="tractors/", height_field=None, width_field=None, max_length=None, null=True, blank=True)
    tyre_image = models.ImageField(
        upload_to="tractors/", height_field=None, width_field=None, max_length=None, null=True, blank=True)

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")

    def __str__(self):
        return str(self.left_image)

    def get_absolute_url(self):
        return reverse("Image_detail", kwargs={"pk": self.pk})


class Tractor(models.Model):

    name = models.CharField(_("Tractor Name"), max_length=500)
    model = models.CharField(_("Tractor Model"), max_length=500)
    price = models.DecimalField(
        _("Tractor Price"), max_digits=25, decimal_places=2)
    brakes = models.CharField(_("Brakes"), max_length=500)
    engine = models.ForeignKey(Engine, verbose_name=_(
        "Engine"), on_delete=models.SET_NULL, null=True, blank=True)
    transmission = models.ForeignKey(Transmission, verbose_name=_(
        "Transmission"), on_delete=models.SET_NULL, null=True, blank=True)
    steering = models.ForeignKey(Steering, verbose_name=_(
        "Steering"), on_delete=models.SET_NULL, null=True, blank=True)
    hydraulic = models.ForeignKey(Hydraulic, verbose_name=_(
        "Hydraulic"), on_delete=models.SET_NULL, null=True, blank=True)
    wheel_tyre = models.ForeignKey(WheelAndTyre, verbose_name=_(
        "Wheel And Tyre"), on_delete=models.SET_NULL, null=True, blank=True)
    brand = models.ForeignKey(Brand, verbose_name=_(
        "Brand"), on_delete=models.CASCADE)
    category = models.ForeignKey(Category, verbose_name=_(
        "Category"), on_delete=models.CASCADE, null=True, blank=True)
    other_info = models.ForeignKey(OtherInformation, verbose_name=_(
        "Other Informations"), on_delete=models.SET_NULL, null=True, blank=True)
    power = models.ForeignKey("Power", verbose_name=_(
        "Power"), on_delete=models.SET_NULL, null=True, blank=True)
    fuel_tank = models.ForeignKey("FuelTank", verbose_name=_(
        "Fuel Tank"), on_delete=models.SET_NULL, null=True, blank=True)
    dimension_weight = models.ForeignKey("DimensionAndWeight", verbose_name=_(
        "Dimension And Weight"), on_delete=models.SET_NULL, null=True, blank=True)
    image = models.ForeignKey(
        Image, on_delete=models.CASCADE, null=True, blank=True)
    is_popular = models.BooleanField(default=False)
    is_avaliable = models.BooleanField(default=True)
    is_upcoming = models.BooleanField(default=False)
    is_mini_tractor = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Tractor")
        verbose_name_plural = _("Tractors")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Tractor_detail", kwargs={"pk": self.pk})


rc = (
    ("Yes", "Yes"),
    ("No", "No"),
)


class OldTractorBrand(models.Model):
    name = models.CharField(max_length=250)
    image = models.ImageField(upload_to="old tractor brand")

    def __str__(self):
        return self.name


class OldTractor(models.Model):
    seller = models.ForeignKey("users.SellerDetail", verbose_name=_(
        "Seller Detail"), on_delete=models.CASCADE)
    name = models.CharField(_("Tractor Name"), max_length=500)
    brand = models.ForeignKey(OldTractorBrand, on_delete=models.CASCADE)
    purchase_year = models.IntegerField(_("Purchase Year"))
    purchase_month = models.CharField(_("Purchase Month"), max_length=50)
    hours = models.CharField(_("Hours"), max_length=50)
    reg_no = models.CharField(_("Registration Number"),
                              max_length=50, null=True, blank=True)
    RC = models.CharField(_("RC"), max_length=20,
                          choices=rc, null=True, blank=True)
    price = models.DecimalField(_("Price"), max_digits=25, decimal_places=2)
    ROT_no = models.CharField(_("ROT Number"), max_length=100)
    tyre_condition = models.CharField(_("Tyre Condition"), max_length=500)
    engine_condition = models.CharField(_("Engine Condition"), max_length=500)
    financial_noc = models.CharField(_("Financial NOC"), max_length=500)
    about = models.TextField(_("About Your Tractor"))
    # warranty_avaliable = models.CharField(
    #     _("Warranty Avaliable"), max_length=500)
    # blue_book = models.CharField(_("Blue Book"), max_length=500)
    hp = models.DecimalField(_("Horse Power"), max_digits=6, decimal_places=2)
    overview = models.TextField(_("Overview"), null=True, blank=True)
    image = models.ForeignKey(
        Image, on_delete=models.CASCADE, null=True, blank=True)
    date_added = models.DateField(
        _("Added Date"), auto_now_add=True, null=True)
    is_posted = models.BooleanField(default=False)
    is_premium = models.BooleanField(default=False)
    is_popular = models.BooleanField(default=False)
    view_count = models.PositiveIntegerField(default=1, null=True, blank=True)
    updated = models.DateField(
        _("Updated Date"), auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = _("OldTractor")
        verbose_name_plural = _("OldTractors")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("OldTractor_detail", kwargs={"pk": self.pk})


class BuyOldTractor(models.Model):
    tractor = models.ForeignKey(OldTractor, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    contact = models.PositiveIntegerField()
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    bid_price = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class TyreBrand(models.Model):
    brand = models.CharField(max_length=250)
    image = models.ImageField(upload_to='tyre brand')

    def __str__(self):
        return self.brand


SIZE = (
    ('6.00 X 16', '6.00 X 16'),
    ('6.50 X 16', '6.50 X 16'),
    ('7.50 X 16', '7.50 X 16'),
    ('12.4 X 28', '12.4 X 28'),
    ('13.6 X 28', '13.6 X 28'),
    ('14.9 X 28', '14.9 X 28')
)


class TractorTyre(models.Model):
    TYRE_TYPE = (
        ('Front Tyre', 'Front Tyre'),
        ('Rear Tyre', 'Rear Tyre')
    )

    type = models.CharField(max_length=250, choices=TYRE_TYPE)
    model_name = models.CharField(_("Model name"), max_length=50)
    brand = models.ForeignKey(TyreBrand, on_delete=models.CASCADE)
    # image = models.ForeignKey(
    #     Image, on_delete=models.CASCADE, null=True, blank=True)
    diameter = models.DecimalField(
        _("diameter"), max_digits=30, decimal_places=2)
    width = models.DecimalField(_("width"), max_digits=20, decimal_places=2)
    size = models.CharField(
        _("Size"), max_length=250, choices=SIZE)
    price = models.DecimalField(_("Price"), max_digits=20, decimal_places=2)
    overview = models.TextField(_("overview"))
    feature = models.TextField(_("feature"))
    image = models.ManyToManyField(Images)
    ply_rating = models.CharField(_("Rating"), max_length=30)
    warrenty = models.CharField(max_length=250, null=True, blank=True)
    view_count = models.PositiveIntegerField(default=1, null=True, blank=True)

    class Meta:
        verbose_name = _("Tyre")
        verbose_name_plural = _("Tyres")

    def __str__(self):
        return f' brand : {self.brand}, type : {self.type}, model : {self.model_name}'

    def get_absolute_url(self):
        return reverse("Tyre_detail", kwargs={'pk': self.pk})


class ImplementCategory(models.Model):
    name = models.CharField(max_length=500)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Implement_Category_detail", kwargs={"pk": self.pk})


class ImplementBrand(models.Model):
    name = models.CharField(max_length=500)
    image = models.ImageField(upload_to="implement_brands")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Implement_Brand_detail", kwargs={"pk": self.pk})


class ImplementType(models.Model):
    name = models.CharField(max_length=500)
    slug = models.SlugField(max_length=500)
    is_published = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("implement_type_detail", kwargs={"pk": self.pk})


class Implement(models.Model):
    name = models.CharField(max_length=500)
    thumbnail = models.ImageField(upload_to="implement_thumbnail")
    implementcategory = models.ForeignKey(
        ImplementCategory, on_delete=models.CASCADE)
    brand = models.ForeignKey(ImplementBrand, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=300)
    implementtype = models.ForeignKey(ImplementType, on_delete=models.CASCADE)
    modelname = models.CharField(max_length=200)
    implementpower = models.CharField(max_length=50)
    price = models.PositiveIntegerField()
    image = models.ManyToManyField(Images)
    description = models.TextField()
    view_count = models.PositiveIntegerField(default=1, null=True, blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("implement_detail", kwargs={"slug": self.slug})


class OldImplement(models.Model):
    seller = models.ForeignKey(SellerDetail, on_delete=models.CASCADE)
    category = models.ForeignKey(ImplementCategory, on_delete=models.CASCADE)
    brand = models.ForeignKey(ImplementBrand, on_delete=models.CASCADE)
    model = models.CharField(max_length=250)
    purchase_year = models.IntegerField()
    title = models.CharField(max_length=250)
    price = models.PositiveIntegerField(default=0)
    power = models.CharField(max_length=50, null=True, blank=True)
    about = models.TextField()
    image1 = models.ImageField(upload_to='old_implement')
    image2 = models.ImageField(upload_to='old_implement')
    image3 = models.ImageField(
        upload_to='old_implement', null=True, blank=True)
    image4 = models.ImageField(
        upload_to='old_implement', null=True, blank=True)


def __str__(self):
    return f'brand: {self.brand}, model: {self.model}'


class BuyOldImplement(models.Model):
    implement = models.ForeignKey(OldImplement, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    contact = models.PositiveIntegerField()
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    bid_price = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class LubricantBrand(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='lubricant_brand')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("lubricant_brand_detail", kwargs={'slug': self.slug})


class LubricantType(models.Model):
    lubricant_type = models.CharField(max_length=100)

    def __str__(self):
        return self.lubricant_type

    def get_absolute_url(self):
        return reverse('lubricant_type_detail', kwargs={'slug': self.slug})


class LubricantDetail(models.Model):
    name = models.CharField(max_length=500)
    temperature = models.IntegerField()
    weight = models.IntegerField()
    price = models.PositiveIntegerField()
    brand = models.ForeignKey(LubricantBrand, on_delete=models.CASCADE)
    lubricantdetail_type = models.ForeignKey(
        LubricantType, on_delete=models.CASCADE)
    image = models.ManyToManyField(Images)
    quantity = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("lubricant_detail_detail", kwargs={"slug": self.slug})


# class LubricantImage(models.Model):
#     lubricant = models.ForeignKey(
#         LubricantDetail, related_name='lubricant_image', on_delete=models.CASCADE)
#     image = models.ImageField(upload_to='lubricant image')
