from django_summernote.admin import SummernoteModelAdmin
from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Engine)
admin.site.register(Transmission)
admin.site.register(Steering)
admin.site.register(Hydraulic)
admin.site.register(WheelAndTyre)
admin.site.register(Brand)
admin.site.register(Category)
admin.site.register(OtherInformation)
admin.site.register([Tractor, OldTractorBrand])
admin.site.register(Image)
admin.site.register(Power)
admin.site.register(FuelTank)
admin.site.register(DimensionAndWeight)
admin.site.register(TractorTyre)
admin.site.register(TyreBrand)
admin.site.register(BuyOldTractor)
admin.site.register(Implement)
admin.site.register(ImplementType)
admin.site.register(ImplementBrand)
admin.site.register(ImplementCategory)
admin.site.register([OldImplement, BuyOldImplement])
admin.site.register(Slider)
admin.site.register(LubricantBrand)
admin.site.register(LubricantDetail)
admin.site.register(LubricantType)


class OldTractorAdmin(SummernoteModelAdmin):
    summernote_fields = ('about', 'overview')


admin.site.register(OldTractor, OldTractorAdmin)
