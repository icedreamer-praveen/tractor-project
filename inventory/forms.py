from django.forms import widgets
from django.core.exceptions import ValidationError
from .models import *
from django import forms
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from harvester.forms import FormControlMixin


# class FormControlMixin:
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field in self.fields:
#             self.fields[field].widget.attrs.update({

#                 'class': 'form-control tractor-input'

#             })


class OldTractorBrandForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = OldTractorBrand
        fields = ['name', 'image']


class OldTractorForm(FormControlMixin, forms.ModelForm):

    class Meta:
        model = OldTractor
        fields = "__all__"
        widgets = {
            'about': SummernoteWidget(),
            'overview': SummernoteWidget(),
        }


class InventoryImageForm(FormControlMixin, forms.ModelForm):

    class Meta:
        model = Image
        fields = ['left_image', 'right_image', 'front_image',
                  'back_image', 'meter_image', 'tyre_image']


class CategoryForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'


class DimensionAndWeightsForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = DimensionAndWeight
        fields = '__all__'


class TractorEngineForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Engine
        fields = '__all__'


class FuelTankForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = FuelTank
        fields = '__all__'


class HydraulicesForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Hydraulic
        fields = '__all__'


class TractorForm(forms.ModelForm):
    class Meta:
        model = Tractor
        fields = ['name', 'model', 'price', 'brakes', 'engine', 'transmission', 'steering', 'hydraulic', 'wheel_tyre', 'brand', 'category',
                  'other_info', 'power', 'fuel_tank', 'dimension_weight', 'is_popular', 'is_avaliable', 'is_upcoming', 'is_mini_tractor']
        widgets = {
            "name": forms.TextInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name",
                "required": ""
            }),
            "model": forms.TextInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name",
                "required": ""
            }),
            "price": forms.TextInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name",
                "required": ""
            }),
            "brakes": forms.TextInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "brakes system",
                "required": ""
            }),
            "engine": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "transmission": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "steering": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "hydraulic": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "wheel_tyre": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "brand": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "category": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "other_info": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "power": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "fuel_tank": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "dimension_weight": forms.Select(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "is_popular": forms.CheckboxInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "is_avaliable": forms.CheckboxInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "is_upcoming": forms.CheckboxInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
            "is_mini_tractor": forms.CheckboxInput(attrs={
                "class": "form-control tractor-input",
                "placeholder": "Tractor Name"
            }),
        }


class TractorTyreForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = TractorTyre
        fields = '__all__'
        widgets = {
            'feature': SummernoteWidget(),
            'overview': SummernoteWidget()
        }

    def clean(self):
        cleaned_data = super().clean()
        t_type = self.cleaned_data.get('type')
        size = self.cleaned_data.get('size')
        if t_type == 'Front Tyre':
            if size != '6.00 X 16' and size != '6.50 X 16' and size != '7.50 X 16':
                raise ValidationError({
                    'size': f' no tyre of size {size} in {t_type}'
                })
            else:
                pass
        elif t_type == 'Rear Tyre':
            if size != '12.4 X 28' and size != '13.6 X 28' and size != '14.9 X 28':
                raise ValidationError({
                    'size': f' no tyre of size {size} in {t_type}'
                })
            else:
                pass
        else:
            pass


class TractorTyreBrandForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = TyreBrand
        fields = '__all__'


class OldImplementForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = OldImplement
        fields = '__all__'

        widgets = {
            'model': forms.TextInput(attrs={
                'placeholder': 'Enter model'
            }),
            'title': forms.TextInput(attrs={
                'placeholder': 'Enter title'
            }),
            'about': forms.Textarea(attrs={
                'placeholder': 'short descriptionn of your implements',
                'rows': '4'
            }),
            'image1': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview1(event);',
            }),
            'image2': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview2(event);',
            }),
            'image3': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview3(event);',
            }),
            'image4': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview4(event);',
            }),
        }
