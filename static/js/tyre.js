$('.slider-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '.tractor-year__container .tractor-content-nav',
    responsive: {
        0: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

$('.popularbyTyres-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        navText: [
            '<i class="fa fa-angle-left" aria-hidden="true"></i>',
            '<i class="fa fa-angle-right" aria-hidden="true"></i>'
        ],
        navContainer: '.smallSlider__container .custom-nav',
        responsive: {
            0: {
                items: 2
            },

            600: {
                items: 4
            },

            1000: {
                items: 6
            }
        }
    })
    // popiular tyre
var owl = $('.populartyre-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#populartyre .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// front six
var owl = $('.frontsixtyre-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#frontsixtyre .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// front six half
var owl = $('.frontsixhalftyre-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#frontsixhalftyre .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// front seven half
var owl = $('.frontsevenhalftyre-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#frontsevenhalftyre .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// reartwelveTyre
var owl = $('.reartwelveTyre-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#reartwelveTyre .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// rearthirteenTyre
var owl = $('.rearthirteenTyre-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#rearthirteenTyre .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// rearfourteenTyre
var owl = $('.rearfourteenTyre-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#rearfourteenTyre .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});