// Carousel for Popular tractor
var owl = $('.popular-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#popularTractor .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// Carousel for Latest tractor
var owl = $('.latest-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#latestTractor .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// Carousel for upcoming tractor
var owl = $('.upcoming-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#upcomingTractor .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// Carousel for popular implement tractor
var owl = $('.popularImplements-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#popularImplementsTractor .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// Carousel for three lakh tractor
var owl = $('.threelakh-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#threelakhContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// Carousel for 3-5 lakh tractor
var owl = $('.abovethreelakh-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#abovethreelakhContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// Carousel for 5-7 lakh tractor
var owl = $('.abovefivelakh-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#abovefivelakhContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});


// Carousel for 7-10 lakh tractor
var owl = $('.abovesevenlakh-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#abovesevenlakhContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});


// Carousel for three lakh tractor
var owl = $('.abovetenlakh-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#abovetenlakhContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// perium

var owl = $('.premium-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#premiumContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// selltractorlatest
var owl = $('.selltractorlatest-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#selltractorlatestContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// selltractorpopular
var owl = $('.selltractorpopular-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#selltractorpopularContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },

        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});

// comapre tractor
var owl = $('.compare-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 1.5
        },
        600: {
            items: 2
        },
        800: {
            items: 2
        },
        1000: {
            items: 3
        },
        1400: {
            items: 4
        }
    },
});
// News Video slider
var owl = $('.newsVideo-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 2
        },
        750: {
            items: 2
        },
        900: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 5
        }
    },
});
// Carousel for similar tractor
var owl = $('.similar-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#similarTractor .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});
// Carousel for similarUsed tractor
var owl = $('.usedTractor-content-carousel');
owl.owlCarousel({
    loop: false,
    margin: 16,
    nav: true,
    dots: false,
    navText: [
        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '#usedTractorContainer .tractor-content-nav',
    responsive: {
        0: {
            items: 1.5
        },
        500: {
            items: 2
        },
        800: {
            items: 3
        },
        1000: {
            items: 4
        },
        1400: {
            items: 6
        }
    },
    afterAction: function() {
        if (this.itemsAmount > this.visibleItems.length) {
            $('.next').show();
            $('.prev').show();

            $('.next').removeClass('disabled');
            $('.prev').removeClass('disabled');
            if (this.currentItem == 0) {
                $('.prev').addClass('disabled');
            }
            if (this.currentItem == this.maximumItem) {
                $('.next').addClass('disabled');
            }

        } else {
            $('.next').hide();
            $('.prev').hide();
        }
    }
});