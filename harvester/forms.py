from django import forms
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget

from .models import *


class FormControlMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({

                'class': 'form-control tractor-input'

            })


class EngineForm(FormControlMixin, forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = HarvesterEngine


class ClutchForm(FormControlMixin, forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Clutch


class TransmissionForm(FormControlMixin, forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = HarvesterTransmission


class CutterBarForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = CutterBar
        fields = '__all__'


class StrawWalkerForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = StrawWalker
        fields = '__all__'


class CleaningSievesForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = CleaningSieves
        fields = '__all__'


class CapacityForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Capacity
        fields = '__all__'


class BatteryForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Battery
        fields = '__all__'


class MainDimensionsForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = MainDimensions
        fields = '__all__'


class ConcaveForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Concave
        fields = '__all__'


class TyreForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Tyre
        fields = '__all__'
        widgets = {
            'front': forms.TextInput(attrs={
                'placeholder': '16-09-28'
            }),
            'rear': forms.TextInput(attrs={
                'placeholder': '7.50/16'
            })
        }


class ThreshingDrumForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = ThreshingDrum
        fields = '__all__'
        # widgets = {
        #     'mechanical_adjustment': forms.Select(attrs={
        #         'class': 'form-check-input'
        #     })
        # }


class HarvesterBrandForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = HarvesterBrand
        fields = '__all__'
        widgets = {
            'image': forms.ClearableFileInput()
        }


class PowerSourceForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = PowerSource
        fields = '__all__'


class HarvesterForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Harvester
        fields = '__all__'
        widgets = {
            'overview': SummernoteWidget()
        }


class OldHarvesterForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = OldHarvester
        fields = '__all__'

        widgets = {
            'model': forms.TextInput(attrs={
                'placeholder': 'Enter model'
            }),
            'title': forms.TextInput(attrs={
                'placeholder': 'Enter title'
            }),
            'about': forms.Textarea(attrs={
                'placeholder': 'short descriptionn of your implements',
                'rows': '4'
            }),
            'image1': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview1(event);',
            }),
            'image2': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview2(event);',
            }),
            'image3': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview3(event);',
            }),
            'image4': forms.ClearableFileInput(attrs={
                'onchange': 'imagePreview4(event);',
            }),
        }
