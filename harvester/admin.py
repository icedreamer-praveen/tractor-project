from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register([Harvester, Clutch, CutterBar, HarvesterEngine, HarvesterTransmission, StrawWalker,
                    CleaningSieves, Capacity, Battery, MainDimensions, Concave, Tyre, ThreshingDrum,
                    PowerSource, HarvesterBrand, OldHarvester, BuyOldHarvester])
