import datetime
from django.db.models.fields import CharField
from users.models import SellerDetail
from django.db import models
from tool.models import Images
from dashboard.models import State, District
# Create your models here.


class HarvesterEngine(models.Model):
    no_of_cylinder = models.CharField(max_length=250)
    model = models.CharField(max_length=250)
    engine_type = models.CharField(max_length=250)
    cooling_system = models.CharField(max_length=250)
    power = models.CharField(max_length=250)
    air_cleaner = models.CharField(max_length=250)

    def __str__(self):
        return f'model: {self.model}, type: {self.engine_type}'


class Clutch(models.Model):
    clutch_type = models.CharField(max_length=250, null=True, blank=True)
    diameter = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.clutch_type


class HarvesterTransmission(models.Model):
    forward_gear = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    reverse_gear = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    gear_speed = models.CharField(max_length=250, null=True, blank=True)
    first_gear_low = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    first_gear_high = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    second_gear_low = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    second_gear_high = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    third_gear_low = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    third_gear_high = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    fourth_gear_low = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    fourth_gear_high = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    reverse_gear_low = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    reverse_gear_high = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return f'{self.forward_gear} forward {self.reverse_gear} reverse'


class CutterBar(models.Model):
    wheat = models.CharField(max_length=250, null=True, blank=True)
    maize = models.CharField(max_length=250, null=True, blank=True)
    width_for_maize = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    width_for_wheat = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    clutting_height = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    blades = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    guards = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    stroke = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    reel = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    diameter = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    no_of_rows = models.PositiveIntegerField(default=0, null=True, blank=True)
    row_spacing = models.PositiveIntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return f'wheat: {self.width_for_maize}, maize: {self.width_for_maize} '


class StrawWalker(models.Model):
    no_of_straw_walker = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    no_of_steps = models.PositiveIntegerField(default=0, null=True, blank=True)
    width = models.PositiveIntegerField(default=0, null=True, blank=True)
    length = models.PositiveIntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return f'No. {self.no_of_straw_walker}, steps: {self.no_of_steps}'


class CleaningSieves(models.Model):
    area = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    upper_seive_area = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    lower_seive_area = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return str(self.area)


class Capacity(models.Model):
    grain_tank = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    fuel_tank = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return f'grain tank: {self.grain_tank}, fuel tank: {self.fuel_tank}'


class Battery(models.Model):
    no_of_battery = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    capacity_and_ratings = models.CharField(
        max_length=250, null=True, blank=True)

    def __str__(self):
        return f'battery: {self.no_of_battery}, {self.capacity_and_ratings}'


class MainDimensions(models.Model):
    working_length = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    working_width = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    working_height = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    working_ground_clearance = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    transport_length = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    transport_width = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    transport_height = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)
    transport_total_weight = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return f'working length: {self.working_length}, transport length: {self.transport_length}'


class Concave(models.Model):
    front_wheat_clearance = models.CharField(
        max_length=250, null=True, blank=True)
    rear_wheat_clearance = models.CharField(
        max_length=250, null=True, blank=True)
    front_paddy_clearance = models.CharField(
        max_length=250, null=True, blank=True)
    rear_paddy_clearance = models.CharField(
        max_length=250, null=True, blank=True)
    adjustment = models.BooleanField(default=False)

    def __str__(self):
        return f'Wheat Clearance: {self.front_wheat_clearance} Paddy Clearance: {self.front_paddy_clearance}'


class Tyre(models.Model):
    front_size = models.CharField(max_length=250, null=True, blank=True)
    rear_size = models.CharField(max_length=250, null=True, blank=True)
    front_ply_rating = models.CharField(max_length=250, null=True, blank=True)
    rear_ply_rating = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return f'front: {self.front_size}, rear : {self.rear_size}'


class ThreshingDrum(models.Model):
    diameter_for_wheat = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    diameter_for_maize = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    diameter_for_paddy = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    width_for_wheat = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    width_for_maize = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    width_for_paddy = models.PositiveIntegerField(
        default=0, null=True, blank=True)
    speed_from = models.PositiveIntegerField(default=0)
    speed_to = models.PositiveIntegerField(default=0)
    mechanical_adjustment = models.BooleanField(default=False)

    def __str__(self):
        return f'diameter: wheat= { self.diameter_for_wheat}, paddy: {self.diameter_for_paddy}, mechanical: {self.mechanical_adjustment}'


class PowerSource(models.Model):
    POWER_SOURCE = (
        ('Self Propelled', 'Self Propelled'),
        ('Tractor Mounted', 'Tractor Mounted')
    )

    power_source = models.CharField(
        max_length=250, choices=POWER_SOURCE, null=True, blank=True)

    def __str__(self):
        return self.power_source


class HarvesterBrand(models.Model):
    name = models.CharField(max_length=250, null=True, blank=True)
    image = models.ImageField(upload_to="harvester brand")

    def __str__(self):
        return self.name


class Harvester(models.Model):
    name = models.CharField(max_length=250)
    model = models.CharField(max_length=250)
    overview = models.TextField()
    brand = models.ForeignKey(
        HarvesterBrand, on_delete=models.CASCADE, null=True, blank=True)
    power = models.CharField(max_length=250)
    cutter_bar_width = models.DecimalField(max_digits=10, decimal_places=2)
    power_source = models.ForeignKey(
        PowerSource, on_delete=models.SET_NULL, null=True, blank=True)
    crop = models.CharField(max_length=250)
    engine = models.ForeignKey(
        HarvesterEngine, on_delete=models.SET_NULL, null=True, blank=True)
    clutch = models.ForeignKey(
        Clutch, on_delete=models.SET_NULL, null=True, blank=True)
    transmission = models.ForeignKey(
        HarvesterTransmission, on_delete=models.SET_NULL, null=True, blank=True)
    cutter_bar = models.ForeignKey(
        CutterBar, on_delete=models.SET_NULL, null=True, blank=True)
    threshing_drum = models.ForeignKey(
        ThreshingDrum, on_delete=models.SET_NULL, null=True, blank=True)
    tyre = models.ForeignKey(
        Tyre(), on_delete=models.SET_NULL, null=True, blank=True)
    concave = models.ForeignKey(
        Concave, on_delete=models.SET_NULL, null=True, blank=True)
    straw_walker = models.ForeignKey(
        StrawWalker, on_delete=models.SET_NULL, null=True, blank=True)
    cleaning_seives = models.ForeignKey(
        CleaningSieves, on_delete=models.SET_NULL, null=True, blank=True)
    capacity = models.ForeignKey(
        Capacity, on_delete=models.SET_NULL, null=True, blank=True)
    battery = models.ForeignKey(
        Battery, on_delete=models.SET_NULL, null=True, blank=True)
    main_dimension = models.ForeignKey(
        MainDimensions, on_delete=models.SET_NULL, null=True, blank=True)
    image = models.ManyToManyField(Images)

    def __str__(self):
        return f'name:{self.name}, model: {self.model}'


class OldHarvester(models.Model):
    CROP = (
        ('MultiCrop', 'MultiCrop'),
        ('Paddy', 'Paddy'),
        ('Maize', 'Maize'),
        ('Sugarcane', 'Sugarcane')
    )
    CUTTING_WIDTH = (
        ('1-7 feets', '1-7 feets'),
        ('8-14 feets', '8-14 feets'),
        ('aboove 14 feets', 'above 14 feets')
    )
    year = datetime.date.today().year
    seller = models.ForeignKey(SellerDetail, on_delete=models.CASCADE)
    brand = models.ForeignKey(HarvesterBrand, on_delete=models.CASCADE)
    model = models.CharField(max_length=250)
    crop_type = models.CharField(max_length=250, choices=CROP)
    cutting_width = models.CharField(max_length=250, choices=CUTTING_WIDTH)
    power_source = models.ForeignKey(PowerSource, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    hours = models.CharField(max_length=250, null=True, blank=True)
    purchase_year = models.IntegerField(default=year)
    price = models.PositiveIntegerField(default=0)
    about = models.TextField()
    image1 = models.ImageField(upload_to='old harvester')
    image2 = models.ImageField(upload_to='old harvester')
    image3 = models.ImageField(
        upload_to='old harvester', null=True, blank=True)
    image4 = models.ImageField(
        upload_to='old harvester', null=True, blank=True)

    def __str__(self):
        return f'brand: {self.brand}, model:{self.model}'


class BuyOldHarvester(models.Model):
    harvester = models.ForeignKey(OldHarvester, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    contact = models.PositiveIntegerField()
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    bid_price = models.PositiveIntegerField()

    def __str__(self):
        return self.name
