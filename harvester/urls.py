from django.urls import path
from .views import *

urlpatterns = [

    path('harvester/', HarvesterListView.as_view(), name='harvester'),
    path('harvester/<int:pk>/detail/', HarvesterDetailView.as_view(),
         name='harvester-detail'),
    path('old/harvester/list/', OldHarvesterView.as_view(),
         name='buy_old_harvester'),
    path('old/harvester/<int:pk>/detail/',
         OldHarvesterDetailtView.as_view(), name='buy_harvester_detail'),


]
