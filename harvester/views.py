from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView
from django.urls import reverse_lazy
from .models import *
from django.db.models import Count
from django.contrib import messages

from frontend.homemixins import FooterFormMixin
from dashboard.forms import BuyerOldHarvesterForm
from django.http.response import HttpResponseRedirect


class HarvesterListView(FooterFormMixin, TemplateView):
    template_name = 'harvester/harvester.html'
    model = Harvester

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['harvester_brands'] = HarvesterBrand.objects.all().order_by('-id')
        context['total_harvester'] = Harvester.objects.count()
        context['powersource'] = PowerSource.objects.all()
        harvesters = Harvester.objects.all().order_by('-id')

        filter_detail = {}

        brand = self.request.GET.get('harbrand')
        if brand:
            brand = self.request.GET.getlist('harbrand')
            harvesters = harvesters.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        power = self.request.GET.get('power')
        if power:
            power = self.request.GET.getlist('power')
            harvesters = harvesters.filter(
                power_source__id__in=power)
            filter_detail["powers"] = power

        cutting = self.request.GET.get('cutting')
        if cutting:
            cutting = self.request.GET.getlist('cutting')
            if len(cutting) == 1:
                for val in cutting:
                    if val == 'lessthan10':
                        harvesters = harvesters.filter(
                            cutter_bar_width__range=(1, 10))
                    if val == 'greternthan10':
                        harvesters = harvesters.filter(
                            cutter_bar_width__range=(11, 20))
            else:
                harvesters = harvesters.filter(
                    cutter_bar_width__range=(1, 20))

        context["object_list"] = harvesters
        context["filters"] = filter_detail

        return context

    # def get_queryset(self):
    #     queryset = super().get_queryset()
    #     if self.request.GET != '':
    #         for key, value in self.request.GET.lists():
    #             if key == 'harbrand':
    #                 queryset = queryset.filter(brand__name__in=value)
    #             if key == 'cutting':
    #                 if len(value) == 1:
    #                     for val in value:
    #                         if val == 'lessthan10':
    #                             queryset = queryset.filter(
    #                                 cutter_bar_width__range=(1, 10))
    #                         if val == 'greternthan10':
    #                             queryset = queryset.filter(
    #                                 cutter_bar_width__range=(11, 20))
    #                 else:
    #                     queryset = queryset.filter(
    #                         cutter_bar_width__range=(1, 20))
    #             if key == 'power':
    #                 queryset = queryset.filter(
    #                     power_source__power_source__in=value)

    #     return queryset


class HarvesterDetailView(FooterFormMixin, DetailView):
    template_name = 'harvester/harvester_detail.html'
    model = Harvester
    context_object_name = 'harvester'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['harvesters'] = Harvester.objects.exclude(
            id=self.get_object().id)

        return context


class OldHarvesterView(FooterFormMixin, TemplateView):
    template_name = 'buy/buy_harvester.html'

    def get_context_data(self, *args, **kwargs):
        context = super(OldHarvesterView, self).get_context_data(**kwargs)
        harvesters = OldHarvester.objects.all().order_by('-id')

        filter_detail = {}

        type = self.request.GET.get('type')
        if type:
            type = self.request.GET.getlist('type')
            harvesters = harvesters.filter(implementtype__id__in=type)
            filter_detail["types"] = type

        category = self.request.GET.get('category')
        if category:
            category = self.request.GET.getlist('category')
            harvesters = harvesters.filter(implementcategory__id__in=category)
            filter_detail["categories"] = category

        brand = self.request.GET.get('brand')
        if brand:
            brand = self.request.GET.getlist('brand')
            harvesters = harvesters.filter(brand__id__in=brand)
            filter_detail["brands"] = brand

        context["harvesters"] = harvesters
        context["filters"] = filter_detail
        return context


class OldHarvesterDetailtView(FooterFormMixin, DetailView):
    template_name = 'buy/old_harvester_detail.html'
    model = OldHarvester
    context_object_name = 'old_harvester'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = BuyerOldHarvesterForm

        return context

    def post(self, request, *args, **kwargs):
        harvester_id = self.kwargs.get('pk')
        harvester = OldHarvester.objects.get(pk=harvester_id)
        name = request.POST.get('name')
        contact = request.POST.get('contact')
        s_id = request.POST.get('state')
        state = State.objects.filter(id=s_id).first()
        d_id = request.POST.get('district')
        district = District.objects.filter(id=d_id).first()
        bid_price = request.POST.get('bid_price')
        obj = BuyOldHarvester.objects.create(name=name, contact=contact, state=state, district=district,
                                             bid_price=bid_price, harvester=harvester)
        messages.success(request, "Your inquery has been sent.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
