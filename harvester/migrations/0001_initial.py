# Generated by Django 3.1.7 on 2021-06-19 14:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Battery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no_of_battery', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('capacity_and_ratings', models.CharField(blank=True, max_length=250, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='BuyOldHarvester',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('contact', models.PositiveIntegerField()),
                ('bid_price', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Capacity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('grain_tank', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('fuel_tank', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='CleaningSieves',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('area', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('upper_seive_area', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('lower_seive_area', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Clutch',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('clutch_type', models.CharField(blank=True, max_length=250, null=True)),
                ('diameter', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Concave',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('front_wheat_clearance', models.CharField(blank=True, max_length=250, null=True)),
                ('rear_wheat_clearance', models.CharField(blank=True, max_length=250, null=True)),
                ('front_paddy_clearance', models.CharField(blank=True, max_length=250, null=True)),
                ('rear_paddy_clearance', models.CharField(blank=True, max_length=250, null=True)),
                ('adjustment', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='CutterBar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('wheat', models.CharField(blank=True, max_length=250, null=True)),
                ('maize', models.CharField(blank=True, max_length=250, null=True)),
                ('width_for_maize', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('width_for_wheat', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('clutting_height', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('blades', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('guards', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('stroke', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('reel', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('diameter', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('no_of_rows', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('row_spacing', models.PositiveIntegerField(blank=True, default=0, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Harvester',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('model', models.CharField(max_length=250)),
                ('overview', models.TextField()),
                ('power', models.CharField(max_length=250)),
                ('cutter_bar_width', models.DecimalField(decimal_places=2, max_digits=10)),
                ('crop', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='HarvesterBrand',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=250, null=True)),
                ('image', models.ImageField(upload_to='harvester brand')),
            ],
        ),
        migrations.CreateModel(
            name='HarvesterEngine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no_of_cylinder', models.CharField(max_length=250)),
                ('model', models.CharField(max_length=250)),
                ('engine_type', models.CharField(max_length=250)),
                ('cooling_system', models.CharField(max_length=250)),
                ('power', models.CharField(max_length=250)),
                ('air_cleaner', models.CharField(max_length=250)),
            ],
        ),
        migrations.CreateModel(
            name='HarvesterTransmission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('forward_gear', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('reverse_gear', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('gear_speed', models.CharField(blank=True, max_length=250, null=True)),
                ('first_gear_low', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('first_gear_high', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('second_gear_low', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('second_gear_high', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('third_gear_low', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('third_gear_high', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('fourth_gear_low', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('fourth_gear_high', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('reverse_gear_low', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('reverse_gear_high', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MainDimensions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('working_length', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('working_width', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('working_height', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('working_ground_clearance', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('transport_length', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('transport_width', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('transport_height', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('transport_total_weight', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PowerSource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('power_source', models.CharField(blank=True, choices=[('Self Propelled', 'Self Propelled'), ('Tractor Mounted', 'Tractor Mounted')], max_length=250, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='StrawWalker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no_of_straw_walker', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('no_of_steps', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('width', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('length', models.PositiveIntegerField(blank=True, default=0, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ThreshingDrum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('diameter_for_wheat', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('diameter_for_maize', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('diameter_for_paddy', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('width_for_wheat', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('width_for_maize', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('width_for_paddy', models.PositiveIntegerField(blank=True, default=0, null=True)),
                ('speed_from', models.PositiveIntegerField(default=0)),
                ('speed_to', models.PositiveIntegerField(default=0)),
                ('mechanical_adjustment', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Tyre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('front_size', models.CharField(blank=True, max_length=250, null=True)),
                ('rear_size', models.CharField(blank=True, max_length=250, null=True)),
                ('front_ply_rating', models.CharField(blank=True, max_length=250, null=True)),
                ('rear_ply_rating', models.CharField(blank=True, max_length=250, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='OldHarvester',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.CharField(max_length=250)),
                ('crop_type', models.CharField(choices=[('MultiCrop', 'MultiCrop'), ('Paddy', 'Paddy'), ('Maize', 'Maize'), ('Sugarcane', 'Sugarcane')], max_length=250)),
                ('cutting_width', models.CharField(choices=[('1-7 feets', '1-7 feets'), ('8-14 feets', '8-14 feets'), ('aboove 14 feets', 'above 14 feets')], max_length=250)),
                ('title', models.CharField(max_length=250)),
                ('hours', models.CharField(blank=True, max_length=250, null=True)),
                ('purchase_year', models.IntegerField(default=2021)),
                ('price', models.PositiveIntegerField(default=0)),
                ('about', models.TextField()),
                ('image1', models.ImageField(upload_to='old harvester')),
                ('image2', models.ImageField(upload_to='old harvester')),
                ('image3', models.ImageField(blank=True, null=True, upload_to='old harvester')),
                ('image4', models.ImageField(blank=True, null=True, upload_to='old harvester')),
                ('brand', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='harvester.harvesterbrand')),
                ('power_source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='harvester.powersource')),
            ],
        ),
    ]
