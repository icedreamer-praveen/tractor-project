from django.db import models

# Create your models here.

TOOL_CATEGORY = (
    ('Tarpaulin', 'Tarpaulin'),
    ('Brush Cutter', 'Brush Cutter'),
    ('Power Weeder', 'Power Weeder'),
    ('Earth Auger', 'Earth Auger'),
    ('Power Tiller', 'Power Tiller'),
    ('Sprayers', 'Sprayers'),
    ('Power Reaper', 'Power Reaper'),
    ('Mowers & Trimmers', 'Mowers & Trimmers'),
    ('Accessories', 'Accessories')
)


class ToolBrand(models.Model):
    name = models.CharField(max_length=250)
    image = models.ImageField(upload_to='tool image')

    def __str__(self):
        return self.name


class ToolCategory(models.Model):
    name = models.CharField(max_length=250, choices=TOOL_CATEGORY)
    image = models.ImageField(upload_to='tool category')

    def __str__(self):
        return self.name


class Images(models.Model):
    image = models.ImageField(upload_to='images')


class Tools(models.Model):
    STATUS = (
        ('In Stock', 'In Stock'),
        ('Upcoming', 'Upcoming'),
        ('Out of Stock', 'Out of Stock')
    )
    name = models.CharField(max_length=250)
    brand = models.ForeignKey(
        ToolBrand, on_delete=models.SET_NULL, null=True, blank=True)
    category = models.ForeignKey(
        ToolCategory, on_delete=models.SET_NULL, null=True, blank=True)
    model = models.CharField(max_length=250)
    thickness = models.CharField(max_length=250)
    size = models.CharField(max_length=250)
    price = models.DecimalField(max_digits=20, decimal_places=2)
    status = models.CharField(max_length=250, choices=STATUS)
    description = models.TextField()
    image = models.ManyToManyField(Images)
    color = models.CharField(max_length=250)

    def __str__(self):
        return self.name
