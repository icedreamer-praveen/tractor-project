from django.shortcuts import render

from django.views.generic import RedirectView, TemplateView, ListView, DetailView

from .models import *

from frontend.homemixins import FooterFormMixin


class ToolsView(FooterFormMixin, ListView):
    template_name = 'tool/tool.html'
    model = ToolCategory
    context_object_name = 'categories'

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(*args, **kwargs)
    #     context['categories'] = ToolCategory.objects.all()

    #     return context


class BrushCutterView(FooterFormMixin, ListView):
    template_name = 'tool/brush_cutter.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='brush cutter')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brush_brand'] = ToolBrand.objects.all()
        return context


class PowerWeederView(FooterFormMixin, ListView):
    template_name = 'tool/power_weeder.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='power weeder')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['weeder_brand'] = ToolBrand.objects.all()
        return context


class PowerTillerView(FooterFormMixin, ListView):
    template_name = 'tool/power-tiller.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='power tiller')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tiller_brand'] = ToolBrand.objects.all()
        return context


class SprayersView(FooterFormMixin, ListView):
    template_name = 'tool/sprayers.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='sprayer')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sprayer_brand'] = ToolBrand.objects.all()
        return context


class PowerReaperView(FooterFormMixin, ListView):
    template_name = 'tool/power-reaper.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='power reaper')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reaper_brand'] = ToolBrand.objects.all()
        return context


class EarthAugerView(FooterFormMixin, ListView):
    template_name = 'tool/earth-auger.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='earth auger')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['auger_brand'] = ToolBrand.objects.all()
        return context


class MowersTrimmersView(FooterFormMixin, ListView):
    template_name = 'tool/mowers-trimmers.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='mower')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['mower_brand'] = ToolBrand.objects.all()
        return context


class AccessoriesView(FooterFormMixin, ListView):
    template_name = 'tool/accessories.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='accessories')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            for key, value in self.request.GET.lists():
                if key == 'harbrand':
                    queryset = queryset.filter(brand__name__in=value)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['accesories_brand'] = ToolBrand.objects.all()
        return context


class TarpaulinView(FooterFormMixin, ListView):
    template_name = 'tool/tarpaulin.html'
    model = Tools

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(category__name__icontains='tarpaulin')
        if 'prices' in self.request.GET:
            if self.request.GET.get('prices') != '':
                prices = self.request.GET.get('prices')
                prices = prices.split('-')
                price_from = prices[0]
                price_to = prices[1]
                queryset = queryset.filter(price__range=(price_from, price_to))
        if 'harbrand' in self.request.GET:
            if self.request.GET.get('harbrand') != '':
                tarpaulin = self.request.GET.get('harbrand')
                queryset = queryset.filter(brand__name=tarpaulin)

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tarpaulin_brand'] = ToolBrand.objects.all()
        return context


class ToolDetailView(FooterFormMixin, DetailView):
    template_name = 'tool/tool_detail.html'
    model = Tools
    context_object_name = 'tool_detail'



class ToolSearchView(FooterFormMixin, TemplateView):
    template_name = 'tool/tool_search.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'keyword' in self.request.GET:
            kw = self.request.GET.get('keyword')
            results = Tools.objects.filter(Q(name__contains=kw) |
                                           Q(brand__name__contains=kw) |
                                           Q(category__name__contains=kw) |
                                           Q(model__contains=kw) |
                                           Q(description__contains=kw))
        context['results'] = results
        return context
