from django.contrib import admin
from .models import *

admin.site.register([ToolBrand, ToolCategory, Images, Tools])
# Register your models here.
