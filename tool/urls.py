from django.urls import path
from .views import *


urlpatterns = [
    path('tool/', ToolsView.as_view(), name='tool'),
    path('brush-cutter/', BrushCutterView.as_view(), name='brush-cutter'),
    path('power-weeder/', PowerWeederView.as_view(), name='power-weeder'),
    path('power-tiller/', PowerTillerView.as_view(), name='power-tiller'),
    path('sprayers/', SprayersView.as_view(), name='sprayers'),
    path('power-reaper/', PowerReaperView.as_view(), name='power-reaper'),
    path('earth-auger/', EarthAugerView.as_view(), name='earth-auger'),
    path('mowers-trimmers/', MowersTrimmersView.as_view(), name='mowers-trimmers'),
    path('accessories/', AccessoriesView.as_view(), name='accessories'),
    path('tarpaulin/', TarpaulinView.as_view(), name='tarpaulin'),
    path('tool/<int:pk>/detail/', ToolDetailView.as_view(), name='tool-detail'),

    path('tool/search/', ToolSearchView.as_view(), name='tool-search'),
]
