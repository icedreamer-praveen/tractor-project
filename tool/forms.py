from django import forms
from django.core.exceptions import ValidationError
from django_summernote.widgets import SummernoteWidget
from django.contrib import messages


from .models import *

from harvester.forms import FormControlMixin


class ToolBrandForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = ToolBrand
        fields = '__all__'
        widgets = {
            'image': forms.ClearableFileInput()
        }


class ImageForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Images
        fields = ['image']


class ToolCategoryForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = ToolCategory
        fields = '__all__'


class ToolsForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Tools
        fields = '__all__'
        widgets = {
            'description': SummernoteWidget()
        }
