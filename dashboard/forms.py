from harvester.models import BuyOldHarvester
from django import forms
from inventory.models import *
from customer.models import *
from inventory.models import *
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from customer.forms import FormControlMixin


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())


class FormControlMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({

                'class': 'form-control tractor-input'

            })


class BrandCreateForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Brand
        fields = '__all__'


class LubricantBrandForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = LubricantBrand
        fields = '__all__'
        widgets = {
            'image': forms.ClearableFileInput()
        }


class LubricantTypeForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = LubricantType
        fields = '__all__'


class LubricantDetailForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = LubricantDetail
        fields = '__all__'


class HomeSliderForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Slider
        fields = '__all__'


class QuestionUpdateForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ["name", "mob_no", "question"]


class ImplementBrandForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = ImplementBrand
        fields = ["name", "image"]


class ImplementTypeForm(forms.ModelForm):
    class Meta:
        model = ImplementType
        fields = ["name", "slug", "is_published"]
        widgets = {
            "name": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter name',
            }),
            "slug": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Slug',

            }),
            "is_published": forms.CheckboxInput(attrs={
                'class': 'form-control',
            }),
        }


class ImplementCategoryForm(forms.ModelForm):
    class Meta:
        model = ImplementCategory
        fields = ["name"]


class ImplementForm(forms.ModelForm):
    class Meta:
        model = Implement
        fields = ["name", "implementcategory","thumbnail", "brand", "slug", "implementtype",
                  "modelname", "implementpower", "description", "price", "image"]
        widgets = {
            "name": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter name',
            }),
            "implementcategory": forms.Select(attrs={
                'class': 'form-control',
            }),
            "thumbnail": forms.ClearableFileInput(attrs={
                'class': 'form-control',
            }),
            "brand": forms.Select(attrs={
                'class': 'form-control',
            }),
            "slug": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Slug',
            }),
            "implementtype": forms.Select(attrs={
                'class': 'form-control',
                'placeholder': 'Enter implement type',
            }),
            "implementpower": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter implement power',
            }),
            "description": forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Enter descriptions',
            }),
            "price": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter price',
            }),
            "modelname": forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter Model Name',
            }),
            "description": SummernoteWidget(),
        }


class BuyerInqueryForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = BuyOldTractor
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Enter your name'
            }),
            'contact': forms.TextInput(attrs={
                'placeholder': 'Enter your contact'
            }),
            'bid_price': forms.TextInput(attrs={
                'placeholder': 'Bid your price'
            })
        }


class BuyerOldImplementForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = BuyOldImplement
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Enter your name'
            }),
            'contact': forms.TextInput(attrs={
                'placeholder': 'Enter your contact'
            }),
            'bid_price': forms.TextInput(attrs={
                'placeholder': 'Bid your price'
            })
        }


class BuyerOldHarvesterForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = BuyOldHarvester
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={
                'placeholder': 'Enter your name'
            }),
            'contact': forms.TextInput(attrs={
                'placeholder': 'Enter your contact'
            }),
            'bid_price': forms.TextInput(attrs={
                'placeholder': 'Bid your price'
            })
        }


class WheelAndTyreForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = WheelAndTyre
        fields = '__all__'


class TractorTransmissionForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Transmission
        fields = '__all__'


class SteeringForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Steering
        fields = '__all__'


class PowerForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = Power
        fields = '__all__'


class OtherInfoForm(FormControlMixin, forms.ModelForm):
    class Meta:
        model = OtherInformation
        fields = '__all__'
