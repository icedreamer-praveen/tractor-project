from django.core.validators import ip_address_validator_map
from django.urls import path
from .views import *
from dashboard import views


urlpatterns = [
    path('admin-dashboard/', DashboardView.as_view(), name='admin-dashboard'),
    path('logout/', LogoutView.as_view(), name="logout"),

    # dynamic district listing
    path('district/list/', DistrictListView.as_view(), name='district-data'),

    # dynamic municipalities
    path('municipality/list/', MunicipalityListView.as_view(),
         name='municipality-data'),

    # many image url
    path('multiple/image/', MultipleImageCreateView.as_view(), name='multiple-image'),
    # brand create

    path('brand/create/', BrandCreateView.as_view(), name='brandcreate'),
    path('brand/<int:pk>/update/', BrandUpdateView.as_view(), name='brand-update'),
    path('brand/<int:pk>/delete/', BrandDeleteView.as_view(), name='brand-delete'),


    # seller detail
    path('seller-detail/', SellerCreateView.as_view(), name='seller-detail'),
    path('seller/<int:pk>/edit/', SellerDetailEditView.as_view(), name="seller-edit"),
    path('seller/<int:pk>/delete/',
         SellerDeleteView.as_view(), name='seller-delete'),


    # customer
    # question
    path('question/', CustomerQuestionCreateView.as_view(),
         name="question"),
    path('question/<int:pk>delete/', CustomerQuestionDeleteView.as_view(),
         name="question_delete"),
    path('question/<int:pk>/edit/',
         CustomerQuestionUpdateView.as_view(), name="question_edit"),


    # answer
    path('answer/', CustomerAnswerCreateView.as_view(), name='answer'),
    path('answer/<int:pk>/edit/',
         CustomerAnswerUpdateView.as_view(), name="answer_edit"),
    path('answer/<int:pk>/delete/',
         CustomerAnswerDeleteView.as_view(), name="answer_delete"),


    # insurance
    path('apply-insurance/', ApplyInsuranceCreateView.as_view(),
         name='apply-insurance'),
    path('apply-insurance/<int:pk>/edit/', ApplyInsuranceUpdateView.as_view(),
         name='apply-insurance-edit'),
    path('insurance-application/<int:pk>/delete/',
         ApplyInsuranceDeleteView.as_view(), name="d_insurance-application"),
    # apply-loan
    path('apply-loan/', ApplyLoanCreateView.as_view(), name='apply-loan'),
    path('loan-application/<int:pk>/edit/',
         ApplyLoanUpdateView.as_view(), name="loan_edit"),
    path('loan-application/<int:pk>/delete/',
         ApplyLoanDeleteView.as_view(), name="d_loan-application"),
    # finance scheme
    path('finance-scheme/', FinanceSchemeListView.as_view(), name='finance_scheme'),
    path('finance-scheme/create/', FinanceSchemeCreateView.as_view(),
         name='finance-scheme-create'),
    path('finance-scheme-/<int:pk>/-update/',
         FinanceSchemeUpdateView.as_view(), name='finance-scheme-update'),
    path('finance-scheme/<int:pk>/delete/', FinanceSchemeDeleteView.as_view(),
         name='finance-scheme-delete'),

    # buyers Inquery
    path('buyers-inquery/', BuyerInqueryCreateView.as_view(),
         name="buyers-inqueries"),
    path('buyers/inquery/<int:pk>/edit/',
         BuyerInqueryUpdateView.as_view(), name='buyers-inquery-update'),
    path('buyers-inquery/<int:pk>/delete/', BuyerInqueryDeleteView.as_view(),
         name="buyers-inquery-delete"),


    path('buyers/implement/inquery/', BuyOldImplementCreateView.as_view(),
         name="implement-inquery"),
    path('buyers/implement/inquery/<int:pk>/edit/',
         BuyOldImplementUpdateView.as_view(), name='implement-inquery-update'),
    path('buyers/implement/inquery/<int:pk>/delete/', BuyOldImplementDeleteView.as_view(),
         name="implement-inquery-delete"),
    # old harvester
    path('buyers/harvester/inquery/', BuyOldHarvesterCreateView.as_view(),
         name="harvester-inquery"),
    path('buyers/harvester/inquery/<int:pk>/edit/',
         BuyOldHarvesterUpdateView.as_view(), name='harvester-inquery-update'),

    path('buyers/harvester/inquery/<int:pk>/delete/', BuyOldHarvesterDeleteView.as_view(),
         name="harvester-inquery-delete"),
    # on road price
    path('get-on-road-price/', OnRoadPriceCreateView.as_view(),
         name='get-on-road-price'),
    path('get-on-road-price/<int:pk>/edit/',
         OnRoadPriceUpdateView.as_view(), name='on-road-price-update'),
    path('get-on-road-price/<int:pk>/delete/', OnRoadPriceDeleteView.as_view(),
         name='delete-on-road-price'),

    # tractor valuation
    path('tractor-valuation/', TractorValuationCreateView.as_view(),
         name='tractor-valuation'),
    path('tractor/valuation/<int:pk>/update/',
         TractorValuationUpdateView.as_view(), name='tractor-valuation-update'),
    path('tractor-valuation/<int:pk>/delete/', TractorValuationDeleteView.as_view(),
         name='tractor-valuation-delete'),

    # Dealership enquiry
    path('dealership-enquiry/', DealerShipEnqueryView.as_view(),
         name='dealership-enquiry'),
    path('dealership-enquery/<int:pk>/delete/', DealershipEnqueryDeleteView.as_view(),
         name="dealership-enq-delete"),

    # certified dealer
    path('certified/dealer/list/', CertifiedDealerView.as_view(),
         name='certified-dealer-list'),
    path('certified/dealer/<int:pk>/edit/',
         CertifiedDealerUpdateView.as_view(), name='certified-dealer-edit'),
    path('certified/dealer/<int:pk>/delete/',
         CertifiedDealerDeleteView.as_view(), name='certified-dealer-delete'),

    # newsletter
    path('newsletter/list/', NewsletterView.as_view(), name='newsletter'),
    path('newsletter/<int:pk>/delete/',
         NewsletterDeleteView.as_view(), name='newsletter-delete'),
    # News category
    path('news-category/', newsCatView.as_view(), name='news-category'),
    path('news-category-post/', newsCatPostView.as_view(),
         name='news-category-post'),
    path('newscatdel/<int:pk>/delete/',
         newsCatDelView.as_view(), name="newscatdel"),
    # News
    path('news-dashboard/', newsView.as_view(), name='news-dashboard'),
    path('news-edit/<int:pk>/', newsEditView.as_view(), name='newsedit'),
    path('news-edit-post/', NewsPostView.as_view(), name='news-post'),
    path('newsdelete/<int:pk>/delete/', newsDelView.as_view(), name="newsdelete"),
    # Post category
    path('post-category/', views.postCategoryView, name='post-category'),
    path('post-cat-edit/', views.postCatEditView, name="post-cat-edit"),
    path('post-cat-post/', views.postCatPostView, name="post-cat-post"),
    path('post-cat-del/', views.postCatDelView, name="post-cat-del"),
    # Post
    path('post/', views.postView, name='post'),
    path('post-edit/', views.postEditView, name="post-edit"),
    path('post-post/', views.postPostView, name="post-post"),
    path('post-del/', views.postDelView, name="post-del"),

    # HomeSlider
    path('adminslider/', HomeSliderView.as_view(), name='home-slider'),
    path('admin/slider-<int:pk>/delete/', HomeSliderDeleteView.as_view(),
         name='home-slider-delete'),






    # category
    path('inventory/category/', CategoryCreateView.as_view(), name='category'),
    path('inventory/category/<int:pk>/update',
         CategoryUpdateView.as_view(), name='category-update'),
    path('inventory/category/<int:pk>/delete/',
         CategoryDeleteView.as_view(), name='category-delete'),

    # dimension
    path('inventory/dimension/', DimensionAndWeightCreateView.as_view(),
         name='dimension-weights'),
    path('inventory/dimensions/<int:pk>/update',
         DimensionandWeightUpdateView.as_view(), name='dimension-update'),
    path('inventory/dimensions/<int:pk>/delete/',
         DimensionAndWeightDeleteView.as_view(), name='delete-dimensions'),


    # engines
    path('inventory/engine/', EngineCreateView.as_view(), name='engine'),
    path('inventory/engine/<int:pk>/update/',
         EngineUpdateView.as_view(), name='engine-update'),
    path('inventory/engine/<int:pk>/delete/',
         EngineDeleteView.as_view(), name='delete-engine'),

    # fueltank
    path('inventory/fuel-tank/', FuelTankCreateView.as_view(), name='fuel-tank'),
    path('inventory/fueltank/<int:pk>/update/',
         FuelTankUpdateView.as_view(), name="fueltank-update"),
    path('inventory/fueltank/<int:pk>/delete/',
         FuelTankDeleteView.as_view(), name='delete-fueltank'),

    # hydraulices
    path('inventory/hydraulices/',
         HydraulicCreateView.as_view(), name='hydraulices'),
    path('inventory/hydraulices/<int:pk>/update/',
         HydraulicUpdateView.as_view(), name='hydraulices-update'),
    path('inventory/hydraulices/<int:pk>/delete/',
         HydraulicDeleteView.as_view(), name='delete-hydraulices'),

    # images

    path('inventory/images/create/',
         ImageCreateView.as_view(), name='image-create'),
    path('inventory/images/<int:pk>/update/',
         ImageUpdateView.as_view(), name='image-update'),
    path('inventory/images/<int:pk>/delete/',
         ImageDeleteView.as_view(), name='image-delete'),



    # otherinformations
    path('inventory/other-information/',
         OtherInfoCreateView.as_view(), name='other-information'),
    path('inventory/otherinfo/<int:pk>/update/',
         OtherInfoUpdateView.as_view(), name='otherinfo-update'),
    path('inventory/otherinfo/<int:pk>/delete/',
         OtherInfoDeleteView.as_view(), name='delete-otherinfo'),

    # powers
    path('inventory/powers/', PowerCreateView.as_view(), name='powers'),
    path('inventory/power/<int:pk>/update/,',
         PowerUpdateView.as_view(), name='power-update'),
    path('inventory/power/<int:pk>/delete/',
         PowerDeleteView.as_view(), name='delete-power'),


    # sterrings
    path('inventory/steerings/', SteeringCreateView.as_view(), name='steerings'),
    path('inventory/steering/<int:pk>/update',
         SteeringUpdateView.as_view(), name='steering-update'),
    path('inventory/steering/<int:pk>/delete/',
         SteeringDeleteView.as_view(), name='delete-steering'),



    # tractors

    path('old/brand/create/', OldBrandCreateView.as_view(), name='old-brandcreate'),
    path('old/brand/<int:pk>/update/',
         OldBrandUpdateView.as_view(), name='old-brand-update'),
    path('old/brand/<int:pk>/delete/',
         OldBrandDeleteView.as_view(), name='old-brand-delete'),

    # old tractors
    #     path('inventory/old-tractors/create/', OldTractorCreateView.as_view(), name='oldtractor-create'),
    path('inventory/old-tractors/',
         OldTractorListView.as_view(), name='old-tractors'),
    path('inventory/add-old-tractors/',
         addOldTractors.as_view(), name='add-old-tractors'),
    path('inventory/<int:pk>/edit-old-tractors/',
         EditOldTractorsView.as_view(), name='edit-old-tractors'),
    path('inventory/old/tractor/<int:pk>/detail/',
         OldTractorDetailView.as_view(), name='admin-old-tractor-detail'),
    path('inventory/old-tractor/<int:pk>/delete/',
         OldTractorDeleteView.as_view(), name='delete-old-tractors'),



    path('inventory/tractors/', TractorsView.as_view(), name='tractors'),
    path('inventory/add-tractors/', AddTractorsView.as_view(), name='add-tractors'),
    path('inventory/update-tractors-<int:pk>/',
         UpdateTractorsView.as_view(), name='update-tractors'),
    path('inventory/detail-tractor-<int:pk>/',
         DetailTractorView.as_view(), name='detail-tractor'),
    path('inventory/delete-<int:pk>/',
         DeleteTractorView.as_view(), name="deletetractor"),
    path('inventory/tractor-image-update-<int:pk>/',
         AdminTractorImageUpdateView.as_view(), name="tractor-image-update"),
    path('inventory/delete-<int:pk>/',
         DeleteTractorView.as_view(), name="deletetractor"),


    # transmission
    path('inventory/transmission/',
         TransmissionCreateView.as_view(), name='transmission'),
    path('inventory/transmission/<int:pk>/update/',
         TransmissionUpdateView.as_view(), name="transmission-update"),
    path('inventory/transmission/<int:pk>/delete/', TransmissionDeleteView.as_view(),
         name="transmissiondelete"),



    # wheel and tyre
    path('inventory/wheel-tyre/',
         WheelAndTyreCreateView.as_view(), name='wheel-tyre'),
    path('inventory/wheelandtyre/<int:pk>/update/',
         WheelAndTyreUpdateView.as_view(), name='wheelandtyre-update'),
    path('inventory/wheelandtyre/<int:pk>/delete/',
         WheelAndTyreDeleteView.as_view(), name='delete-wheelandtyre'),



    # implements
    path('admin-implements/', ImplementView.as_view(), name="admin-implements"),
    path('admin-implements-add/', ImplementAddView.as_view(),
         name="admin-implements-add"),
    path('admin-implements-edit-<int:pk>/',
         ImplementEditView.as_view(), name="admin-implements-edit"),
    path('admin-implements/<int:pk>/detail/',
         ImplementDetailView.as_view(), name='admin-implement-detail'),
    path('admin-implements-delete/<int:pk>/delete/', ImplementDeleteView.as_view(),
         name="admin-implements-delete"),

    path('admin-implement-brand/', ImplementBrandView.as_view(),
         name="admin-implement-brand"),
    path('admin-implement-brand-list/', ImplementBrandListView.as_view(),
         name="admin-implement-brand-list"),
    path('admin-implement-brand-update-<int:pk>/',
         ImplementBrandUpdateView.as_view(), name="implement-brand-update"),

    path('admin-implement-category/', ImplementCategoryView.as_view(),
         name="admin-implement-category"),
    path('admin-implement-category-list/', ImplementCategoryListView.as_view(),
         name="admin-implement-category-list"),
    path('admin-implement-type-add/',
         ImplementAddView.as_view(), name="implement-type-add"),

    path('admin-implement-type/', ImplementTypeView.as_view(),
         name="admin-implement-type"),
    path('admin-implement-type-list/', ImplementTypeListView.as_view(),
         name="admin-implement-type-list"),


    # Tractor tyre
    path('admin/tractor/tyre/brand/',
         TractorTyreBrandView.as_view(), name='tractor-tyre-brand'),
    path('admin/tractor/tyre/brand/<int:pk>/edit/',
         TractorTyreBrandUpdateView.as_view(), name='tractor-tyre-brand-edit'),
    path('admin/tractor/tyre/brand/<int:pk>/delete/',
         TractorTyreBrandDeleteView.as_view(), name='tractor-tyre-brand-delete'),

    path('admin/tractor/tyre/',
         TractorTyreView.as_view(), name='tractor-tyre'),
    path('admin/tractor/tyre/<int:pk>/edit/',
         TractorTyreUpdateView.as_view(), name='tractor-tyre-edit'),
    path('admin/tractor/tyre/<int:pk>/detail/',
         TractorTyreDetailView.as_view(), name='tractor-tyre-detail'),
    path('admin/tractor/tyre/<int:pk>/delete/',
         TractorTyreDeleteView.as_view(), name='tractor-tyre-delete'),




    # lubricant
    path('lubricant/brand/create/', LubricantBrandCreateView.as_view(),
         name='lubricant-brand-create'),
    path('lubricant-brand/<int:pk>/update/',
         LubricantBrandUpdateView.as_view(), name='lubricant-brand-update'),
    path('lubricant-brand/<int:pk>/delete/',
         LubricantBrandDeleteView.as_view(), name='lubricant-brand-delete'),

    path('lubricant/type/create/', LubricantTypeCreateView.as_view(),
         name='lubricant-type-create'),
    path('lubricant-type/<int:pk>/update/',
         LubricantTypeUpdateView.as_view(), name='lubricant-type-update'),
    path('lubricant-type/<int:pk>/delete/',
         LubricantTypeDeleteView.as_view(), name='lubricant-type-delete'),

    path('lubricant/detail/create/', LubricantDetailCreateView.as_view(),
         name='lubricant-detail-create'),
    path('lubricant-detail/<int:pk>/update/',
         LubricantDetailUpdateView.as_view(), name='lubricant-detail-update'),
    path('admin/lubricant/<int:pk>/detail/', LubricantDetailView.as_view(),
         name='admin-lubricant-detail'),
    path('lubricant-detail/<int:pk>/delete/',
         LubricantDetailDeleteView.as_view(), name='lubricant-detail-delete'),

    path('lubricant/brand/create/', LubricantBrandCreateView.as_view(),
         name='lubricant-brand-create'),
    path('lubricant-brand/<int:pk>/update/',
         LubricantBrandUpdateView.as_view(), name='lubricant-brand-update'),
    path('lubricant-brand/<int:pk>/delete/',
         LubricantBrandDeleteView.as_view(), name='lubricant-brand-delete'),

    path('lubricant/type/create/', LubricantTypeCreateView.as_view(),
         name='lubricant-type-create'),
    path('lubricant-type/<int:pk>/delete/',
         LubricantTypeDeleteView.as_view(), name='lubricant-type-delete'),

    path('lubricant/detail/create/', LubricantDetailCreateView.as_view(),
         name='lubricant-detail-create'),

    path('lubricant-detail/<int:pk>/delete/',
         LubricantDetailDeleteView.as_view(), name='lubricant-detail-delete'),

    # video
    path('admin-video/',
         VideoCreateView.as_view(), name='video-lists'),
    path('admin-video/<int:pk>/update/',
         VideoUpdateView.as_view(), name='video-update'),
    path('admin-video/<int:pk>/delete/',
         VideoDeleteView.as_view(), name='video-delete'),

    path('admin-video-category/',
         VideoCategoryCreateView.as_view(), name='video-category'),
    path('admin-video-category/<int:pk>/update/',
         VideoCategoryUpdateView.as_view(), name='video-category-update'),
    path('admin-video-category/<int:pk>/delete/',
         VideoCategoryDeleteView.as_view(), name='video-category-delete'),
    # tool
    path('admin/tool/brand/', ToolBrandCreateView.as_view(), name='tool-brand'),
    path('admin/tool/brand/<int:pk>/update/',
         ToolBrandUpdateView.as_view(), name='tool-brand-update'),
    path('admin/tool/brand/<int:pk>/delete/',
         ToolBrandDeleteView.as_view(), name='tool-brand-delete'),

    path('admin/tool/category/',
         ToolCategoryCreateView.as_view(), name='tool-category'),
    path('admin/tool/category/<int:pk>/update/',
         ToolCategoryUpdateView.as_view(), name='tool-category-update'),
    path('admin/tool/category/<int:pk>/delete/',
         ToolCategoryDeleteView.as_view(), name='tool-category-delete'),

    path('admin/tool/list/', ToolCreateView.as_view(), name='admin-tool-list'),
    path('admn/tool/<int:pk>/update/',
         ToolUpdateView.as_view(), name='tool-update'),
    path('admin/tool/<int:pk>/detail/',
         ToolDetailView.as_view(), name='admin-tool-detail'),
    path('admin/tool/<int:pk>/delete/',
         ToolDeleteView.as_view(), name='tool-delete'),
    # harvestor
    path('dashboard/harvestor/engine/',
         EngineHarvestorView.as_view(), name='harvestor-engine'),
    path('dashboard/harvester/engine/<int:pk>/update/',
         EngineHarvesterUpdateView.as_view(), name='harvester-engine-update'),
    path('dashboard/harvester/engine/<int:pk>/delete/',
         EngineHarvesterDeleteView.as_view(), name='harvester-engine-delete'),

    path('dashboard/harvestor/clutch/',
         ClutchHarvestorView.as_view(), name='harvestor-clutch'),
    path('dashboard/harvestor/clutch/<int:pk>/update/',
         ClutchHarvestorUpdateView.as_view(), name='harvester-clutch-update'),
    path('dashboard/harvestor/clutch/<int:pk>/delete/',
         ClutchHarvestorDeleteView.as_view(), name="harvestor-clutch-delete"),

    path('dashboard/harvestor/transmission/',
         TransmissionHarvestorView.as_view(), name='harvestor-transmission'),
    path('dashboard/harvestor/transmission/<int:pk>/update/',
         TransmissionHarvestorUpdateView.as_view(), name='harvestor-transmission-update'),
    path('dashboard/harvestor/transmission/<int:pk>/delete/',
         TransmissionHarvestorDeleteView.as_view(), name='harvestor-transmission-delete'),

    path('dashboard/harvestor/cutterbar/',
         CutterbarHarvestorView.as_view(), name='harvestor-cutterbar'),
    path('dashboard/harvestor/cutterbar/<int:pk>/update/',
         CutterbarHarvestorUpdateView.as_view(), name='harvestor-cutterbar-update'),
    path('dashboard/harvestor/cutterbar/<int:pk>/delete/',
         CutterbarHarvestorDeleteView.as_view(), name='harvestor-cutterbar-delete'),

    path('dashboard/harvestor/straw-walker',
         StrawWalkerHarvestorView.as_view(), name='harvestor-straw-walker'),
    path('dashboard/harvestor/straw-walker/<int:pk>/update/',
         StrawWalkerHarvestorUpdateView.as_view(), name='harvestor-straw-walker-update'),
    path('dashboard/harvestor/straw-walker/<int:pk>/delete/',
         StraWalkerHarvestorDeleteView.as_view(), name='harvestor-straw-walker-delete'),

    path('dashboard/harvestor/cleaning/',
         CleaningSievesHarvestorView.as_view(), name='harvestor-cleaning'),
    path('dashboard/harvestor/cleaning/<int:pk>/update/',
         CleaningSievesHarvestorUpdateView.as_view(), name='harvestor-cleaning-update'),
    path('dashboard/harvestor/cleaning/<int:pk>/delete/',
         CleaningSievesHarvestorDeleteView.as_view(), name='harvestor-cleaning-delete'),

    path('dashboard/harvestor/capacity/',
         CapacityHarvestorView.as_view(), name='harvestor-capacity'),
    path('dashboard/harvestor/capacity/<int:pk>/update',
         CapacityHarvestorUpadateView.as_view(), name='harvestor-capacity-update'),
    path('dashboard/harvestor/capacity/<int:pk>/delete/',
         CapacityHarvestorDeleteView.as_view(), name='harvestor-capacity-delete'),

    path('dashboard/harvestor/battery/',
         BatteryHarvestorView.as_view(), name='harvestor-battery'),
    path('dashboard/harvestor/battery/<int:pk>/update/',
         BatteryHarvestorUpdateView.as_view(), name='harvestor-battery-update'),
    path('dashboard/harvestor/battery/<int:pk>/delete/',
         BatteryHarvestorDeleteView.as_view(), name='harvestor-battery-delete'),

    path('dashboard/harvestor/dimensions/',
         DimensionsHarvestorView.as_view(), name='harvestor-dimensions'),
    path('dashboard/harvestor/dimensions/<int:pk>/update',
         DimensionHarvestorUpdateView.as_view(), name='harvestor-dimensions-update'),
    path('dashboard/harvestor/dimensions/<int:pk>/delete/',
         MainDimensionsHarvestorDeleteView.as_view(), name='harvestor-dimensions-delete'),

    path('dashboard/harvestor/concave/',
         ConcaveHarvestorView.as_view(), name='harvestor-concave'),
    path('dashboard/harvestor/concave/<int:pk>/update/',
         ConcaveHarvestorUpdateView.as_view(), name='harvestor-concave-update'),
    path('dashboard/harvestor/concave/<int:pk>/delete/',
         ConcaveHarvestorDeleteView.as_view(), name='harvestor-concave-delete'),

    path('dashboard/harvestor/tyre',
         TyreHarvestorView.as_view(), name='harvestor-tyre'),
    path('dashboard/harvestor/tyre/<int:pk>/update/',
         TyreHarvestorUpdateView.as_view(), name='harvestor-tyre-update'),
    path('dashboard/harvestor/tyre/<int:pk>/delete/',
         TyreHarvestorDeleteView.as_view(), name='harvestor-tyre-delete'),

    path('dashboard/harvestor/threshing',
         ThreshingHarvestorView.as_view(), name='harvestor-threshing'),
    path('dashboard/harvestor/threshing/<int:pk>/update/',
         ThreshingHarvestorUpdateView.as_view(), name='harvestor-threshing-update'),
    path('dashboard/harvestor/threshing/<int:pk>/delete/',
         ThreshingDrumHarvestorDeleteView.as_view(), name='harvestor-threshing-delete'),

    path('dashboard/harvester/brand/',
         HarvesterBrandCreateView.as_view(), name='harvester-brand'),
    path('dashboard/harvester/brand/<int:pk>/update/',
         HarvestorBrandUpdateView.as_view(), name='harvester-barnd-update'),
    path('dashboard/harvester/brand/<int:pk>/delete/',
         HarvesterBrandDeleteView.as_view(), name='harvester-brand-delete'),

    path('dashboard/harvester/powersource/',
         HarvesterPowerSourceCreateView.as_view(), name='harvester-power-source'),
    path('dashboard/harvester/powersource/<int:pk>/update/',
         HarvesterPowerSourceUpdateView.as_view(), name='harvester-power-source-update'),
    path('dashboard/harvester/powersource/<int:pk>/delete/',
         PowerSourceDeleteView.as_view(), name='harvester-power-source-delete'),

    path('dashboard/harvestor', HarvestorView.as_view(), name='harvestor'),
    path('dashboard/harvestor/add',
         AddHarvestorView.as_view(), name='harvestor-add'),
    path('dashboard/harvestor/<int:pk>/edit',
         EditHarvestorView.as_view(), name='harvestor-edit'),
    path('dashboard/harvester/<int:pk>/detail/',
         HarvestorDetailView.as_view(), name='admin-harvestor-detail'),
    path('dashboard/harvestor/<int:pk>/delete/',
         HarvestorDeleteView.as_view(), name='harvestor-delete'),


    # user url
    path('dashboard/admin-user', AdminCreateview.as_view(), name='admin-user-list'),
    path('dashboard/admin-user/<int:pk>/edit/',
         AdminUpdateView.as_view(), name='admin-user-update'),

    path('dashboard/admin-user/<int:pk>/delete/',
         AdminDeleteView.as_view(), name='admin-user-delete'),
    path('dashboard/admin/<int:pk>/password/reset/',
         PasswordResetView.as_view(), name='reset-admin-password'),
    path('dashboard/password/update/',
         PasswordsChangeView.as_view(), name="update-password"),

    path('password-forgot/', ForgotPasswordView.as_view(), name='forgot-password'),

    path('dashboard/customer/profile/list/',
         CustomerProfileListView.as_view(), name='customer-profile-list'),
    path('dashboard/customer/<int:pk>/profile/delete/',
         CustomerProfileDeleteView.as_view(), name='customer-profile-delete'),

    # old implement
    path('dashboard/old/implement/list/',
         OldImplementListView.as_view(), name='old-implement-list'),
    path('dashboard/old/implement/<int:pk>/detail/',
         OldImplementDetailView.as_view(), name='admin-old-implement-detail'),

    path('dashboard/old/implement/<int:pk>/list/',
         OldImplementDeleteView.as_view(), name='old-implement-delete'),

    # old harvester
    path('dashboard/old/harveser/list/',
         OldHarvesterListView.as_view(), name='old-harvester-list'),

    path('dashboard/old/harveter/<int:pk>/detail/',
         OldHarvesterDetailView.as_view(), name='admin-old-harvester-detail'),
    path('dashboard/old/harvester/<int:pk>/delete/',
         OldHarvesterDeleteView.as_view(), name='old-harvester-delete'),

    path('dashboard/broker/dealer/',
         BrokerDealerView.as_view(), name='broker-dealer'),

    path('dashboard/broker/dealer/<int:pk>/edit/',
         BrokerDealerUpadteView.as_view(), name='broker-dealer-edit'),
    path('dashboard/broker/dealer/<int:pk>/delete/',
         BrokerDealerDeleteView.as_view(), name='broker-dealer-delete'),
]
