from django.core.exceptions import PermissionDenied

from django.contrib.auth.models import User, Group


class DeleteMixin(object):
    def get(self, *args, **kwargs):
        return self.delete(*args, **kwargs)


class SuperAdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_superuser:
            pass
        else:

            raise PermissionDenied

        return super().dispatch(request, *args, *kwargs)
