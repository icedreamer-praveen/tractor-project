from django.db import models
from dashboard.choices import DISTRICT_PROVINCE_CHOICES, PROVINCE_CHOICES
# Create your models here.


class State(models.Model):
    state = models.CharField(max_length=250)

    def __str__(self):
        return self.state


class District(models.Model):
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.CharField(max_length=250)

    def __str__(self):
        return self.district


class Municipality(models.Model):
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    municipality = models.CharField(max_length=250)

    class Meta:
        verbose_name = 'Municipality'
        verbose_name_plural = 'Municipalities'

    def __str__(self):
        return f'{self.district} --> {self.municipality}'
