from django.core.management.base import BaseCommand
from dashboard.choices import MUNICIPALITY_DISTRICT_CHOICES
from dashboard.models import District, State, Municipality


class Command(BaseCommand):
    help = 'Populate municipality in database'

    def handle(self, *args, **kwargs):
        for key, value in MUNICIPALITY_DISTRICT_CHOICES.items():
            district = District.objects.get(district=value)
            Municipality.objects.create(municipality=key, district=district)
        print('created')
