from django.core.management.base import BaseCommand
from dashboard.choices import PROVINCE_CHOICES, DISTRICT_PROVINCE_CHOICES
from dashboard.models import State


class Command(BaseCommand):
    help = 'Populate Provinces'

    def handle(self, *args, **kwargs):

        for province in PROVINCE_CHOICES:
            State.objects.create(state=province[0])
        print('created')
