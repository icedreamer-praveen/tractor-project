from django.core.management.base import BaseCommand
from dashboard.choices import PROVINCE_CHOICES, DISTRICT_PROVINCE_CHOICES
from dashboard.models import District, State


class Command(BaseCommand):
    help = 'Populate district in database'

    def handle(self, *args, **kwargs):
        for key, value in DISTRICT_PROVINCE_CHOICES.items():
            province = State.objects.get(state=value)
            District.objects.create(district=key, state=province)
        print('created')
