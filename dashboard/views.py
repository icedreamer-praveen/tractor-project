from django.db.models.functions import Lower
from django.shortcuts import (get_object_or_404, HttpResponseRedirect)
from re import template
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from customer.forms import *
from inventory.forms import *
from inventory.models import *
from users.forms import *
from customer.models import *
from dealer.models import *
from dealer.forms import BrokerDealerForm, CertifiedDealerForm
from users.models import *
from django.views.generic import *
from django.http import JsonResponse
from django.utils.crypto import get_random_string
from django.conf import settings as conf_settings
from django.core.mail import send_mail
from django.contrib import messages
from frontend.models import *
from frontend.forms import *
from .forms import *
from .models import *
from tool.forms import *
from tool.models import *
from harvester.models import *
from harvester.forms import *
from .mixins import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordChangeView
from django.contrib.auth.hashers import check_password
from django.contrib.auth import get_user_model
User = get_user_model()


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("login")


class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.filter(name="Admin").exists():
            pass
        else:
            return redirect("/login/")

        return super().dispatch(request, *args, **kwargs)


class DashboardView(AdminRequiredMixin, TemplateView):
    login_url = "/login/"
    template_name = 'admin/dashboard.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        num_oldTractor = OldTractor.objects.all().count()
        num_newTractor = Tractor.objects.all().count()
        num_brand = Brand.objects.all().count()
        num_dealershipEnquiry = DealershipEnquiry.objects.all().count()
        context['num_oldTractor'] = num_oldTractor
        context['num_newTractor'] = num_newTractor
        context['num_brand'] = num_brand
        context['num_dealershipEnquiry'] = num_dealershipEnquiry
        return context

# dynamic district listing with state


class DistrictListView(View):
    model = District
    template_name = 'admin/district-ajax-view.html'

    def get(self, *args, **kwargs):
        id = self.request.GET.get('state')
        districts = District.objects.filter(
            state__id=id)
        return render(self.request, self.template_name, {"districts": districts})


class MunicipalityListView(View):
    model = Municipality
    template_name = 'admin/municipality-ajax-view.html'

    def get(self, *args, **kwargs):
        id = self.request.GET.get('district')
        munincipalities = Municipality.objects.filter(
            district__id=id)
        return render(self.request, self.template_name, {"munincipalities": munincipalities})


class MultipleImageCreateView(AdminRequiredMixin, CreateView):
    model = Images
    form_class = ImageForm
    template_name = "dashboard/admin/image.html"

    def dispatch(self, request, *args, **kwargs):
        if self.request.is_ajax():
            return super().dispatch(request, *args, **kwargs)
        return JsonResponse({"error": "Cannot access this page"}, status=404)

    def form_valid(self, form):
        instance = form.save()
        return JsonResponse(
            {
                "status": "ok",
                "pk": instance.pk,
                "url": instance.image.url,
            }
        )

    def form_invalid(self, form):
        return JsonResponse({"errors": form.errors}, status=400)

# Brand Create


class BrandCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/brand/brandcreate.html'
    form_class = BrandCreateForm
    success_url = reverse_lazy('brandcreate')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allbrands'] = Brand.objects.all().order_by('-id')
        return context


class BrandUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/brand/brand-update.html'
    form_class = BrandCreateForm
    model = Brand
    success_url = reverse_lazy("brandcreate")


# Brand delete

class BrandDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Brand
    success_url = reverse_lazy('brandcreate')


class HomeSliderView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/slider.html'
    form_class = HomeSliderForm
    success_url = reverse_lazy('home-slider')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allslider'] = Slider.objects.all().order_by('-id')

        return context


class HomeSliderDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Slider
    success_url = reverse_lazy('home-slider')


# Users / seller detail

# Users / seller detail
class SellerCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/users/seller-detail.html'
    form_class = SellerDetailForm
    success_url = reverse_lazy('seller-detail')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allsellers'] = SellerDetail.objects.all().order_by('-id')

        return context


class SellerDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = SellerDetail
    success_url = reverse_lazy('seller-detail')

# seller edit


class SellerDetailEditView(UpdateView):
    template_name = 'admin/users/seller-detail-edit.html'
    form_class = SellerDetailForm
    model = SellerDetail
    success_url = reverse_lazy('seller-detail')


# customer / question
class CustomerQuestionCreateView(CreateView):
    login_url = "/login/"
    template_name = 'admin/customer/question.html'
    form_class = QuestionForm
    success_url = reverse_lazy('question')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['questionList'] = Question.objects.all().order_by('-id')
        return context


class CustomerQuestionDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Question
    success_url = reverse_lazy('question')


class CustomerQuestionUpdateView(UpdateView):
    login_url = "/login/"
    template_name = 'admin/customer/question-edit.html'
    model = Question
    form_class = QuestionForm
    success_url = reverse_lazy('question')
# customer / answer


class CustomerAnswerCreateView(CreateView):
    template_name = 'admin/customer/answer.html'
    form_class = AnswerForm
    success_url = reverse_lazy('answer')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['answerList'] = Answer.objects.all().order_by('-id')
        return context


class CustomerAnswerDeleteView(DeleteMixin, DeleteView):
    model = Answer
    success_url = reverse_lazy('answer')


class CustomerAnswerUpdateView(UpdateView):
    template_name = 'admin/customer/answer-edit.html'
    form_class = AnswerForm
    model = Answer
    success_url = reverse_lazy('answer')


# customer / Insurance
class ApplyInsuranceCreateView(CreateView):
    template_name = 'admin/customer/insurance-apply.html'
    form_class = ApplyInsuranceForm
    success_url = reverse_lazy('apply-insurance')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['insuranceList'] = ApplyInsurance.objects.all().order_by('-id')
        return context


class ApplyInsuranceDeleteView(DeleteMixin, DeleteView):
    model = ApplyInsurance
    success_url = reverse_lazy('apply-insurance')


class ApplyInsuranceUpdateView(UpdateView):
    template_name = 'admin/customer/insurance-apply-edit.html'
    model = ApplyInsurance
    form_class = ApplyInsuranceForm
    success_url = reverse_lazy('apply-insurance')


# customer / loan
class ApplyLoanCreateView(CreateView):
    template_name = 'admin/customer/loan-apply.html'
    form_class = ApplyLoanForm
    success_url = reverse_lazy('apply-loan')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['loan_applyList'] = ApplyLoan.objects.all().order_by('-id')

        return context


class ApplyLoanDeleteView(DeleteMixin, DeleteView):
    model = ApplyLoan
    success_url = reverse_lazy('apply-loan')


class ApplyLoanUpdateView(UpdateView):
    template_name = 'admin/customer/loan-apply-edit.html'
    model = ApplyLoan
    form_class = ApplyLoanForm
    success_url = reverse_lazy('apply-loan')


# get on road price
class OnRoadPriceCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/customer/on_road_price.html'
    form_class = OnRoadPriceForm
    success_url = reverse_lazy('get-on-road-price')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['road_price'] = OnRoadPrice.objects.all().order_by('-id')
        return context


class OnRoadPriceDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = OnRoadPrice
    success_url = reverse_lazy('get-on-road-price')


class OnRoadPriceUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/customer/on_road_price-update.html'
    model = OnRoadPrice
    form_class = OnRoadPriceForm
    success_url = reverse_lazy('get-on-road-price')


class FinanceSchemeListView(ListView):
    template_name = 'admin/customer/finance_scheme.html'
    model = FinanceScheme
    context_object_name = 'finances'

# finance scheme


class FinanceSchemeCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/customer/finance_scheme_create.html'
    form_class = FinanceSchemeForm
    success_url = '/finance-scheme/'


class FinanceSchemeUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/customer/finance_scheme_create.html'
    model = FinanceScheme
    form_class = FinanceSchemeForm
    success_url = '/finance-scheme/'


class FinanceSchemeDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = FinanceScheme
    success_url = reverse_lazy('finance_scheme')


# tractor valuation
class TractorValuationCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/customer/tractor_valuation.html'
    form_class = TractorValuationForm
    success_url = reverse_lazy('tractor-valuation')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['tractor_val'] = TractorValuation.objects.all().order_by('-id')
        return context


class TractorValuationUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/customer/tractor_valuation-edit.html'
    model = TractorValuation
    form_class = TractorValuationForm
    success_url = reverse_lazy('tractor-valuation')


class TractorValuationDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = TractorValuation
    success_url = reverse_lazy('tractor-valuation')


# Buyers Inquery

class BuyerInqueryCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admin/customer/buyers_inquery.html'
    form_class = BuyerInqueryForm
    success_url = reverse_lazy('buyers-inqueries')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['buyers'] = BuyOldTractor.objects.all().order_by('-id')
        return context


class BuyerInqueryUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admin/customer/buyers_inquery-edit.html'
    model = BuyOldTractor
    form_class = BuyerInqueryForm
    success_url = reverse_lazy('buyers-inqueries')


class BuyerInqueryDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    model = BuyOldTractor
    success_url = reverse_lazy('buyers-inqueries')


# old harvester
class BuyOldHarvesterCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admin/customer/buy_harvester_inquery.html'
    form_class = BuyerOldHarvesterForm
    success_url = reverse_lazy('harvester-inquery')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['buyers'] = BuyOldHarvester.objects.all().order_by('-id')
        return context


class BuyOldHarvesterUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admin/customer/buy_harvester_inquery_edit.html'
    form_class = BuyerOldHarvesterForm
    model = BuyOldHarvester
    success_url = reverse_lazy('harvester-inquery')


class BuyOldHarvesterDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    model = BuyOldHarvester
    success_url = reverse_lazy('harvester-inquery')


# Old implement
class BuyOldImplementCreateView(AdminRequiredMixin, CreateView):
    template_name = 'admin/customer/buy_implement_inquery.html'
    form_class = BuyerOldImplementForm
    success_url = reverse_lazy('implement-inquery')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['buyers'] = BuyOldImplement.objects.all().order_by('-id')
        return context


class BuyOldImplementUpdateView(AdminRequiredMixin, UpdateView):
    template_name = 'admin/customer/buy_implement_inquery_edit.html'
    form_class = BuyerOldImplementForm
    model = BuyOldImplement
    success_url = reverse_lazy('implement-inquery')


class BuyOldImplementDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    model = BuyOldImplement
    success_url = reverse_lazy('implement-inquery')

# Dealership enquiry


class DealerShipEnqueryView(AdminRequiredMixin, ListView):
    login_url = "/login/"
    template_name = 'admin/frontend/dealership-enquiry.html'
    model = DealershipEnquiry
    success_url = reverse_lazy('dealership-enquiry')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['enquiries'] = DealershipEnquiry.objects.all()

        return context


class DealershipEnqueryDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = DealershipEnquiry
    success_url = reverse_lazy('dealership-enquiry')


class CertifiedDealerView(CreateView):
    template_name = 'admin/dealer/certified-dealer.html'
    form_class = CertifiedDealerForm
    success_url = reverse_lazy('certified-dealer-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = CertifiedDealer.objects.all().order_by('-id')
        return context


class CertifiedDealerUpdateView(UpdateView):
    template_name = 'admin/dealer/certified-dealer-edit.html'
    model = CertifiedDealer
    form_class = CertifiedDealerForm
    success_url = reverse_lazy('certified-dealer-list')


class CertifiedDealerDeleteView(DeleteMixin, DeleteView):
    model = CertifiedDealer
    success_url = reverse_lazy('certified-dealer-list')


class NewsletterView(AdminRequiredMixin, ListView):
    login_url = "/login/"
    template_name = 'admin/frontend/newsletter.html'
    model = Newsletter


class NewsletterDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Newsletter
    success_url = reverse_lazy('newsletter')


class newsCatDelView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = NewsCategory
    success_url = reverse_lazy('news-category')


# News
class newsView(AdminRequiredMixin, ListView):
    login_url = "/login/"
    template_name = 'admin/frontend/news.html'
    queryset = News.objects.all().order_by('-id')
    context_object_name = 'newslists'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = NewsForm

        return context


class NewsPostView(AdminRequiredMixin, View):
    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            form = NewsForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()

        return redirect('/news-dashboard/')


class newsEditView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/frontend/newsedit.html'
    form_class = NewsForm
    model = News
    success_url = "/news-dashboard/"


class newsDelView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = News
    success_url = reverse_lazy('news-dashboard')


class newsCatView(AdminRequiredMixin, TemplateView):
    login_url = "/login/"
    template_name = 'admin/frontend/news-category.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context["newscategories"] = NewsCategory.objects.all()
        context["form"] = NewsCatForm

        return context


class newsCatPostView(AdminRequiredMixin, View):
    login_url = "/login/"

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            if not request.POST.get('id'):
                form = NewsCatForm(request.POST)
                if form.is_valid():
                    form.save()
            else:
                id = request.POST.get('id')
                name = request.POST.get('name')
                slug = request.POST.get('slug')
                order_num = request.POST.get('order_num')
                newscat_obj = NewsCategory.objects.get(id=id)
                newscat_obj.name = name
                newscat_obj.slug = slug
                newscat_obj.order_num = order_num
                newscat_obj.save()

        return redirect("news-category")


# Post


def postView(request):
    post = Post.objects.all()
    cats = PostCategory.objects.all()
    return render(request, 'admin/frontend/post.html', {'posts': post, 'category': cats})


def postPostView(request):
    if request.method == "POST":
        title = request.POST.get('title')
        text = request.POST.get('text')
        # request.FILES used for to get files
        image = request.FILES.get('image')
        cat_id = request.POST.get('category')
        category = PostCategory.objects.get(id=cat_id)
        post = Post.objects.create(
            title=title, text=text, image=image, category=category)

        response = {
            'status': "1 record is posted!"
        }
        return JsonResponse(response)


def postEditView(request):
    if request.method == "POST":
        id = request.POST.get('id')
        title = request.POST.get('title')
        text = request.POST.get('text')
        # request.FILES used for to get files
        image = request.FILES.get('image')
        cat_id = request.POST.get('category')
        category = PostCategory.objects.get(id=cat_id)
        obj = Post.objects.get(pk=id)
        obj.title = title
        obj.text = text
        obj.image = image
        obj.category = category
        obj.save()

        response = {
            'status': "Record Edited!"
        }
        return JsonResponse(response)


def postDelView(request):
    if request.method == "POST":
        id = request.POST['id']
        obj = Post.objects.get(pk=id)
        obj.delete()

        response = {
            'status': 'Deleted!'
        }

        return JsonResponse(response)

# Post category


def postCategoryView(request):
    postcategory = PostCategory.objects.all()
    return render(request, 'admin/frontend/post-category.html', {'postcategories': postcategory})


def postCatPostView(request):
    if request.method == "POST":
        nm = request.POST.get('name')
        postcatpost = PostCategory.objects.create(name=nm)

        response = {
            'status': "1 record is posted!"
        }
        return JsonResponse(response)


def postCatEditView(request):
    if(request.method == "POST"):
        pk = request.POST.get('id')
        name = request.POST.get('name')
        if PostCategory.objects.filter(id=pk).exists():
            obj = PostCategory.objects.get(pk=pk)
            obj.name = name
            obj.save()

        response = {
            "flash_satatus": "Your data is updated!"
        }
        return JsonResponse(response)


def postCatDelView(request):
    if request.method == "POST":
        id = request.POST['id']
        obj = PostCategory.objects.get(pk=id)
        obj.delete()
        response = {
            'status': 'Deleted!'
        }

        return JsonResponse(response)

# inventory


# categorylist


class CategoryCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/category.html'
    form_class = CategoryForm
    success_url = reverse_lazy('category')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allcategory'] = Category.objects.all().order_by('-id')
        return context


class CategoryUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/categoryupdate.html'
    form_class = CategoryForm
    model = Category
    success_url = reverse_lazy('category')


class CategoryDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Category
    success_url = reverse_lazy('category')


# dimension list


class DimensionAndWeightCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/dimension.html'
    form_class = DimensionAndWeightsForm
    success_url = reverse_lazy('dimension-weights')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['alldimensions'] = DimensionAndWeight.objects.all().order_by('-id')
        return context


class DimensionandWeightUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/dimension-update.html'
    form_class = DimensionAndWeightsForm
    model = DimensionAndWeight
    success_url = reverse_lazy('dimension-weights')


class DimensionAndWeightDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = DimensionAndWeight
    success_url = reverse_lazy('dimension-weights')


# engineview


class EngineCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/engines.html'
    form_class = TractorEngineForm
    success_url = reverse_lazy('engine')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allengines'] = Engine.objects.all().order_by('-id')
        return context


class EngineUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/engineupdate.html'
    form_class = TractorEngineForm
    model = Engine
    success_url = reverse_lazy('engine')


class EngineDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Engine
    success_url = reverse_lazy('engine')


# FuelTank


class FuelTankCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/fuel.html'
    form_class = FuelTankForm
    success_url = reverse_lazy('fuel-tank')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allfueltanks'] = FuelTank.objects.all().order_by('-id')
        return context


class FuelTankUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/fueltankupdate.html'
    form_class = FuelTankForm
    model = FuelTank
    success_url = reverse_lazy('fuel-tank')


class FuelTankDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = FuelTank
    success_url = reverse_lazy('fuel-tank')

# hydraulices


class HydraulicCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/hydraulices.html'
    form_class = HydraulicesForm
    success_url = reverse_lazy('hydraulices')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allhydraulices'] = Hydraulic.objects.all().order_by('-id')

        return context


class HydraulicUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/hydraulicesupdate.html'
    form_class = HydraulicesForm
    model = Hydraulic
    success_url = reverse_lazy('hydraulices')


class HydraulicDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Hydraulic
    success_url = reverse_lazy('hydraulices')

# image


class ImageCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/image.html'
    form_class = InventoryImageForm
    success_url = reverse_lazy("image-create")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['imageList'] = Image.objects.all().order_by('-id')
        context['categorylist'] = Category.objects.all().order_by('-id')
        return context


class ImageUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/imageupdate.html'
    form_class = InventoryImageForm
    model = Image
    success_url = reverse_lazy('image-create')


class ImageDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Image
    success_url = reverse_lazy('image-create')


# old tractor

# class OldTractorCreateView(CreateView):
#     template_name = 'admin/inventory/oldtractor.html'
#     form_class = OldTractorForm
#     success_url = reverse_lazy('oldtractor-create')

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['o_tractorList'] = OldTractor.objects.all().order_by('-id')
#         return context


class OldBrandCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/brand/old-brandcreate.html'
    form_class = OldTractorBrandForm
    success_url = reverse_lazy('old-brandcreate')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allbrands'] = OldTractorBrand.objects.all().order_by('-id')
        return context


class OldBrandUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/brand/old-brand-update.html'
    form_class = OldTractorBrandForm
    model = OldTractorBrand
    success_url = reverse_lazy("old-brandcreate")


# Brand delete

class OldBrandDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = OldTractorBrand
    success_url = reverse_lazy('old-brandcreate')


class OldTractorListView(AdminRequiredMixin, ListView):
    login_url = "/login/"
    template_name = 'admin/inventory/oldtractor.html'
    model = OldTractor
    context_object_name = 'o_tractorList'


class addOldTractors(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/add-old-tractor.html'
    form_class = OldTractorForm
    success_url = '/inventory/old-tractors/'

    def form_valid(self, form):
        price = int(form.cleaned_data.get('price'))
        if price > 4000000:
            form.instance.is_premium = True
        return super().form_valid(form)


class EditOldTractorsView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/inventory/edit-old-tractor.html"
    form_class = OldTractorForm
    model = OldTractor
    success_url = '/inventory/old-tractors/'

    def form_valid(self, form):
        price = int(form.cleaned_data.get('price'))
        if price > 4000000:
            form.instance.is_premium = True
        return super().form_valid(form)


class OldTractorDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/inventory/old-tractor-detail.html'
    model = OldTractor
    context_object_name = 'old_tractor'


class OldTractorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = OldTractor
    success_url = reverse_lazy('old-tractors')


# otherinformations
class OtherInfoCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/otherinfo.html'
    form_class = OtherInfoForm
    success_url = reverse_lazy('other-information')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allinfo'] = OtherInformation.objects.all().order_by('-id')

        return context


class OtherInfoUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/otherinfo-update.html'
    form_class = OtherInfoForm
    model = OtherInformation
    success_url = reverse_lazy('other-information')


class OtherInfoDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = OtherInformation
    success_url = reverse_lazy('other-information')


# powers
class PowerCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/powers.html'
    form_class = PowerForm
    success_url = reverse_lazy('powers')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allpowers'] = Power.objects.all().order_by('-id')
        return context


class PowerUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/power-update.html'
    form_class = PowerForm
    model = Power
    success_url = reverse_lazy('powers')


class PowerDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Power
    success_url = reverse_lazy('powers')


# steerings
class SteeringCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/steering.html'
    form_class = SteeringForm
    success_url = reverse_lazy('steerings')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allsteerings'] = Steering.objects.all().order_by('-id')

        return context


class SteeringUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/steering-update.html'
    form_class = SteeringForm
    model = Steering
    success_url = reverse_lazy('steerings')


class SteeringDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Steering
    success_url = reverse_lazy('steerings')
# tractors


class TractorsView(AdminRequiredMixin, ListView):
    login_url = "/login/"
    template_name = "admin/inventory/tractor.html"
    queryset = Tractor.objects.all()
    context_object_name = 'tractors'


class AddTractorsView(AdminRequiredMixin, TemplateView):
    login_url = "/login/"
    template_name = "admin/inventory/add-tractor.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = TractorForm
        context['img_form'] = InventoryImageForm

        return context

    def post(self, request, *args, **kwargs):
        form = TractorForm(request.POST)
        img_form = InventoryImageForm(request.POST, request.FILES)
        if form.is_valid() and img_form.is_valid():
            tractor_obj = form.save(commit=False)
            img_obj = img_form.save()
            tractor_obj.image = img_obj
            tractor_obj.save()
        return HttpResponseRedirect(reverse('tractors'))


class UpdateTractorsView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/inventory/update-tractor.html"
    form_class = TractorForm
    model = Tractor
    success_url = "/inventory/tractors/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tractor_obj = Tractor.objects.get(pk=self.kwargs.get('pk'))
        context['img_id'] = tractor_obj.image_id
        context['tr_id'] = tractor_obj.id

        return context


class DetailTractorView(AdminRequiredMixin, DetailView):
    template_name = 'admin/inventory/detail-tractor.html'
    model = Tractor
    context_object_name = 'tractor_detail'


class DeleteTractorView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Tractor
    success_url = reverse_lazy('tractors')


class AdminTractorImageUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/inventory/image_update.html"
    form_class = InventoryImageForm
    model = Image
    success_url = "/inventory/images/"

    def get_success_url(self):
        if "next" in self.request.GET:
            next_url = self.request.GET.get("next")
            return next_url
        else:
            return self.success_url


# transmission


class TransmissionCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/transmission.html'
    form_class = TractorTransmissionForm
    success_url = reverse_lazy('transmission')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['alltransmission'] = Transmission.objects.all().order_by('-id')

        return context


class TransmissionUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/transmission-update.html'
    form_class = TractorTransmissionForm
    model = Transmission
    success_url = reverse_lazy('transmission')


class TransmissionDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Transmission
    success_url = reverse_lazy('transmission')


# wheel_tyre


class WheelAndTyreCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/wheel.html'
    form_class = WheelAndTyreForm
    success_url = reverse_lazy('wheel-tyre')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['allwheels'] = WheelAndTyre.objects.all().order_by('-id')
        return context


class WheelAndTyreUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/wheel-update.html'
    form_class = WheelAndTyreForm
    model = WheelAndTyre
    success_url = reverse_lazy('wheel-tyre')


class WheelAndTyreDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = WheelAndTyre
    success_url = reverse_lazy('wheel-tyre')


# edit WheelAndTyre


def edit_wheelandtyre(request):
    if request.method == 'POST':
        id = request.POST.get("id")
        wheel_drive = request.POST.get("wheel_drive")
        front = request.POST.get("front")
        rear = request.POST.get("rear")
        obj = WheelAndTyre.objects.get(pk=id)
        obj.wheel_drive = wheel_drive
        obj.front = front
        obj.rear = rear
        obj.save()

        response = {
            'status': 'ok'

        }
        return JsonResponse(response)


# Tractor Tyre

class TractorTyreBrandView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/tractor-tyre-brand.html'
    form_class = TractorTyreBrandForm
    success_url = reverse_lazy('tractor-tyre-brand')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = TyreBrand.objects.all().order_by('-id')
        return context


class TractorTyreBrandUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/tractor-tyre-brand-edit.html'
    model = TyreBrand
    form_class = TractorTyreBrandForm
    success_url = reverse_lazy('tractor-tyre-brand')


class TractorTyreBrandDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = TyreBrand
    success_url = reverse_lazy('tractor-tyre-brand')


class TractorTyreView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/tractor_tyre.html'
    form_class = TractorTyreForm
    success_url = reverse_lazy('tractor-tyre')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = TractorTyre.objects.all()
        return context


class TractorTyreUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/tractor_tyre-edit.html'
    form_class = TractorTyreForm
    model = TractorTyre
    success_url = reverse_lazy('tractor-tyre')


class TractorTyreDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/inventory/tractor_tyre-detail.html'
    model = TractorTyre
    context_object_name = 'tractor_tyre'


class TractorTyreDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = TractorTyre
    success_url = reverse_lazy('tractor-tyre')


class ToolBrandCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/tool/tool-brand.html"
    form_class = ToolBrandForm
    success_url = reverse_lazy('tool-brand')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['toolbrands'] = ToolBrand.objects.all().order_by('-id')
        return context


class ToolBrandUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/tool/tool-brand-update.html"
    model = ToolBrand
    form_class = ToolBrandForm
    success_url = reverse_lazy('tool-brand')


class ToolBrandDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = ToolBrand
    success_url = reverse_lazy('tool-brand')


class ToolCategoryCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/tool/tool-category.html"
    form_class = ToolCategoryForm
    success_url = reverse_lazy('tool-category')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['toolcategories'] = ToolCategory.objects.all().order_by('-id')

        return context


class ToolCategoryUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/tool/tool-category-update.html"
    model = ToolCategory
    form_class = ToolCategoryForm
    success_url = reverse_lazy('tool-category')


class ToolCategoryDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = ToolCategory
    success_url = reverse_lazy('tool-category')


class ToolCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/tool/tool-detail.html"
    form_class = ToolsForm
    success_url = reverse_lazy('admin-tool-list')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['tools'] = Tools.objects.all().order_by('-id')

        return context


class ToolUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/tool/tool-detail-update.html"
    model = Tools
    form_class = ToolsForm
    success_url = reverse_lazy('admin-tool-list')


class ToolDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/tool/tool-detail-page.html'
    model = Tools
    context_object_name = 'tool'


class ToolDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Tools
    success_url = reverse_lazy('admin-tool-list')


# Video


class VideoCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/video/video.html"
    form_class = VideoForm
    success_url = reverse_lazy('video-lists')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['videos'] = Video.objects.all().order_by('-id')

        return context


class VideoUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/video/video-update.html"
    model = Video
    form_class = VideoForm
    success_url = reverse_lazy('video-lists')


class VideoDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Video
    success_url = reverse_lazy('video-lists')


class VideoCategoryCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/video/video-category.html"
    form_class = VideoCategoryForm
    success_url = reverse_lazy('video-category')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['videocategories'] = VideoCategory.objects.all().order_by('-id')

        return context


class VideoCategoryUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/video/video-category-update.html"
    model = VideoCategory
    form_class = VideoCategoryForm
    success_url = reverse_lazy('video-category')


class VideoCategoryDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = VideoCategory
    success_url = reverse_lazy('video-category')

# harvestor


class EngineHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/engine.html"
    model = HarvesterEngine
    form_class = EngineForm
    success_url = reverse_lazy('harvestor-engine')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = HarvesterEngine.objects.all().order_by('-id')

        return context


class EngineHarvesterUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/engine-update.html"
    model = HarvesterEngine
    form_class = EngineForm
    success_url = reverse_lazy('harvestor-engine')


class EngineHarvesterDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = HarvesterEngine
    success_url = reverse_lazy('harvestor-engine')


class ClutchHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/clutch.html"
    form_class = ClutchForm
    success_url = reverse_lazy('harvestor-clutch')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Clutch.objects.all().order_by('-id')

        return context


class ClutchHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Clutch
    success_url = reverse_lazy('harvestor-clutch')


class ClutchHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/harvestor-inventory/clutch-update.html'
    model = Clutch
    form_class = ClutchForm
    success_url = reverse_lazy('harvestor-clutch')


class TransmissionHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/transmission.html"
    form_class = TransmissionForm
    success_url = reverse_lazy('harvestor-transmission')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = HarvesterTransmission.objects.all().order_by('-id')
        return context


class TransmissionHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/harvestor-inventory/transmission-update.html'
    model = HarvesterTransmission
    form_class = TransmissionForm
    success_url = reverse_lazy('harvestor-transmission')


class TransmissionHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = HarvesterTransmission
    success_url = reverse_lazy('harvestor-transmission')


class CutterbarHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/cutterbar.html"
    form_class = CutterBarForm
    success_url = reverse_lazy('harvestor-cutterbar')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = CutterBar.objects.all().order_by('-id')

        return context


class CutterbarHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/harvestor-inventory/cutterbar-update.html'
    model = CutterBar
    form_class = CutterBarForm
    success_url = reverse_lazy('harvestor-cutterbar')


class CutterbarHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = CutterBar
    success_url = reverse_lazy('harvestor-cutterbar')


class StrawWalkerHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/straw-walker.html"
    form_class = StrawWalkerForm
    success_url = reverse_lazy('harvestor-straw-walker')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = StrawWalker.objects.all().order_by('-id')
        return context


class StrawWalkerHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/harvestor-inventory/straw-walker-update.html'
    form_class = StrawWalkerForm
    model = StrawWalker
    success_url = reverse_lazy('harvestor-straw-walker')


class StraWalkerHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = StrawWalker
    success_url = reverse_lazy('harvestor-straw-walker')


class CleaningSievesHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/cleaning.html"
    form_class = CleaningSievesForm
    success_url = reverse_lazy('harvestor-cleaning')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = CleaningSieves.objects.all().order_by('-id')
        return context


class CleaningSievesHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/cleaning-update.html"
    model = CleaningSieves
    form_class = CleaningSievesForm
    success_url = reverse_lazy('harvestor-cleaning')


class CleaningSievesHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = CleaningSieves
    success_url = reverse_lazy('harvestor-cleaning')


class CapacityHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/capacity.html"
    form_class = CapacityForm
    success_url = reverse_lazy('harvestor-capacity')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Capacity.objects.all().order_by('-id')
        return context


class CapacityHarvestorUpadateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/harvestor-inventory/capacity-update.html'
    form_class = CapacityForm
    model = Capacity
    success_url = reverse_lazy('harvestor-capacity')


class CapacityHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Capacity
    success_url = reverse_lazy('harvestor-capacity')


class BatteryHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/battery.html"
    form_class = BatteryForm
    success_url = reverse_lazy('harvestor-battery')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Battery.objects.all().order_by('-id')
        return context


class BatteryHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/battery-update.html"
    model = Battery
    form_class = BatteryForm
    success_url = reverse_lazy('harvestor-battery')


class BatteryHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Battery
    success_url = reverse_lazy('harvestor-battery')


class DimensionsHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/main-dimensions.html"
    form_class = MainDimensionsForm
    success_url = reverse_lazy('harvestor-dimensions')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(*kwargs)
        context['object_list'] = MainDimensions.objects.all().order_by('-id')
        return context


class DimensionHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/main-dimensions-update.html"
    model = MainDimensions
    form_class = MainDimensionsForm
    success_url = reverse_lazy('harvestor-dimensions')


class MainDimensionsHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = MainDimensions
    success_url = reverse_lazy('harvestor-dimensions')


class ConcaveHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/concave.html"
    form_class = ConcaveForm
    success_url = reverse_lazy('harvestor-concave')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Concave.objects.all().order_by('-id')
        return context


class ConcaveHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/concave-update.html"
    model = Concave
    form_class = ConcaveForm
    success_url = reverse_lazy('harvestor-concave')


class ConcaveHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Concave
    success_url = reverse_lazy('harvestor-concave')


class TyreHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/tyre.html"
    form_class = TyreForm
    success_url = reverse_lazy('harvestor-tyre')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Tyre.objects.all().order_by('-id')
        return context


class TyreHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/tyre-update.html"
    model = Tyre
    form_class = TyreForm
    success_url = reverse_lazy('harvestor-tyre')


class TyreHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Tyre
    success_url = reverse_lazy('harvestor-tyre')


class ThreshingHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/threshing.html"
    form_class = ThreshingDrumForm
    success_url = reverse_lazy('harvestor-threshing')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = ThreshingDrum.objects.all().order_by('-id')
        return context


class ThreshingHarvestorUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/threshing-update.html"
    model = ThreshingDrum
    form_class = ThreshingDrumForm
    success_url = reverse_lazy('harvestor-threshing')


class ThreshingDrumHarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = ThreshingDrum
    success_url = reverse_lazy('harvestor-threshing')


class HarvesterBrandCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/harvester-brand.html"
    form_class = HarvesterBrandForm
    success_url = reverse_lazy('harvester-brand')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['harvesterbrands'] = HarvesterBrand.objects.all().order_by('-id')
        return context


class HarvestorBrandUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/harvester-brand-update.html"
    model = HarvesterBrand
    form_class = HarvesterBrandForm
    success_url = reverse_lazy('harvester-brand')


class HarvesterBrandDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = HarvesterBrand
    success_url = reverse_lazy('harvester-brand')


class HarvesterPowerSourceCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/powersource.html"
    form_class = PowerSourceForm
    success_url = reverse_lazy('harvester-power-source')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['powersources'] = PowerSource.objects.all().order_by('-id')

        return context


class HarvesterPowerSourceUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/powersource-update.html"
    model = PowerSource
    form_class = PowerSourceForm
    success_url = reverse_lazy('harvester-power-source')


class PowerSourceDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = PowerSource
    success_url = reverse_lazy('harvester-power-source')


class HarvestorView(AdminRequiredMixin, ListView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/harvestor.html"
    model = Harvester


class AddHarvestorView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/add-harvestor.html"
    form_class = HarvesterForm
    success_url = reverse_lazy('harvestor')


class EditHarvestorView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/harvestor-inventory/edit-harvestor.html"
    form_class = HarvesterForm
    model = Harvester
    success_url = reverse_lazy('harvestor')


class HarvestorDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/harvestor-inventory/detail-harvestor.html'
    model = Harvester
    context_object_name = 'harvestor'


class HarvestorDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Harvester
    success_url = reverse_lazy('harvestor')


# implements
class ImplementView(AdminRequiredMixin, ListView):
    login_url = "/login/"
    template_name = 'admin/inventory/implements.html'
    queryset = Implement.objects.all().order_by('-id')
    context_object_name = 'implements'


class ImplementAddView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/add-implements.html'
    form_class = ImplementForm
    success_url = '/admin-implements/'


class ImplementEditView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = "admin/inventory/edit-implements.html"
    form_class = ImplementForm
    model = Implement
    success_url = '/admin-implements/'


class ImplementDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/inventory/detail-implements.html'
    model = Implement
    context_object_name = 'implement'


class ImplementDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = Implement
    success_url = reverse_lazy('admin-implements')


class ImplementBrandListView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/implementbrand.html'
    form_class = ImplementBrandForm
    success_url = reverse_lazy("admin-implement-brand-list")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['implementbrand'] = ImplementBrand.objects.all().order_by('-id')
        return context


class ImplementBrandUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/inventory/implementbrandupdate.html'
    form_class = ImplementBrandForm
    model = ImplementBrand
    success_url = reverse_lazy("admin-implement-brand-list")


class ImplementBrandView(AdminRequiredMixin, View):
    login_url = "/login/"

    def post(self, request):
        if request.POST['key'] == 'update':
            id = request.POST['id']
            name = request.POST['name']
            imp = ImplementBrand.objects.get(pk=id)
            imp.name = name
            imp.save()
            data = name
            myid = id

        elif request.POST['key'] == 'delete':
            id = request.POST['id']
            imp = ImplementBrand.objects.get(pk=id)
            imp.delete()
            data = ""
            myid = ""

        return JsonResponse({
            'name': data,
            'id': myid,
        })


class ImplementCategoryListView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/implementcategory.html'
    form_class = ImplementCategoryForm
    success_url = reverse_lazy("admin-implement-category-list")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['implement_category'] = ImplementCategory.objects.all().order_by(
            '-id')
        return context


class ImplementCategoryView(AdminRequiredMixin, View):
    login_url = "/login/"

    def post(self, request):
        if request.POST['key'] == 'update':
            id = request.POST['id']
            name = request.POST['name']
            imp = ImplementCategory.objects.get(pk=id)
            imp.name = name
            imp.save()
            data = request.POST['name']
            myid = request.POST['id']

        elif request.POST['key'] == 'delete':
            id = request.POST['id']
            imp = ImplementCategory.objects.get(pk=id)
            imp.delete()
            data = ""
            myid = ""

        return JsonResponse({
            'name': data,
            'id': myid,
        })


class ImplementTypeListView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/inventory/implementtype.html'
    form_class = ImplementTypeForm
    success_url = reverse_lazy("admin-implement-type-list")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['implement_type'] = ImplementType.objects.all().order_by('-id')
        return context


class ImplementTypeView(AdminRequiredMixin, View):
    login_url = "/login/"

    def post(self, request):
        if request.POST['key'] == 'update':
            id = request.POST['id']
            name = request.POST['name']
            slug = request.POST['slug']
            imp = ImplementType.objects.get(pk=id)
            imp.name = name
            imp.slug = slug
            imp.save()
            data = request.POST['name']
            myid = request.POST['id']

        elif request.POST['key'] == 'delete':
            id = request.POST['id']
            imp = ImplementType.objects.get(pk=id)
            imp.delete()
            data = ""
            myid = ""

        return JsonResponse({
            'name': data,
            'id': myid,
        })


def ImplementTypeIs_publishedView(request):
    if request.method == 'POST':
        myid = request.POST['id']
        imp = ImplementType.objects.get(pk=myid)
        if imp.is_published:
            imp.is_published = False
            status = "F"
        else:
            imp.is_published = True
            status = "T"

        imp.save()

    return JsonResponse({'status': status})


# Lubricant view start
class LubricantBrandCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/lubricant/lubricant-brand-create.html'
    form_class = LubricantBrandForm
    success_url = reverse_lazy("lubricant-brand-create")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['lubricantbrands'] = LubricantBrand.objects.all().order_by('-id')
        return context


class LubricantBrandUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/lubricant/lubricant-brand-update.html'
    form_class = LubricantBrandForm
    model = LubricantBrand
    success_url = reverse_lazy('lubricant-brand-create')


class LubricantBrandDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = LubricantBrand
    success_url = reverse_lazy("lubricant-brand-create")


class LubricantTypeCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/lubricant/lubricant-type-create.html'
    form_class = LubricantTypeForm
    success_url = reverse_lazy("lubricant-type-create")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['lubricanttypes'] = LubricantType.objects.all().order_by("-id")
        return context


class LubricantTypeUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/lubricant/lubricant-type-update.html'
    form_class = LubricantTypeForm
    model = LubricantType
    success_url = reverse_lazy("lubricant-type-create")


class LubricantTypeDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = LubricantType
    success_url = reverse_lazy('lubricant-type-create')


class LubricantDetailCreateView(AdminRequiredMixin, CreateView):
    login_url = "/login/"
    template_name = 'admin/lubricant/lubricant.html'
    form_class = LubricantDetailForm
    success_url = reverse_lazy("lubricant-detail-create")

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['lubricantdetails'] = LubricantDetail.objects.all().order_by("-id")
        return context


class LubricantDetailUpdateView(AdminRequiredMixin, UpdateView):
    login_url = "/login/"
    template_name = 'admin/lubricant/lubricant-detail-update.html'
    form_class = LubricantDetailForm
    model = LubricantDetail
    success_url = reverse_lazy("lubricant-detail-create")


class LubricantDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/lubricant/lubricant-detail-page.html'
    model = LubricantDetail
    context_object_name = 'lubricant'


class LubricantDetailDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    login_url = "/login/"
    model = LubricantDetail
    success_url = reverse_lazy('lubricant-detail-create')

# user create


class PasswordResetView(SuperAdminRequiredMixin, AdminRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        # user = get_object_or_404(User, pk = kwargs.get("pk"))
        account = AdminUser.objects.filter(pk=self.kwargs.get("pk")).first()
        password = get_random_string(8)
        account.set_password(password)
        account.save(update_fields=['password'])

        text_content = f'''
        your login credential for tractor shop
        username : {account.email}
        password : {password}
        '''
        send_mail(
            'You are Welcome to our Team!',
            text_content,
            conf_settings.EMAIL_HOST_USER,
            [account.email],
            fail_silently=False,
        )
        messages.success(
            self.request, "Password and Username is sent")

        return redirect(reverse_lazy('admin-user-list'))


class AdminCreateview(SuperAdminRequiredMixin, AdminRequiredMixin, CreateView):
    template_name = 'admin/users/users.html'
    form_class = AdminCreateForm
    success_url = reverse_lazy('admin-user-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['userList'] = AdminUser.objects.all()

        return context

    def get_success_url(self):
        return reverse('reset-admin-password', kwargs={'pk': self.object.pk})


class AdminUpdateView(SuperAdminRequiredMixin, AdminRequiredMixin, UpdateView):
    template_name = 'admin/users/user-edit.html'
    form_class = AdminCreateForm
    model = AdminUser
    success_url = reverse_lazy('admin-user-list')


class AdminDeleteView(SuperAdminRequiredMixin, AdminRequiredMixin, DeleteMixin, DeleteView):
    model = AdminUser
    success_url = reverse_lazy('admin-user-list')


class ForgotPasswordView(FormView):
    template_name = 'admin/users/reset-password.html'
    form_class = PasswordResetForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        email = form.cleaned_data['email']
        user = User.objects.filter(email=email).first()
        password = get_random_string(8)
        user.set_password(password)
        user.save(update_fields=['password'])

        text_content = f'''Your password has been changed.
        password: {password}'''
        send_mail(
            'Password Reset | Tractor Shop',
            text_content,
            conf_settings.EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )
        messages.success(self.request, "Password reset code is sent")
        return super().form_valid(form)


class PasswordsChangeView(FormView):
    template_name = 'admin/users/password_change.html'
    form_class = ChangePasswordForm
    success_url = reverse_lazy('admin-dashboard')

    def get_form(self):
        form = super().get_form()
        form.set_user(self.request.user)
        return form

    def form_valid(self, form):
        password = form.cleaned_data['confirm_password']
        account = User.objects.filter(username=self.request.user).first()
        account.set_password(password)
        account.save(update_fields=['password'])
        user = authenticate(username=self.request.user, password=password)
        login(self.request, user)

        return super().form_valid(form)


class CustomerProfileListView(AdminRequiredMixin, ListView):
    template_name = 'admin/users/customer-list.html'
    model = User

    def get_queryset(self):
        return super().get_queryset().filter(groups__name="Customer")


class CustomerProfileDeleteView(AdminRequiredMixin, SuperAdminRequiredMixin, DeleteMixin, DeleteView):
    model = User
    success_url = reverse_lazy('customer-profile-list')


# old implement
class OldImplementListView(AdminRequiredMixin, ListView):
    template_name = 'admin/inventory/old-implement-list.html'
    model = OldImplement


class OldImplementDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/inventory/old-implement-detail.html'
    model = OldImplement
    context_object_name = 'old_implement'


class OldImplementDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    model = OldImplement
    success_url = reverse_lazy('old-implement-list')

# old harvester


class OldHarvesterListView(AdminRequiredMixin, ListView):
    template_name = 'admin/harvestor-inventory/old-harvester-list.html'
    model = OldHarvester


class OldHarvesterDetailView(AdminRequiredMixin, DetailView):
    template_name = 'admin/harvestor-inventory/old-harvester-detail.html'
    model = OldHarvester
    context_object_name = 'old_harvester'


class OldHarvesterDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    model = OldHarvester
    success_url = reverse_lazy('old-harvester-list')


class BrokerDealerView(AdminRequiredMixin, CreateView):
    template_name = 'admin/dealer/broker-dealer.html'
    form_class = BrokerDealerForm
    success_url = reverse_lazy('broker-dealer')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = BrokerDealer.objects.all().order_by('-id')

        return context


class BrokerDealerUpadteView(AdminRequiredMixin, UpdateView):
    template_name = 'admin/dealer/broker-dealer-edit.html'
    form_class = BrokerDealerForm
    model = BrokerDealer
    success_url = reverse_lazy('broker-dealer')


class BrokerDealerDeleteView(AdminRequiredMixin, DeleteMixin, DeleteView):
    model = BrokerDealer
    success_url = reverse_lazy('broker-dealer')
