from dealer.forms import BrokerDealerForm
from dealer.models import BrokerDealer
from harvester.models import HarvesterBrand, OldHarvester, PowerSource
from harvester.forms import OldHarvesterForm
from users.models import SellerDetail
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib import messages
from django.urls.base import reverse_lazy

from django.views.generic import ListView, RedirectView, TemplateView, DetailView
from django.views.generic.edit import CreateView
from .forms import ApplyInsuranceForm, ApplyLoanForm, QuestionForm, AnswerForm

from inventory.models import Brand, ImplementBrand, ImplementCategory, OldImplement
from .models import *
from frontend.models import Post

from frontend.homemixins import FooterFormMixin

from users.forms import SellerDetailForm
from inventory.forms import OldImplementForm

from dashboard.models import *


class LoanView(FooterFormMixin, CreateView):
    template_name = 'customer/loan.html'
    form_class = ApplyLoanForm
    success_url = reverse_lazy('loan')

    def get_context_data(self, *args, **kwargs):
        context = super(LoanView, self).get_context_data(
            *args, **kwargs
        )
        context['finances'] = FinanceScheme.objects.prefetch_related()
        return context


class EnquiryView(FooterFormMixin, TemplateView):
    template_name = 'customer/enquiry.html'


class InsuranceView(FooterFormMixin, CreateView):
    template_name = "customer/insurance.html"
    form_class = ApplyInsuranceForm
    success_url = reverse_lazy('insurance')

    def form_valid(self, form):
        if 'pre_policy' in self.request.GET:
            prev_policy = self.request.POST.get('pre_policy')
        if form.is_valid():
            messages.success(self.request, "succfully applied!")
        else:
            messages.error(self.request, 'Something went wrong')

        return super().form_valid(form)


class EmiCalculatorView(FooterFormMixin, TemplateView):
    template_name = 'customer/emi_calculator.html'


class BrokerDealerView(FooterFormMixin, CreateView):
    template_name = 'customer/broker_dealer.html'
    form_class = BrokerDealerForm
    success_url = reverse_lazy('broker-dealer')

    def form_valid(self, form):
        messages.success(self.request, "Your request has been sent")
        return super().form_valid(form)


class AskQuestionView(FooterFormMixin, ListView):
    model = Question
    paginate_by = 5
    template_name = 'customer/ask_question.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AskQuestionView, self).get_context_data(
            *args, **kwargs)
        paginator = context['paginator']
        page_numbers_range = 5
        max_index = len(paginator.page_range)
        page = self.request.GET.get('page')
        current_page = int(page) if page else 1

        start_index = int((current_page - 1) /
                          page_numbers_range) * page_numbers_range
        end_index = start_index + page_numbers_range
        if end_index >= max_index:
            end_index = max_index

        page_range = paginator.page_range[start_index:end_index]
        context['page_range'] = page_range
        context['question'] = Question.objects.order_by('-published_date')[:9]
        return context

    def post(self, request, *args, **kwargs):
        form = QuestionForm(self.request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Thank you for posting Question')
            return redirect('ask-question')
        return render(request, self.template_name, context)


class AnswerView(FooterFormMixin, TemplateView):
    template_name = 'customer/answer.html'
    form_class = AnswerForm

    def get_context_data(self, *args, **kwargs):
        context = super(AnswerView, self).get_context_data(
            *args, **kwargs)
        context['question'] = Question.objects.get(id=self.kwargs.get('pk'))
        context['questions'] = Question.objects.all()
        context['answers'] = Answer.objects.filter(
            question__id=self.kwargs.get('pk'))
        context['form'] = self.form_class(self.request.POST or None)
        return context

    def post(self, request, *args, **kwargs):
        name = request.POST.get('name')
        mobile = request.POST.get('mob_no')
        answer = request.POST.get('answer')
        context = self.get_context_data()
        question = Question.objects.get(id=self.kwargs.get('pk'))

        ans = Answer(name=name, mob_no=mobile,
                     answer=answer, question=question)
        ans.save()

        messages.success(request, 'Answer Posted')

        return render(request, self.template_name, context)


class BlogView(FooterFormMixin, TemplateView):
    template_name = 'customer/blog.html'

    def get_context_data(self, *args, **kwargs):
        context = super(BlogView, self).get_context_data(
            *args, **kwargs)
        blogs = Post.objects.all().order_by('-date')
        context['blogs'] = blogs[:10]
        context['recents'] = blogs[:3]
        return context


class BlogDetailView(FooterFormMixin, DetailView):
    template_name = 'customer/blog_detail.html'
    model = Post

    # def get_context_data(self, *args, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     # context['now'] = timezone.now()
    #     context['blog'] = Post.objects.get(id= kwargs.get("pk"))
    #     return context


class SellView(FooterFormMixin, TemplateView):
    template_name = 'customer/sellbutton.html'


class BankDetailView(FooterFormMixin, DetailView):
    template_name = 'customer/bank_detail.html'
    model = FinanceScheme
    context_object_name = 'finance_schemes'


class SellFarmImplementsView(FooterFormMixin, TemplateView):
    template_name = 'forms/farmImplements_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['imp_form'] = OldImplementForm
        context['seller_form'] = SellerDetailForm

        return context

    def post(self, request, *args, **kwargs):
        cat_id = request.POST.get('category')
        category = ImplementCategory.objects.filter(id=cat_id).first()
        b_id = request.POST.get('brand')
        brand = ImplementBrand.objects.filter(id=b_id).first()
        model = request.POST.get('model')
        purchase_year = request.POST.get('purchase_year')
        title = request.POST.get('title')
        price = request.POST.get('price')
        about = request.POST.get('about')

        image1 = request.FILES.get('image1')
        image2 = request.FILES.get('image2')
        image3 = request.FILES.get('image3')
        image4 = request.FILES.get('image4')

        name = request.POST.get('name')
        phone_no = request.POST.get('phone_no')
        s_id = request.POST.get('state')
        state = State.objects.filter(id=s_id).first()
        d_id = request.POST.get('district')
        district = District.objects.filter(id=d_id).first()
        m_id = request.POST.get('municipality')
        municipality = Municipality.objects.get(id=m_id)
        pincode = request.POST.get('pincode')
        obj2 = SellerDetail.objects.create(
            name=name, phone_no=phone_no, state=state, district=district,  municipality=municipality, pincode=pincode)

        obj1 = OldImplement.objects.create(
            category=category, brand=brand, model=model, purchase_year=purchase_year,
            title=title, price=price, about=about, image1=image1, image2=image2,
            image3=image3, image4=image4, seller=obj2)

        messages.success(request, "Your request has been sent")

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class SellHarversterView(FooterFormMixin, TemplateView):
    template_name = 'forms/harvester_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['har_form'] = OldHarvesterForm
        context['seller_form'] = SellerDetailForm

        return context

    def post(self, request, *args, **kwargs):
        p_id = request.POST.get('power_source')
        power = PowerSource.objects.filter(id=p_id).first()
        h_id = request.POST.get('brand')
        brand = HarvesterBrand.objects.filter(id=h_id).first()
        model = request.POST.get('model')
        hours = request.POST.get('hours')
        purchase_year = request.POST.get('purchase_year')
        crop_type = request.POST.get('crop_type')
        cutting_width = request.POST.get('cutting_width')
        title = request.POST.get('title')
        price = request.POST.get('price')
        about = request.POST.get('about')

        image1 = request.FILES.get('image1')
        image2 = request.FILES.get('image2')
        image3 = request.FILES.get('image3')
        image4 = request.FILES.get('image4')

        name = request.POST.get('name')
        phone_no = request.POST.get('phone_no')
        s_id = request.POST.get('state')
        state = State.objects.filter(id=s_id).first()
        d_id = request.POST.get('district')
        district = District.objects.filter(id=d_id).first()
        m_id = request.POST.get('municipality')
        municipality = Municipality.objects.get(id=m_id)
        pincode = request.POST.get('pincode')
        obj2 = SellerDetail.objects.create(
            name=name, phone_no=phone_no, state=state, district=district, municipality=municipality, pincode=pincode)

        obj1 = OldHarvester.objects.create(
            power_source=power, brand=brand, hours=hours, model=model, purchase_year=purchase_year,
            title=title, price=price, crop_type=crop_type, cutting_width=cutting_width, about=about,
            image1=image1, image2=image2, image3=image3, image4=image4, seller=obj2)

        messages.success(request, "Your request has been sent")

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class SellLandPropertiesView(TemplateView):
    template_name = 'forms/landProperties_form.html'


class SellLivestockView(FooterFormMixin, TemplateView):
    template_name = 'forms/livestock_form.html'
