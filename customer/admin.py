from django.contrib import admin

from .models import ApplyInsurance, ApplyLoan, Question, Answer, FinanceScheme

admin.site.register(ApplyInsurance)
admin.site.register(ApplyLoan)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(FinanceScheme)
