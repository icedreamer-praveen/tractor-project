from django.db import models
from django.utils.translation import gettext_lazy as _
from django.urls import reverse

import datetime
from django.core.validators import MaxValueValidator, MinValueValidator
from dashboard.models import State, District


def current_year():
    return datetime.date.today().year


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


class ApplyInsurance(models.Model):

    name = models.CharField(_("Name"), max_length=500)
    mob_no = models.CharField(_("Phone No"), max_length=50)
    brand = models.ForeignKey("inventory.Brand", verbose_name=_(
        "Brand"), on_delete=models.SET_NULL, null=True)
    model = models.CharField(_("Model"), max_length=100)
    reg_no = models.CharField(_("Registration Number"), max_length=100)
    reg_year = models.PositiveIntegerField(
        default=current_year(), validators=[MinValueValidator(1984), max_value_current_year])
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    previous_claim = models.BooleanField(default=False, null=True, blank=True)

    class Meta:
        verbose_name = _("ApplyInsurance")
        verbose_name_plural = _("ApplyInsurances")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("ApplyInsurance_detail", kwargs={"pk": self.pk})


class ApplyLoan(models.Model):
    FINANCE_CHOICES = (
        ("New Tractor", "New Tractor"),
        ("Old Tractor", "Old Tractor")
    )
    LOAN_AMOUNT = (
        ('1-2 lakhs', '1-2 lakhs'),
        ('2-3 lakhs', '2-3 lakhs'),
        ('3-4 lakhs', '3-4 lakhs'),
        ('4-5 lakhs', '4-5 lakhs'),
        ('more than 5 lakhs', 'more than 5 lakhs')
    )

    type_finance = models.CharField(
        _("Type of Finance"), choices=FINANCE_CHOICES, max_length=250)
    name = models.CharField(_("Name"), max_length=500)
    mob_no = models.CharField(_("MObile No"), max_length=50)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    loan_amount = models.CharField(
        _("Loan Amount"), max_length=50, choices=LOAN_AMOUNT)
    other_info = models.TextField(
        _("Other Information"), null=True, blank=True)

    class Meta:
        verbose_name = _("ApplyLoan")
        verbose_name_plural = _("ApplyLoans")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("ApplyLoan_detail", kwargs={"pk": self.pk})


class Question(models.Model):
    name = models.CharField(_("Name"), max_length=50)
    mob_no = models.CharField(_("Mobile No"), max_length=50)
    question = models.TextField(_("Your Question"))
    published_date = models.DateTimeField(
        _("Published Date"), auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name = _("Question")
        verbose_name_plural = _("Questions")

    def __str__(self):
        return self.question

    def get_absolute_url(self):
        return reverse("Question_detail", kwargs={"pk": self.pk})


class Answer(models.Model):
    question = models.ForeignKey(Question, verbose_name=_(
        "Question"), on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=50)
    mob_no = models.CharField(_("Mobile No"), max_length=50)
    answer = models.TextField(_("Your Answer"))
    submitted_date = models.DateTimeField(
        _("Submitted Date"), auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name = _("Answer")
        verbose_name_plural = _("Answers")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Answer_detail", kwargs={"pk": self.pk})


class FinanceScheme(models.Model):
    bank_name = models.CharField(max_length=250)
    title = models.CharField(max_length=250)
    image = models.ImageField(upload_to="finance scheme")
    description = models.TextField()

    class Meta:
        verbose_name = _('FinanceScheme')
        verbose_name_plural = _('FinanceSchemes')
