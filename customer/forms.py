from .models import ApplyInsurance, ApplyLoan, Question, Answer, FinanceScheme
from django import forms
from django.core.exceptions import ValidationError
from django_summernote.widgets import SummernoteWidget


class FormControlMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({

                'class': 'form-control tractor-input'

            })


class ApplyInsuranceForm(FormControlMixin, forms.ModelForm):

    class Meta:
        model = ApplyInsurance
        fields = "__all__"

    def clean_mob_no(self):
        mob_no = self.cleaned_data['mob_no']
        try:
            int(mob_no)
        except ValueError:
            raise forms.ValidationError("Invalid Contact Number")
        return mob_no

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({
            'placeholder': 'Your Name'
        })
        self.fields['mob_no'].widget.attrs.update({
            'placeholder': 'Phone Number'
        })
        self.fields['model'].widget.attrs.update({
            'placeholder': 'Enter Model'
        })
        self.fields['reg_no'].widget.attrs.update({
            'placeholder': 'Registration no.'
        })

    def clean(self):
        cleaned_data = super().clean()
        mobile = self.cleaned_data['mob_no']
        if '-' in mobile:
            if '-' in mobile[2]:
                y = mobile.split('-')
                if len(y[0]) != 2 or len(y[1]) != 8:
                    raise ValidationError({
                        'mob_no': 'Invalid Contact number'
                    })
            else:
                raise ValidationError({
                    'mob_no': 'invalid Contact number'
                })
        else:
            pass
        if len(mobile) < 8 or len(mobile) > 14:
            raise ValidationError({
                'mob_no': 'Invalid Contact Number'
            })


class ApplyLoanForm(FormControlMixin, forms.ModelForm):

    class Meta:
        model = ApplyLoan
        fields = "__all__"

    def clean_mob_no(self):
        mob_no = self.cleaned_data['mob_no']
        try:
            int(mob_no)
        except ValueError:
            raise forms.ValidationError("Invalid Contact Number")
        return mob_no

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['type_finance'].choices = self.fields['type_finance'].choices[1:]
        self.fields['loan_amount'].choices = self.fields['loan_amount'].choices[1:]
        self.fields['name'].widget.attrs.update({
            'placeholder': 'Your Name'
        })
        self.fields['mob_no'].widget.attrs.update({
            'placeholder': 'Phone Number'
        })
        self.fields['district'].widget.attrs.update({
            'placeholder': 'District Here'
        })
        self.fields['other_info'].widget.attrs.update({
            'placeholder': 'other info here...'
        })

    def clean(self):
        cleaned_data = super().clean()
        mobile = self.cleaned_data['mob_no']
        if '-' in mobile:
            if '-' in mobile[2]:
                y = mobile.split('-')
                if len(y[0]) != 2 or len(y[1]) != 8:
                    raise ValidationError({
                        'mob_no': 'Invalid Contact number'
                    })
            else:
                raise ValidationError({
                    'mob_no': 'invalid Contact number'
                })
        else:
            pass
        if len(mobile) < 8 or len(mobile) > 14:
            raise ValidationError({
                'mob_no': 'Invalid Contact Number'
            })


class QuestionForm(FormControlMixin, forms.ModelForm):

    class Meta:
        model = Question
        fields = "__all__"

    def clean_mob_no(self):
        mob_no = self.cleaned_data['mob_no']
        try:
            int(mob_no)
        except ValueError:
            raise forms.ValidationError("Invalid Contact Number")
        return mob_no


class AnswerForm(FormControlMixin, forms.ModelForm):

    class Meta:
        model = Answer
        fields = "__all__"

    def clean_mob_no(self):
        mob_no = self.cleaned_data['mob_no']
        try:
            int(mob_no)
        except ValueError:
            raise forms.ValidationError("Invalid Contact Number")
        return mob_no


class FinanceSchemeForm(forms.ModelForm):
    class Meta:
        model = FinanceScheme
        fields = '__all__'
        widgets = {
            'bank_name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'bank name'
            }),
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'scheme title'
            }),
            'image': forms.ClearableFileInput(),
            'description': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Scheme detail'
            }),
            'description': SummernoteWidget(),
        }
