from django.urls import path
from .views import *


urlpatterns = [
    path('loan/', LoanView.as_view(), name='loan'),
    path('enquiry/', EnquiryView.as_view(), name='enquiry'),
    path('insurance/', InsuranceView.as_view(), name='insurance'),
    path('sell/', SellView.as_view(), name='sell'),
    path('emi-calculator/', EmiCalculatorView.as_view(), name='emi-calculator'),
    path('broker-dealer/', BrokerDealerView.as_view(), name='broker'),
    path('ask-question/', AskQuestionView.as_view(), name='ask-question'),
    path('answer/<int:pk>/', AnswerView.as_view(), name='answer'),
    path('blog/', BlogView.as_view(), name='blog'),
    path('blog-detail/<pk>/', BlogDetailView.as_view(), name='blog-detail'),
    path('bank-/<int:pk>/-detail/', BankDetailView.as_view(), name='bank_detail'),
    path('farm-implements/', SellFarmImplementsView.as_view(),
         name='farm-implements'),
    path('sell-harvester/', SellHarversterView.as_view(), name='sell-harvester'),
    path('land-properties/', SellLandPropertiesView.as_view(),
         name='land-properties'),
    path('livestock/', SellLivestockView.as_view(), name='livestock'),

]
